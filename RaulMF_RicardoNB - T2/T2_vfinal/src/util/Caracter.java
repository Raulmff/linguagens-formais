package util;

import java.util.ArrayList;

public class Caracter {
	private ArrayList<Character> maiusculas, minusculas, digitos;
	
	public Caracter() {
		this.maiusculas = new ArrayList<Character>();
		this.maiusculas.add('A');
		this.maiusculas.add('B');
		this.maiusculas.add('C');
		this.maiusculas.add('D');
		this.maiusculas.add('E');
		this.maiusculas.add('F');
		this.maiusculas.add('G');
		this.maiusculas.add('H');
		this.maiusculas.add('I');
		this.maiusculas.add('J');
		this.maiusculas.add('K');
		this.maiusculas.add('L');
		this.maiusculas.add('M');
		this.maiusculas.add('N');
		this.maiusculas.add('O');
		this.maiusculas.add('P');
		this.maiusculas.add('Q');
		this.maiusculas.add('R');
		this.maiusculas.add('S');
		this.maiusculas.add('T');
		this.maiusculas.add('U');
		this.maiusculas.add('V');
		this.maiusculas.add('W');
		this.maiusculas.add('Y');
		this.maiusculas.add('X');
		this.maiusculas.add('Z');
		
		this.minusculas = new ArrayList<Character>();
		this.minusculas.add('a');
		this.minusculas.add('b');
		this.minusculas.add('c');
		this.minusculas.add('d');
		this.minusculas.add('e');
		this.minusculas.add('f');
		this.minusculas.add('g');
		this.minusculas.add('h');
		this.minusculas.add('i');
		this.minusculas.add('j');
		this.minusculas.add('k');
		this.minusculas.add('l');
		this.minusculas.add('m');
		this.minusculas.add('n');
		this.minusculas.add('o');
		this.minusculas.add('p');
		this.minusculas.add('q');
		this.minusculas.add('r');
		this.minusculas.add('s');
		this.minusculas.add('t');
		this.minusculas.add('u');
		this.minusculas.add('v');
		this.minusculas.add('w');
		this.minusculas.add('y');
		this.minusculas.add('x');
		this.minusculas.add('z');
		
		this.digitos = new ArrayList<Character>();
		this.digitos.add('0');
		this.digitos.add('1');
		this.digitos.add('2');
		this.digitos.add('3');
		this.digitos.add('4');
		this.digitos.add('5');
		this.digitos.add('6');
		this.digitos.add('7');
		this.digitos.add('8');
		this.digitos.add('9');
	}
	
	public boolean is_letra_maiuscula(char caracter) {
		return this.maiusculas.contains(caracter);
	}
	public boolean is_letra_minuscula(char caracter) {
		return this.minusculas.contains(caracter);
	}
	public boolean is_digito(char caracter) {
		return this.digitos.contains(caracter);
	}
}

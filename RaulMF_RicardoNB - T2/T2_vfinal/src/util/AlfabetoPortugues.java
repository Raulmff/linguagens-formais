package util;
public class AlfabetoPortugues {
	private char[] alfabeto;
	private int cont;
	
	public AlfabetoPortugues() {
		this.cont = -1;
		this.gerar_alfabeto();
	}
	
	public char get_next_simbolo() {
		this.cont++;
		
		if (this.cont >= alfabeto.length) {
			this.cont = 0;
		}
		
		return this.alfabeto[this.cont];
	}
	
	private void gerar_alfabeto() {
		char[] alfabeto = {
				'S', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
				'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q',
				'R', 'T', 'U', 'V', 'W', 'Y', 'X', 'Z'
		};
		
		this.alfabeto = alfabeto;
	}
}

/*
S -> a
A -> a
B -> a
C -> a
D -> a
E -> a
F -> a
G -> a
H -> a
I -> a
J -> a
K -> a
L -> a
M -> a
N -> a
O -> a
P -> a
Q -> a
R -> a
T -> a
U -> a
V -> a
W -> a
Y -> a
X -> a
Z -> a
 */
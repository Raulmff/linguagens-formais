package table;

import java.util.ArrayList;

public class Table {
	private TableRow head;
	private ArrayList<TableRow> body;
	
	/* Metodos Construtor */
	public Table(TableRow head) {
		this.head = head;
		this.re_load();
	}
	public void re_load() {
		this.body = new ArrayList<TableRow>();
	}
	
	/* Metodos add/remove */
	public void add_row(TableRow row) {
		this.body.add(row);
	}
	
	public void remove_row(int i) {
		this.body.remove(i);
	}
	
	/* Metodos Getter */
	public TableRow get_head() {
		return this.head;
	}
	public TableRow get_body(int i) {
		return this.body.get(i);
	}
	public int get_size() {
		return this.body.size();
	}
}

package conjunto;

import gramatica.Gramatica;

public class ConjuntoAlfabeto extends Conjunto<Character> {
	public ConjuntoAlfabeto() {
		super();
	}
	
	@Override
	public Character add(Character simbolo) {
		if (simbolo == Gramatica.EPSILON) {
			return null;
		}
		
		return super.add(simbolo);
	}
}

package conjunto;

import java.util.ArrayList;

import gramatica.Gramatica;

public class ConjuntoGramatica {
	protected ArrayList<Gramatica> gramaticas;
	
	public ConjuntoGramatica() {
		this.gramaticas = new ArrayList<Gramatica>();
	}
	public void add(Gramatica gramatica) {
		if (!this.gramaticas.contains(gramatica)) {
			this.gramaticas.add(gramatica);
		}
	}
	public void remove(Gramatica gramatica) {
		this.gramaticas.remove(gramatica);
	}
	public Gramatica get(String nomeBuscado) {
		/*
		 * Percorre o array de gramaticas, e para cada uma delas
		 * verifica se possui o nome da gramatica buscada.
		 */
		for (int c = 0; c < this.gramaticas.size(); c++) {
			Gramatica gramatica;
			gramatica = this.gramaticas.get(c);
			
			String nomeGramatica;
			nomeGramatica = gramatica.get_nome();
			
			if (nomeGramatica.equals(nomeBuscado)) {
				return gramatica;
			}
		}
		
		return null;
	}
	
	public ArrayList<Gramatica> get_arrayList() {
		return this.gramaticas;
	}
}

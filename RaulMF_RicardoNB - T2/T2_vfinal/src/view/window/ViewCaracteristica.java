package view.window;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ViewCaracteristica {
	protected JPanel panel;
	protected JTextField inputFinita, inputVazia, inputFatorada, inputFatoravel;
	protected JTextField inputRecursaoDireta, inputRecursaoIndireta, inputPropria;
	protected JButton showFirst, showFirstNt, showFollow, showPropria, gerarPropria;
	protected JButton editar, remover, verificarFatoravel;
	
	protected JButton removerDireta, removerIndireta;
	
	protected ActionListener event;
	
	protected boolean finita, vazia, fatorada, fatoravel;
	protected boolean recursaoDireta, recursaoIndireta, propria;
	
	public ViewCaracteristica(ActionListener event, int x, int y, int width, int height) {
		this.panel = new JPanel();
		this.panel.setLayout(null);
		this.panel.setBounds(x, y+80, width, height-80);
		
		this.event = event;
		
		this.load_input();
		this.load_buttons();
		
		this.set_fatorada(false);
		this.set_fatoravel(false);
		this.set_finita(false);
		this.set_propria(false);
		this.set_recursao_direta(false);
		this.set_recursao_indireta(false);
		this.set_vazia(false);
	}
	
	protected String conteudo(boolean b) {
		if (b) {
			return "Sim";
		}
		
		return "Nao";
	}
	public int get_n_passos() {
		int passos;
		System.out.println(this.inputFatoravel.getText());
		try {
			passos = Integer.parseInt(this.inputFatoravel.getText());
			
			return passos;
		} catch(NumberFormatException e) {
			passos = -1;
		}
		
		return passos;
	}
	protected void load_input() {
		int y;
		y = 0;
		
		this.panel.add( Window.new_label(0, y, 140, 30, "Finita") );
		this.inputFinita = new JTextField();
		this.inputFinita.setBounds(150, 0, 100, 30);
		this.inputFinita.setEditable(false);
		this.panel.add(this.inputFinita);
		
		this.panel.add( Window.new_label(0, y+40, 140, 30, "Vazia") );
		this.inputVazia = new JTextField();
		this.inputVazia.setBounds(150, 40, 100, 30);
		this.inputVazia.setEditable(false);
		this.panel.add(this.inputVazia);
		
		this.panel.add( Window.new_label(0, y+80, 140, 30, "Fatorada") );
		this.inputFatorada = new JTextField();
		this.inputFatorada.setBounds(150, y+80, 100, 30);
		this.inputFatorada.setEditable(false);
		this.panel.add(this.inputFatorada);
		
		this.panel.add( Window.new_label(0, y+120, 140, 30, "Rec. E. Direta") );
		this.inputRecursaoDireta = new JTextField();
		this.inputRecursaoDireta.setBounds(150, y+120, 100, 30);
		this.inputRecursaoDireta.setEditable(false);
		this.panel.add(this.inputRecursaoDireta);
		
		this.removerDireta = new JButton("Remover");
		this.removerDireta.setBounds(150, y+160, 100, 30);
		this.removerDireta.addActionListener(this.event);
		this.removerDireta.setActionCommand("REMOVER_DIRETA");
		this.panel.add(this.removerDireta);
		
		this.panel.add( Window.new_label(0, y+200, 140, 30, "Rec. E. Indireta") );
		this.inputRecursaoIndireta = new JTextField();
		this.inputRecursaoIndireta.setBounds(150, y+200, 100, 30);
		this.inputRecursaoIndireta.setEditable(false);
		this.panel.add(this.inputRecursaoIndireta);
		
		this.removerIndireta = new JButton("Remover");
		this.removerIndireta.setBounds(150, y+240, 100, 30);
		this.removerIndireta.addActionListener(this.event);
		this.removerIndireta.setActionCommand("REMOVER_INDIRETA");
		this.panel.add(this.removerIndireta);
		
		this.panel.add( Window.new_label(0, y+280, 145, 30, "Fatoravel") );
		this.inputFatoravel = new JTextField();
		this.inputFatoravel.setBounds(150, y+280, 40, 30);
		this.panel.add(this.inputFatoravel);
		this.panel.add( Window.new_label(190, y+280, 170, 30, "passos") );
		
		this.verificarFatoravel = new JButton("Verificar");
		this.verificarFatoravel.setBounds(150, y+320, 100, 30);
		this.verificarFatoravel.addActionListener(this.event);
		this.verificarFatoravel.setActionCommand("VERIFICAR_FATORAVEL");
		this.panel.add(this.verificarFatoravel);
	}
	
	protected void load_buttons() {
		int y;
		y = 380;
		
		this.showFirstNt = new JButton("FirstNt");
		this.showFirstNt.setBounds(0, y, 110, 30);
		this.showFirstNt.addActionListener(this.event);
		this.showFirstNt.setActionCommand("SHOW_FIRST_NT");
		this.panel.add(this.showFirstNt);
		
		this.showFollow = new JButton("Follow");
		this.showFollow.setBounds(120, y, 130, 30);
		this.showFollow.addActionListener(this.event);
		this.showFollow.setActionCommand("SHOW_FOLLOW");
		this.panel.add(this.showFollow);
		
		this.showFirst = new JButton("First");
		this.showFirst.setBounds(0, y+40, 80, 30);
		this.showFirst.addActionListener(this.event);
		this.showFirst.setActionCommand("SHOW_FIRST");
		this.panel.add(this.showFirst);
		
		this.showPropria = new JButton("Mostrar Propria");
		this.showPropria.setBounds(90, y+40, 160, 30);
		this.showPropria.addActionListener(this.event);
		this.showPropria.setActionCommand("SHOW_PROPRIA");
		this.panel.add(this.showPropria);
		
		this.gerarPropria = new JButton("Gerar Propria");
		this.gerarPropria.setBounds(90, y+40, 160, 30);
		this.gerarPropria.addActionListener(this.event);
		this.gerarPropria.setActionCommand("GERAR_PROPRIA");
		this.panel.add(this.gerarPropria);
		
		this.editar = new JButton("Editar");
		this.editar.setBounds(0, 469, 120, 30);
		this.editar.addActionListener(this.event);
		this.editar.setActionCommand("EDITAR");
		this.panel.add(this.editar);
		
		this.remover = new JButton("Remover");
		this.remover.setBounds(130, 469, 120, 30);
		this.remover.addActionListener(this.event);
		this.remover.setActionCommand("REMOVER");
		this.panel.add(this.remover);
	}
	
	public void set_visible(boolean visible) {
		this.panel.setVisible(visible);
	}
	public void set_finita(boolean b) {
		this.finita = b;
		this.inputFinita.setText(this.conteudo(this.finita));
	}
	public void set_vazia(boolean b) {
		this.vazia = b;
		this.inputVazia.setText(this.conteudo(this.vazia));
	}
	public void set_fatorada(boolean b) {
		this.fatorada = b;
		this.inputFatorada.setText(this.conteudo(this.fatorada));
	}
	public void set_fatoravel(boolean b) {
		this.fatoravel = b;
	}
	public void set_recursao_direta(boolean b) {
		this.recursaoDireta = b;
		this.inputRecursaoDireta.setText(this.conteudo(this.recursaoDireta));
		
		if (this.recursaoDireta) {
			this.removerDireta.setEnabled(true);
		} else {
			this.removerDireta.setEnabled(false);
		}
	}
	public void set_recursao_indireta(boolean b) {
		this.recursaoIndireta = b;
		this.inputRecursaoIndireta.setText(this.conteudo(this.recursaoIndireta));
		
		if (this.recursaoIndireta) {
			this.removerIndireta.setEnabled(true);
		} else {
			this.removerIndireta.setEnabled(false);
		}
	}
	public void set_propria(boolean b) {
		this.propria = b;
		
		if (b) {
			this.gerarPropria.setVisible(false);
			this.showPropria.setVisible(true);
		} else {
			this.gerarPropria.setVisible(true);
			this.showPropria.setVisible(false);
		}
	}
	
	public JPanel get_JPanel() {
		return this.panel;
	}
}

package view.window;

import java.awt.Component;

import javax.swing.JPanel;

import gramatica.Gramatica;
import view.component.MenuDialog;
import view.event.EventOperar;

public class WindowOperar extends Window {
	protected JPanel panelMain;
	protected EventOperar event;
	protected ViewGramatica view;
	
	protected String operacao;
	
	public WindowOperar(Manager manager, String operacao) {
		super(manager, 10, 10, 440, 500);
		this.set_title("Nova Gramatica");
		
		this.event = new EventOperar(this);
		this.view = new ViewGramatica(15, 10, 415, 400);
		
		this.panelMain = new JPanel();
		this.panelMain.setLayout(null);
		this.panelMain.setBounds(0, 0, 440, 450);
		
		this.panelMain.add(this.view.get_JPanel());
		this.panelMain.setVisible(true);
		
		this.operacao = operacao;
		this.load_menu_dialog(operacao);
		
		this.contentPane.add(this.panelMain);
	}
	
	protected void load_menu_dialog(String operacao) {
		this.menuDialog = new MenuDialog(this.event, 15, 430, 409, 50);
		this.menuDialog.add_dialog_botao("Cancelar", "CANCELAR");
		
		if (operacao.equals("Novo")) {
			this.menuDialog.add_dialog_botao("Adicionar", "ADICIONAR");
		} else {
			this.menuDialog.add_dialog_botao("Salvar", "SALVAR");
		}
		
		this.contentPane.add(this.menuDialog.get_JPanel());
	}
	
	@Override
	public void set_visible(boolean visible) {
		super.set_visible(visible);
		
		if (!visible) {
			return;
		}
		
		Window.insert_message_sucess("Um terminal eh considerado como um unico caracter\nEx: S -> i d possui dois terminais: \"i\" e \"d\"", "AVISO");
		
		if (this.operacao.equals("Novo")) {
			this.view.set_nome(Gramatica.get_novo_nome_sem_inc());
		}
		
		Component[] components;
		components = this.panelMain.getComponents();
		
		for (int c = 0; c < components.length; c++) {
			Component component;
			component = components[c];
			component.setVisible(false);
			component.setVisible(true);
		}
	}
	
	public void set_gramatica(Gramatica gramatica) {
		this.view.set_nome(gramatica.get_nome());
		this.view.set_producao(gramatica.get_producoes());
	}
	
	public void adicionar() {
		String nome, producoes;
		nome = this.view.get_nome();
		producoes = this.view.get_producao();
		
		if (Gramatica.validar_gramatica(producoes)) {
			Gramatica gramatica;
			gramatica = new Gramatica(Gramatica.get_novo_nome());
			gramatica.set_producoes(producoes);
			this.manager.add_gramatica(gramatica);
			Window.insert_message_sucess("Gramatica "+nome+" adicionada com sucesso!", "Nova Gramatica");
		} else {
			Window.insert_message_error("Producao invalida!", "Nova Gramatica");
		}
	}
	public void salvar() {
		String nome, producoes;
		nome = this.view.get_nome();
		producoes = this.view.get_producao();
		
		if (Gramatica.validar_gramatica(producoes)) {
			Window.insert_message_sucess("Gramatica "+nome+" salva com sucesso!\nAs gramaticas intermediarias foram removidas!", "Nova Gramatica");
			
			Gramatica gramatica;
			gramatica = this.manager.get_gramatica(nome);
			gramatica.set_producoes(producoes);
			
			this.manager.hide_editar();
			this.manager.atualizar(gramatica);
		} else {
			Window.insert_message_error("Producao invalida!", "Nova Gramatica");
		}
	}
	public void fechar() {
		this.set_visible(false);
	}
}

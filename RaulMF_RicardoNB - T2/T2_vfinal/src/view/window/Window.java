package view.window;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import view.component.MenuDialog;

public class Window {
	protected JPanel contentPane;
	protected JFrame frame;
	protected MenuDialog menuDialog;
	protected Manager manager;
	
	protected int width, height, marginLeft, marginTop;
	
	/* Metodos Construtores */
	public Window(Manager manager, int x, int y, int width, int height) {
		this.frame = new JFrame();
		this.frame.setBounds(0, 0, width, height);
		
		this.contentPane = new JPanel();
		this.contentPane.setLayout(new FlowLayout());
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.frame.setContentPane(this.contentPane);
		
		this.frame.setLayout(null);
		this.frame.setResizable(false);
		
		this.contentPane.setVisible(true);
		
		this.manager = manager;
	}
	
	public void set_visible(boolean visible) {
		this.frame.setVisible(visible);
		
		if (!visible) {
			return;
		}
		
		Component[] components;
		components = this.contentPane.getComponents();
		
		for (int c = 0; c < components.length; c++) {
			Component component;
			component = components[c];
			component.setVisible(false);
			component.setVisible(true);
		}
	}
	
	public void set_title(String title) {
		this.frame.setTitle(title);
	}
	
	protected static JLabel new_label(int x, int y, int width, int height, String title) {
		JLabel label;
		label = new JLabel(title);
		label.setFont(Window.get_font());
		label.setBounds(x, y, width, height);
		
		return label;
	}
	
	public static Font get_font() {
		String familia = "Arial";
		int estilo = Font.BOLD;
		int tamanho = 13;
		
		return new Font(familia, estilo, tamanho);
	}
	public static Font get_font2() {
		String familia = "Arial";
		int estilo = 0;
		int tamanho = 15;
		
		return new Font(familia, estilo, tamanho);
	}
	public Manager get_manager() {
		return this.manager;
	}
	
	public static void insert_message_error(String message, String title) {
		JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
	}
	public static void insert_message_sucess(String message, String title) {
		JOptionPane.showMessageDialog(null, message, title, JOptionPane.INFORMATION_MESSAGE);
	}
}

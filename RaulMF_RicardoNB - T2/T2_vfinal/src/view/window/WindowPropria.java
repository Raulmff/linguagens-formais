package view.window;

import java.util.ArrayList;

import javax.swing.JPanel;

import gramatica.Gramatica;
import view.component.MenuDialog;
import view.event.EventPropria;

public class WindowPropria extends Window {
	protected ViewGramatica viewNe, viewNa, viewNf, viewVi;
	protected ArrayList<Gramatica> derivadas;
	protected ArrayList<String> conjuntos;
	protected EventPropria event;
	
	public WindowPropria(Manager manager) {
		super(manager, 10, 10, 1000, 600);
		
		int y, width, height;
		y = 60;
		width = 220;
		height = 380;
		
		this.event = new EventPropria(this);
		this.contentPane.add( Window.new_label(30, 20, 220, 20, "GLC &-LIVRE") );
		this.contentPane.add( Window.new_label(270, 20, 220, 20, "GLC SEM CICLOS") );
		this.contentPane.add( Window.new_label(510, 20, 220, 20, "GLC FERTIL") );
		this.contentPane.add( Window.new_label(750, 20, 220, 20, "GLC PROPRIA") );
		
		this.viewNe = new ViewGramatica(30, y, width, height);
		this.viewNa = new ViewGramatica(270, y, width, height);
		this.viewNf = new ViewGramatica(510, y, width, height);
		this.viewVi = new ViewGramatica(750, y, width, height);
		
		this.viewNe.set_editable(false);
		this.viewNa.set_editable(false);
		this.viewNf.set_editable(false);
		this.viewVi.set_editable(false);
		
		this.contentPane.add(this.viewNe.get_JPanel());
		this.contentPane.add(this.viewNa.get_JPanel());
		this.contentPane.add(this.viewNf.get_JPanel());
		this.contentPane.add(this.viewVi.get_JPanel());
		
		this.conjuntos = new ArrayList<String>();
		
		this.load_menu_dialog();
	}
	
	protected void load_menu_dialog() {
		this.menuDialog = new MenuDialog(this.event, 0, 450, 1000, 200);
		
		this.menuDialog.add_dialog_botao("Adicionar", "ADICIONAR_NE", 93, 0, 150);
		this.menuDialog.add_dialog_botao("Adicionar", "ADICIONAR_NA", 333, 0, 150);
		this.menuDialog.add_dialog_botao("Adicionar", "ADICIONAR_NF", 573, 0, 150);
		this.menuDialog.add_dialog_botao("Adicionar", "ADICIONAR_VI", 813, 0, 150);
		
		this.menuDialog.add_dialog_botao("Mostrar Ne", "MOSTRAR_NE", 93, 40, 150);
		this.menuDialog.add_dialog_botao("Mostrar Na", "MOSTRAR_NA", 333, 40, 150);
		this.menuDialog.add_dialog_botao("Mostrar Nf", "MOSTRAR_NF", 573, 40, 150);
		this.menuDialog.add_dialog_botao("Mostrar Vi", "MOSTRAR_VI", 813, 40, 150);
		
		JPanel panelMenu;
		panelMenu = this.menuDialog.get_JPanel();
		panelMenu.add( Window.new_label(30, 90, 1000, 20, "BOTAO ADICIONAR: CRIA UMA NOVA GRAMATICA COM AS PRODUCOES DA GRAMATICA CORRESPONDENTE.") );
		
		this.contentPane.add(this.menuDialog.get_JPanel());
	}
	
	public void set_derivadas(ArrayList<Gramatica> derivadas) {
		Gramatica epsilon, ciclos, fertil, propria;
		epsilon = derivadas.get(0);
		ciclos = derivadas.get(1);
		fertil = derivadas.get(2);
		propria = derivadas.get(3);
		
		this.derivadas = derivadas;
		
		this.viewNe.set_nome(epsilon.get_nome());
		this.viewNe.set_producao(epsilon.get_producoes());
		
		this.viewNa.set_nome(ciclos.get_nome());
		this.viewNa.set_producao(ciclos.get_producoes());
		
		this.viewNf.set_nome(fertil.get_nome());
		this.viewNf.set_producao(fertil.get_producoes());
		
		this.viewVi.set_nome(propria.get_nome());
		this.viewVi.set_producao(propria.get_producoes());
	}
	
	public void adicionar(String tipoGramatica) {
		System.out.println("Adicionar "+tipoGramatica);
		
		Gramatica derivada;
		switch (tipoGramatica) {
			case "NE":
				derivada = this.derivadas.get(0);
				break;
			case "NA":
				derivada = this.derivadas.get(1);
				break;
			case "NF":
				derivada = this.derivadas.get(2);
				break;
			case "VI":
				derivada = this.derivadas.get(3);
				break;
			default:
				derivada = null;
				break;
		}
		
		if (derivada == null) {
			Window.insert_message_error("A gramatica ja foi adicionada!!", "Adicionar Gramatica");
			return;
		}
		
		if (this.manager.get_gramaticas().contains(derivada)) {
			Window.insert_message_error("A gramatica ja foi adicionada!", "Adicionar Gramatica");
		} else {
			this.manager.add_gramatica(derivada);
			Window.insert_message_sucess("Gramatica adicionada com sucesso!", "Adicionar Gramatica");
		}
	}
	public void mostrar(String conjuntoNome) {
		System.out.println("Mostrar "+conjuntoNome);
		
		String conjunto;
		switch (conjuntoNome) {
			case "NE":
				conjunto = this.conjuntos.get(0);
				break;
			case "NA":
				conjunto = this.conjuntos.get(1);
				break;
			case "NF":
				conjunto = this.conjuntos.get(2);
				break;
			case "VI":
				conjunto = this.conjuntos.get(3);
				break;
			default:
				conjunto = "";
				break;
		}
		
		this.manager.visible_conjunto(conjuntoNome, conjunto);
	}

	public void set_conjuntos(ArrayList<String> conjuntos) {
		this.conjuntos = conjuntos;
	}
}

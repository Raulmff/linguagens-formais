package view.window;
import javax.swing.JFrame;

import conjunto.Conjunto;
import conjunto.ConjuntoNaoTerminal;
import gramatica.Gramatica;
import gramatica.NaoTerminal;
import gramatica.operacao.OperarGramatica;
import gramatica.operacao.TransformarGramaticaPropria;
import view.component.MenuAside;
import view.component.MenuBar;
import view.event.EventMenuBar;
import view.event.EventPrincipal;

public class WindowPrincipal extends Window {
	protected MenuBar menuBar;
	protected MenuAside menuAside;
	
	protected EventMenuBar event;
	
	protected ViewGramatica viewGramatica;
	protected ViewCaracteristica viewCaracteristica;
	protected OperarGramatica operarGramatica;
	protected TransformarGramaticaPropria transformarPropria;
	
	/* Metodos Construtores */
	public WindowPrincipal(Manager manager) {
		super(manager, 10, 10, 1000, 650);
		this.set_title("Visualizar Gramatica");
		
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.width = 675;
		this.height = 380;
		this.marginLeft = 200;
		this.marginTop = 40;
		
		this.operarGramatica = new OperarGramatica();
		this.menuAside = new MenuAside(this, 20, 50, 200, 550);
		this.menuAside.set_menu( this.manager.gramaticas );
		
		this.viewGramatica = new ViewGramatica(240, 50, 450, 530);
		this.viewGramatica.set_editable(false);
		
		this.transformarPropria = new TransformarGramaticaPropria();
		
		EventPrincipal eventPrincipal;
		eventPrincipal = new EventPrincipal(this);
		this.viewCaracteristica = new ViewCaracteristica(eventPrincipal, 710, 0, 290, 600);
		
		this.event = new EventMenuBar(this);
		this.menuBar = new MenuBar(this.event, 1000);
		
		this.contentPane.add(this.menuAside.get_JPanel());
		this.contentPane.add(this.menuBar.get_jmenu_bar());
		this.contentPane.add(this.viewGramatica.get_JPanel());
		this.contentPane.add(this.viewCaracteristica.get_JPanel());
	}
	
	public void repaint() {
		this.menuAside.set_menu(this.manager.gramaticas);
	}
	
	@Override
	public void set_visible(boolean visible) {
		super.set_visible(visible);
		
		this.viewCaracteristica.set_visible(false);
		this.viewGramatica.set_visible(false);
	}
	
	public void mostrar_gramatica(String nomeGramatica) {
		Gramatica gramatica;
		gramatica = this.manager.get_gramatica(nomeGramatica);
		this.manager.set_gramatica_operando(gramatica);
		
		this.viewGramatica.set_nome(gramatica.get_nome());
		this.viewGramatica.set_producao( gramatica.get_producoes() );
		
		this.viewCaracteristica.set_finita( this.operarGramatica.get_gramatica_finita(gramatica) );
		this.viewCaracteristica.set_vazia( this.operarGramatica.get_linguagem_vazia(gramatica) );
		this.viewCaracteristica.set_fatorada( this.operarGramatica.get_fatorada(gramatica) );
		
		ConjuntoNaoTerminal naoTerminaisRecursaoDireta, naoTerminaisRecursaoIndireta;
		naoTerminaisRecursaoDireta = new ConjuntoNaoTerminal();
		naoTerminaisRecursaoIndireta = new ConjuntoNaoTerminal();
		this.operarGramatica.get_recursao_esquerda_tipo(gramatica, naoTerminaisRecursaoDireta, naoTerminaisRecursaoIndireta);
		
		this.viewCaracteristica.set_recursao_direta( (naoTerminaisRecursaoDireta.size() > 0) );
		this.viewCaracteristica.set_recursao_indireta( (naoTerminaisRecursaoIndireta.size() > 0) );
		
		if (gramatica.get_derivadas().size() == 0) {
			this.viewCaracteristica.set_propria(false);
		} else {
			this.viewCaracteristica.set_propria(true);
		}
		
		this.viewCaracteristica.set_visible(true);
		this.viewGramatica.set_visible(true);
	}
	
	public void remover_direta() {
		Gramatica operando;
		operando = this.manager.get_gramatica_operando();
		
		Gramatica direta;
		direta = operando.get_direta();
		
		if (direta == null) {
			direta = this.operarGramatica.get_remover_recursao_esquerda_direta(operando);
			
			this.manager.add_gramatica(direta);
			operando.set_direta(direta);
			Window.insert_message_sucess("Gramatica "+direta.get_nome()+" gerada com sucesso!", "Gerar Gramatica sem Recursao");
		} else {
			Window.insert_message_error("Gramatica "+direta.get_nome()+" ja foi adicionada!", "Gerar Gramatica sem Recursao");
		}
	}
	public void remover_indireta() {
		Gramatica operando;
		operando = this.manager.get_gramatica_operando();
		
		Gramatica indireta;
		indireta = operando.get_indireta();
		
		if (indireta == null) {
			indireta = this.operarGramatica.get_remover_recursao_esquerda_indireta(operando);
			
			this.manager.add_gramatica(indireta);
			operando.set_indireta(indireta);
			Window.insert_message_sucess("Gramatica "+indireta.get_nome()+" gerada com sucesso!", "Gerar Gramatica sem Recursao");
		} else {
			Window.insert_message_error("Gramatica "+indireta.get_nome()+" ja foi adicionada!", "Gerar Gramatica sem Recursao");
		}
	}
	
	public void editar() {
		System.out.println("Editar");
		this.manager.visible_editar(this.manager.get_gramatica_operando());
	}
	public void remover() {
		System.out.println("Remover");
		
		this.manager.remove_gramatica( this.manager.get_gramatica_operando() );
		this.menuAside.set_menu(this.manager.get_gramaticas());
		
		this.viewCaracteristica.set_visible(false);
		this.viewGramatica.set_visible(false);
	}
	public void mostrar_propria() {
		this.manager.visible_propria(this.manager.get_gramatica_operando().get_derivadas(), this.transformarPropria);
	}
	public void mostrar_conjunto(String conjunto) {
		System.out.println("Mostrar: "+conjunto);
		
		Gramatica operando;
		operando = this.manager.get_gramatica_operando();
		
		String conj;
		conj = "";
		
		ConjuntoNaoTerminal naoTerminais;
		naoTerminais = operando.get_naoTerminais();
		
		switch (conjunto) {
			case "FIRST":
				for (int c = 0; c < naoTerminais.size(); c++) {
					NaoTerminal naoTerminal;
					naoTerminal = naoTerminais.get(c);
					
					conj += "FIRST("+naoTerminal.get_simbolo()+")= {";
					
					Conjunto<Character> firsts;
					firsts = naoTerminal.get_firstsT();
					
					for (int i = 0; i < firsts.size(); i++) {
						char first;
						first = firsts.get(i);
						
						conj += first;
						
						if (i != firsts.size()-1) {
							conj += ", ";
						}
					}
					
					conj += "}\n";
				}
				
				this.manager.visible_conjunto("FIRST", conj);
				
				break;
			case "FIRST_NT":
				for (int c = 0; c < naoTerminais.size(); c++) {
					NaoTerminal naoTerminal;
					naoTerminal = naoTerminais.get(c);
					
					conj += "FIRST NT ("+naoTerminal.get_simbolo()+")= {";
					
					ConjuntoNaoTerminal firsts;
					firsts = naoTerminal.get_firstsNt();
					
					for (int i = 0; i < firsts.size(); i++) {
						NaoTerminal first;
						first = firsts.get(i);
						
						conj += first.get_simbolo();
						
						if (i != firsts.size()-1) {
							conj += ", ";
						}
					}
					
					conj += "}\n";
				}
				
				this.manager.visible_conjunto("FIRST NT", conj);
				break;
			case "FOLLOW":
				for (int c = 0; c < naoTerminais.size(); c++) {
					NaoTerminal naoTerminal;
					naoTerminal = naoTerminais.get(c);
					
					conj += "FOLLOW ("+naoTerminal.get_simbolo()+")= {";
					
					Conjunto<Character> follows;
					follows = naoTerminal.get_follows();
					
					for (int i = 0; i < follows.size(); i++) {
						char first;
						first = follows.get(i);
						
						conj += first;
						
						if (i != follows.size()-1) {
							conj += ", ";
						}
					}
					
					conj += "}\n";
				}
				
				this.manager.visible_conjunto("FOLLOW", conj);
				break;
			default:
				break;
		}
		
	}
	public void gerar_propria() {
		System.out.println("Gerar propria");
		this.transformarPropria.get_gramatica_propria( this.manager.get_gramatica_operando() );
		this.viewCaracteristica.set_propria(true);
		Window.insert_message_sucess("Gramatica gerada com sucesso!\nClique em Mostrar Propria para visualizar.", "Gerar Gramatica Propria");
	}

	public void verificar_fatoravel() {
		int passos;
		passos = this.viewCaracteristica.get_n_passos();
		
		if (passos == -1) {
			Window.insert_message_error("Digite um valor numerico", "Verificar fatoracao em n passos");
			return;
		}
		
		Gramatica gramatica;
		gramatica = this.manager.get_gramatica_operando();
		
		Gramatica gramaticaRetorno;
		gramaticaRetorno = this.operarGramatica.get_fatoravel_n_passos(gramatica, passos);
		
		if (gramaticaRetorno == null) {
			Window.insert_message_error(gramatica.get_nome()+" nao eh fatoravel em "+passos+" passos", "Verificar fatoracao em n passos");
		} else {
			Window.insert_message_sucess(gramatica.get_nome()+" eh fatoravel em "+passos+" passos", "Verificar fatoracao em n passos");
		}
	}
}

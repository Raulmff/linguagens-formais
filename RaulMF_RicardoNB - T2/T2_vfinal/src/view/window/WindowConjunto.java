package view.window;

import javax.swing.JLabel;

import view.component.MenuDialog;
import view.component.TextArea;
import view.event.EventConjunto;

public class WindowConjunto extends Window {
	protected EventConjunto event;
	protected JLabel nomeConjunto;
	protected TextArea input;
	
	public WindowConjunto(Manager manager) {
		super(manager, 10, 10, 440, 500);
		this.set_title("Mostrar conjunto");
		this.event = new EventConjunto(this);
		
		this.nomeConjunto = null;
		this.input = new TextArea(15, 65, 410, 355);
		this.input.set_enable(false);
		
		this.load_menu_dialog();
		
		this.contentPane.add(this.input.get_JScrollPane());
	}
	
	public void set_nome(String nome) {
		if (this.nomeConjunto != null) {
			this.contentPane.remove(this.nomeConjunto);
		}
		this.nomeConjunto = Window.new_label(15, 15, 440, 50, "Conjunto "+nome);
		this.contentPane.add(this.nomeConjunto);
	}
	public void set_conjunto(String conjunto) {
		String conjuntosAtuais;
		conjuntosAtuais = "";
		
		if (!conjuntosAtuais.equals("")) {
			conjuntosAtuais += "\n";
		}
		conjuntosAtuais += conjunto;
		
		this.input.set_text(conjuntosAtuais);
	}
	
	protected void load_menu_dialog() {
		this.menuDialog = new MenuDialog(this.event, 15, 440, 409, 50);
		this.menuDialog.add_dialog_botao("Fechar", "FECHAR");
		
		this.contentPane.add(this.menuDialog.get_JPanel());
	}
}

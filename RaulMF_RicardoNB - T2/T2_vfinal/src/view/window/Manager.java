package view.window;

import java.util.ArrayList;

import gramatica.Gramatica;
import gramatica.operacao.OperarGramatica;
import gramatica.operacao.TransformarGramaticaPropria;

public class Manager {
	protected WindowPrincipal windowPrincipal;
	protected WindowPropria windowPropria;
	protected WindowOperar windowEditar, windowCriar;
	protected WindowConjunto windowConjunto;
	
	protected OperarGramatica operarGramatica;
	
	protected ArrayList<Gramatica> gramaticas;
	protected Gramatica operando;
	
	public Manager() {
		this.gramaticas = new ArrayList<Gramatica>();
		
		//Gramatica teste;
		//teste = new Gramatica(Gramatica.get_novo_nome());
		//teste.set_producoes("S -> S | a A g | A\nA -> b | a A | B\nB -> & | A");
		
		this.operarGramatica = new OperarGramatica();
		this.windowPrincipal = new WindowPrincipal(this);
		this.windowPropria = new WindowPropria(this);
		this.windowEditar = new WindowOperar(this, "Editar");
		this.windowCriar = new WindowOperar(this, "Novo");
		this.windowConjunto = new WindowConjunto(this);
		
		//this.add_gramatica(teste);
	}
	
	public void set_gramatica_operando(Gramatica operando) {
		this.operando = operando;
	}
	public Gramatica get_gramatica_operando() {
		return this.operando;
	}
	
	public void atualizar(Gramatica gramatica) {
		this.operarGramatica.construir_first(gramatica);
		this.operarGramatica.construir_first_nt(gramatica);
		this.operarGramatica.construir_follow(gramatica);
	}
	
	public void add_gramatica(Gramatica gramatica) {
		this.gramaticas.add(gramatica);
		
		this.atualizar(gramatica);
		
		this.hide_criar();
		this.windowPrincipal.repaint();
	}
	public void remove_gramatica(Gramatica gramatica) {
		this.gramaticas.remove(gramatica);
	}
	public Gramatica get_gramatica(String nome) {
		for (int c = 0; c < this.gramaticas.size(); c++) {
			Gramatica gramatica;
			gramatica = this.gramaticas.get(c);
			
			if (gramatica.get_nome().equals(nome)) {
				return gramatica;
			}
		}
		
		return null;
	}
	public ArrayList<Gramatica> get_gramaticas() {
		return this.gramaticas;
	}
	public void visible_principal() {
		this.windowPrincipal.set_visible(true);
	}
	public void visible_propria(ArrayList<Gramatica> derivadas, TransformarGramaticaPropria transformarPropria) {
		ArrayList<String> conjuntos;
		conjuntos = new ArrayList<String>();
		conjuntos.add(transformarPropria.get_conjuntoNe());
		conjuntos.add(transformarPropria.get_conjuntoNa());
		conjuntos.add(transformarPropria.get_conjuntoNf());
		conjuntos.add(transformarPropria.get_conjuntoVi());
		
		this.windowPropria.set_derivadas(derivadas);
		this.windowPropria.set_conjuntos(conjuntos);
		this.windowPropria.set_visible(true);
	}
	public void visible_editar(Gramatica gramatica) {
		this.windowEditar.set_gramatica(gramatica);
		this.windowEditar.set_visible(true);
	}
	public void visible_criar() {
		this.windowCriar.set_visible(true);
	}
	public void visible_conjunto(String nome, String conjunto) {
		this.windowConjunto.set_nome(nome);
		this.windowConjunto.set_conjunto(conjunto);
		this.windowConjunto.set_visible(true);
	}
	
	public void hide_principal() {
		this.windowPrincipal.set_visible(false);
	}
	public void hide_propria() {
		this.windowPropria.set_visible(false);
	}
	public void hide_editar() {
		this.windowEditar.set_visible(false);
		this.windowPrincipal.mostrar_gramatica(this.operando.get_nome());
	}
	public void hide_criar() {
		this.windowCriar.set_visible(false);
	}
	public void hide_conjunto() {
		this.windowConjunto.set_visible(false);
	}
}

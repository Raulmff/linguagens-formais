package view.window;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JPanel;
import javax.swing.JTextField;

import view.component.TextArea;

public class ViewGramatica {
	protected JPanel panelMain;
	
	protected TextArea inputProducao;
	protected JTextField inputNome;
	
	protected int x, y, width, height;
	
	public ViewGramatica(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		
		this.load_panel_main();
		this.load_input_nome();
		this.load_input_producao();
	}
	
	protected void load_input_nome() {
		this.inputNome = new JTextField();
		this.inputNome.setBounds(0, 20, 200, 20);
		this.inputNome.setEditable(false);
		
		this.panelMain.add(this.inputNome);
	}
	protected void load_input_producao() {
		this.inputProducao = new TextArea(0, 80, this.width-5, this.height-80);
		this.panelMain.add(this.inputProducao.get_JScrollPane());
	}
	protected void load_panel_main() {
		this.panelMain = new JPanel();
		this.panelMain.setLayout(null);
		this.panelMain.setBounds(this.x, this.y, this.width, this.height);
		
		this.panelMain.add( Window.new_label(0, 0, 100, 20, "Nome:") );
		this.panelMain.add( Window.new_label(0, 50, 150, 30, "Producoes:") );
	}
	
	public void set_visible(boolean visible) {
		this.panelMain.setVisible(visible);
		
		if (!visible) {
			return;
		}
		
		Component[] components;
		components = this.panelMain.getComponents();
		
		for (int c = 0; c < components.length; c++) {
			Component component;
			component = components[c];
			component.setVisible(false);
			component.setVisible(true);
		}
	}
	
	public void set_editable(boolean editable) {
		this.inputProducao.get_JTextArea().setEditable(editable);
		this.inputProducao.get_JTextArea().setText("S -> A a S | &\nA -> S A a | B b | C d\nB -> C B c | S A B a\nC -> B c | S C a | A b | &");
		this.inputProducao.get_JTextArea().setFont( Window.get_font2() );
		
		if (!editable) {
			this.inputProducao.get_JTextArea().setBackground( Color.LIGHT_GRAY );
			this.inputProducao.get_JTextArea().setForeground( Color.BLACK );
		}
	}
	
	public void set_nome(String nome) {
		this.inputNome.setText(nome);
	}
	public void set_producao(String producao) {
		this.inputProducao.get_JTextArea().setText(producao);
	}
	
	public String get_nome() {
		return this.inputNome.getText();
	}
	public String get_producao() {
		return this.inputProducao.get_JTextArea().getText();
	}
	
	public JPanel get_JPanel() {
		return this.panelMain;
	}
}

package view.component;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import view.window.Window;

public class MenuDialog {
	private JPanel panel;
	private ActionListener event;
	
	private int widthLivre;
	
	public MenuDialog(ActionListener i, int x, int y, int width, int height) {
		this.event = i;
		
		this.widthLivre = width;
		
		this.panel = new JPanel();
		this.panel.setLayout(null);
		this.panel.setBounds(x, y, width, height);
	}
	
	public void set_visible(boolean visible) {
		this.panel.setVisible(visible);
	}
	
	/* Metodos Getter */
	public JPanel get_JPanel() {
		return this.panel;
	}
	
	public void add_dialog_botao(String label, String actionCommand, int width) {
		this.widthLivre -= width;
		
		int posX;
		posX = this.widthLivre;
		
		// Adiciona espacamento para proximo botao nao ficar grudado com o atualmente criado
		this.widthLivre -= 5;
		
		JButton button;
		button = new JButton(label);
		button.setFont( Window.get_font() );
		button.setBounds(posX, 0, width, 32);
		button.addActionListener(this.event);
		button.setActionCommand(actionCommand);
		
		this.panel.add(button);
	}
	
	/* Metodos Add */
	public void add_dialog_botao(String label, String actionCommand) {
		this.add_dialog_botao(label, actionCommand, 130);
	}
	
	public void add_dialog_botao(String label, String actionCommand, int x, int y, int width) {
		JButton button;
		button = new JButton(label);
		button.setFont( Window.get_font() );
		button.setBounds(x, y, width, 32);
		button.addActionListener(this.event);
		button.setActionCommand(actionCommand);
		
		this.panel.add(button);
	}
}

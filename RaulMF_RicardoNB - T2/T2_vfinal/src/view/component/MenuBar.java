package view.component;

import java.awt.Color;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.UIManager;

import view.event.EventMenuBar;

public class MenuBar {
	protected JMenuBar menuBar;
	protected EventMenuBar event;
	
	/* 
	 * Construtor
	 */
	public MenuBar(EventMenuBar event, int width) {
		this.event = event;
		
		this.menuBar = new JMenuBar();
		this.menuBar.setFont(UIManager.getFont("MenuBar.font"));
		this.menuBar.setBackground( new Color(211, 220, 220) );
		this.menuBar.setForeground( new Color(0, 0, 0) );
		this.menuBar.setBounds(0, 0, width, 30);
		
		this.gerar_menu_gr();
	}
	
	private void gerar_menu_gr() {
		JMenuItem criar;
		criar = new JMenuItem("Novo");
		criar.addActionListener(this.event);
		criar.setActionCommand("GR_CRIAR");
		
		JMenu menuGR;
		menuGR = new JMenu("Gramatica");
		menuGR.add(criar);
		
		this.menuBar.add(menuGR);
	}
	
	/* Metodos Getters */
	public JMenuBar get_jmenu_bar() {
		return this.menuBar;
	}
}

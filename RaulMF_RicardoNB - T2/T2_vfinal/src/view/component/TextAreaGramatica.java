package view.component;

import javax.swing.JScrollPane;

import gramatica.Gramatica;

public class TextAreaGramatica {
	protected TextArea textArea;
	
	public TextAreaGramatica(int x, int y) {
		this.textArea = new TextArea(x, y, 340, 380);
		this.textArea.set_enable(false);
		
		this.hide();
	}
	
	public JScrollPane get_JScrollPane() {
		return this.textArea.get_JScrollPane();
	}
	
	public void show(Gramatica gramatica) {
		String producoes;
		producoes = gramatica.get_producoes();
		
		this.textArea.set_text(producoes);
		this.textArea.set_visible(true);
	}
	
	public void hide() {
		this.textArea.set_visible(false);
	}
}

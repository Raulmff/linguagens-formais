package view.component;

import java.util.ArrayList;

import javax.swing.JPanel;

import gramatica.Gramatica;
import table.Table;
import table.TableRow;
import view.event.EventMenuAside;
import view.window.WindowPrincipal;

public class MenuAside {
	protected JPanel panel;
	protected EventMenuAside event;
	
	protected Table table;
	protected ViewTable vTable;
	
	public MenuAside(WindowPrincipal window, int x, int y, int width, int height) {
		this.event = new EventMenuAside(window);
		this.vTable = new ViewTable(0, 0, width, height-20, this.event);
		
		TableRow rowHead;
		rowHead = new TableRow();
		rowHead.add_column("Menu");
		
		this.table = new Table(rowHead);
		
		this.panel = new JPanel();
		this.panel.setLayout(null);
		this.panel.setBounds(x, y, width, height);
		
		this.load();
	}
	
	protected void load() {
		if (this.vTable != null) {
			this.panel.remove(this.vTable.get_JScrollPane());
		}
		
		this.table.re_load();
		this.vTable.set_table(this.table);
		this.panel.add(this.vTable.get_JScrollPane());
	}
	
	/* Metodos Setter */
	public void set_menu(ArrayList<Gramatica> array) {
		this.load();
		
		for (int c = 0; c < array.size(); c++) {
			Gramatica linguagem;
			linguagem = array.get(c);
			
			TableRow row;
			row = new TableRow();
			row.add_column(linguagem.get_nome());
			
			this.table.add_row(row);
		}
		
		this.vTable.reload_model();
	}
	
	/* Metodos Getter */
	public JPanel get_JPanel() {
		return this.panel;
	}
	
	public void repaint() {
		this.panel.repaint();
		this.vTable.repaint();
	}
}

package view.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.window.WindowPropria;

public class EventPropria implements ActionListener {
	protected WindowPropria window;
	
	public EventPropria(WindowPropria window) {
		this.window = window;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String comando;
		comando = e.getActionCommand();
		
		switch (comando) {
			case "ADICIONAR_NE":
				this.window.adicionar("NE");
				break;
			case "ADICIONAR_NA":
				this.window.adicionar("NA");
				break;
			case "ADICIONAR_NF":
				this.window.adicionar("NF");
				break;
			case "ADICIONAR_VI":
				this.window.adicionar("VI");
				break;
			case "MOSTRAR_NE":
				this.window.mostrar("NE");
				break;
			case "MOSTRAR_NA":
				this.window.mostrar("NA");
				break;
			case "MOSTRAR_NF":
				this.window.mostrar("NF");
				break;
			case "MOSTRAR_VI":
				this.window.mostrar("VI");
				break;
			default:
				break;
		}
	}
}

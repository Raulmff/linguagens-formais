package view.event;

import java.awt.event.ActionEvent;

import view.window.Manager;
import view.window.WindowPrincipal;

public class EventMenuBar implements IEvent {
	protected WindowPrincipal window;
	protected Manager manager;
	
	public EventMenuBar(WindowPrincipal window) {
		this.window = window;
		this.manager = window.get_manager();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		this.manager.visible_criar();
	}

	@Override
	public void process(EEvent e, Object conteudo) {}
}

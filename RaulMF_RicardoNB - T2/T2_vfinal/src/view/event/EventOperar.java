package view.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.window.WindowOperar;

public class EventOperar implements ActionListener {
	protected WindowOperar window;
	
	public EventOperar(WindowOperar window) {
		this.window = window;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String comando;
		comando = e.getActionCommand();
		
		switch (comando) {
			case "ADICIONAR":
				this.window.adicionar();
				break;
			case "SALVAR":
				this.window.salvar();
				break;
			default:
				this.window.fechar();
				break;
		}
	}

}

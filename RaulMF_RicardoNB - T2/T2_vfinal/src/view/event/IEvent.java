package view.event;

import java.awt.event.ActionListener;

public interface IEvent extends ActionListener {
	public void process(EEvent e, Object conteudo);
}
package view.event;

import java.awt.event.ActionEvent;

import view.window.WindowPrincipal;

public class EventMenuAside implements IEvent {
	protected WindowPrincipal window;
	
	public EventMenuAside(WindowPrincipal window) {
		this.window = window;
	}
	
	@Override
	public void process(EEvent e, Object conteudo) {
		String nomeGramatica;
		nomeGramatica = (String)conteudo;
		
		this.window.mostrar_gramatica(nomeGramatica);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {}
}

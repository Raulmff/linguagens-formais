package view.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.window.WindowConjunto;

public class EventConjunto implements ActionListener {
	protected WindowConjunto window;
	
	public EventConjunto(WindowConjunto window) {
		this.window = window;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		window.get_manager().hide_conjunto();
	}
}

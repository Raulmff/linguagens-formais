package view.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.window.WindowPrincipal;

public class EventPrincipal implements ActionListener {
	protected WindowPrincipal window;
	
	public EventPrincipal(WindowPrincipal window) {
		this.window = window;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		String comando;
		comando = e.getActionCommand();
		
		switch (comando) {
			case "EDITAR":
				this.window.editar();
				break;
			case "REMOVER":
				this.window.remover();
				break;
			case "SHOW_FIRST":
				this.window.mostrar_conjunto("FIRST");
				break;
			case "SHOW_FOLLOW":
				this.window.mostrar_conjunto("FOLLOW");
				break;
			case "SHOW_FIRST_NT":
				this.window.mostrar_conjunto("FIRST_NT");
				break;
			case "SHOW_PROPRIA":
				this.window.mostrar_propria();
				break;
			case "GERAR_PROPRIA":
				this.window.gerar_propria();
				break;
			case "REMOVER_DIRETA":
				this.window.remover_direta();
				break;
			case "REMOVER_INDIRETA":
				this.window.remover_indireta();
				break;
			case "VERIFICAR_FATORAVEL":
				this.window.verificar_fatoravel();
				break;
			default:
				break;
		}
	}
}

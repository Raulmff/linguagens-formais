package gramatica;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import conjunto.ConjuntoAlfabeto;
import conjunto.ConjuntoNaoTerminal;
import conjunto.ConjuntoObject;
import util.Caracter;

public class Gramatica {
	public static final char EPSILON = '&';
	public static final char FINAL_SENTENCA = '$';
	protected static final char SEPARADOR = ',';
	protected static int novoNome = 0;
	
	protected String nome, producoes;
	protected Caracter caracter;
	
	protected NaoTerminal inicial;
	protected Gramatica gramaticaPai;
	protected ConjuntoAlfabeto alfabeto;
	protected ConjuntoNaoTerminal naoTerminais;
	
	protected ArrayList<Gramatica> derivadas;
	protected Gramatica semDireta, semIndireta;
	
	public static String get_novo_nome() {
		String nome;
		nome = "G"+Gramatica.novoNome;
		
		Gramatica.novoNome++;
		
		return nome;
	}
	public static String get_novo_nome_sem_inc() {
		String nome;
		nome = "G"+Gramatica.novoNome;
		
		return nome;
	}
	
	public static boolean validar_gramatica(String producao) {
		boolean continuar;
		continuar = false;
		
		do {
			if (producao.equals("")) {
				break;
			}
			continuar = true;
			
			int fim;
			fim = producao.indexOf("\n");
			
			if (fim == -1) {
				fim = producao.length();
				continuar = false;
			}
			
			String naoTerminalString;
			naoTerminalString = producao.substring(0, fim);
			
			if (fim >= producao.length()) {
				producao = "";
				continuar = false;
			} else {
				producao = producao.substring(fim+1, producao.length());
			}
			
			String regex = "\\p{Upper}(\\d)* -> (((\\p{Upper} |(\\p{Lower} |\\W ))*|& )*[| ])*((\\p{Upper} |(\\p{Lower} |\\W ))*(\\p{Upper}|(\\p{Lower}|\\W))|&)";
		    Pattern p = Pattern.compile(regex);
		    Matcher m = p.matcher(naoTerminalString);
		    boolean b = m.matches();
		   
		    if (b == false) {
		    	return false;
		    }
		} while (continuar);
		
		return true;
	}
	
	/* Metodos Construtor */
	public Gramatica(String nome) {
		this.nome = nome;
		this.producoes = null;
		this.inicial = null;
		this.gramaticaPai = null;
		
		this.caracter = new Caracter();
		
		this.alfabeto = new ConjuntoAlfabeto();
		this.naoTerminais = new ConjuntoNaoTerminal();
		
		this.derivadas = new ArrayList<Gramatica>();
		this.semDireta = null;
		this.semIndireta = null;
	}
	public Gramatica() {
		this("gramatica_sem_nome");
	}
	public Gramatica(String nome, Gramatica gramatica) {
		this(nome);
		this.set_producoes( gramatica.get_producoes() );
	}
	public Gramatica(Gramatica gramatica) {
		this("gramatica_sem_nome", gramatica);
	}
	
	public void set_direta(Gramatica gramatica) {
		this.semDireta = gramatica;
	}
	public void set_indireta(Gramatica gramatica) {
		this.semIndireta = gramatica;
	}
	public Gramatica get_direta() {
		return this.semDireta;
	}
	public Gramatica get_indireta() {
		return this.semIndireta;
	}
	
	public void add_derivada(Gramatica gramatica) {
		this.derivadas.add(gramatica);
	}
	public ArrayList<Gramatica> get_derivadas() {
		return this.derivadas;
	}
	
	/* Metodos Add */
	public void add_alfabeto(char simboloTerminal) {
		this.alfabeto.add(simboloTerminal);
	}
	public NaoTerminal add_naoTerminal(NaoTerminal naoTerminal) {
		return this.naoTerminais.add(naoTerminal);
	}
	public boolean existe_nome_naoTerminal(String nome) {
		for (int c = 0; c < this.naoTerminais.size(); c++) {
			NaoTerminal naoTerminal;
			naoTerminal = this.naoTerminais.get(c);
			
			if (naoTerminal.get_simbolo().equals(nome)) {
				return true;
			}
		}
		
		return false;
	}
	
	/* Metodos Setter */
	public void set_nome(String nome) {
		this.nome = nome;
	}
	public void set_producoes(String producoes) {
		this.inicial = null;
		this.alfabeto = new ConjuntoAlfabeto();
		this.naoTerminais = new ConjuntoNaoTerminal();
		this.derivadas = new ArrayList<Gramatica>();
		
		this.producoes = producoes;
		this.gerar_naoTerminais(producoes);
	}
	public void set_inicial(NaoTerminal inicial) {
		this.inicial = inicial;
	}
	public void set_gramaticaPai(Gramatica gramaticaPai) {
		this.gramaticaPai = gramaticaPai;
	}
	public void set_conjunto_naoTerminal(ConjuntoNaoTerminal naoTerminais) {
		this.naoTerminais = naoTerminais;
	}
	public void set_naoTerminais(ConjuntoNaoTerminal naoTerminais) {
		this.naoTerminais = naoTerminais;
	}
	
	public void gerar_producoes_string() {
		this.producoes = "";
		
		NaoTerminal inicial;
		inicial = this.get_inicial();
		
		ConjuntoObject<Producao> producoes;
		producoes = inicial.get_producoes();
		
		this.producoes += inicial.get_simbolo()+" -> ";
		
		for (int i = 0; i < producoes.size(); i++) {
			this.producoes += producoes.get(i).get_producao();
			
			if (producoes.size()-1 != i) {
				this.producoes += " | ";
			}
		}
		if (this.naoTerminais.size() != 1) {
			this.producoes += "\n";
		}
		for (int c = 0; c < this.naoTerminais.size(); c++) {
			NaoTerminal naoTerminal;
			naoTerminal = this.naoTerminais.get(c);
			
			if (naoTerminal == inicial) {
				continue;
			}
			
			producoes = naoTerminal.get_producoes();
			
			this.producoes += naoTerminal.get_simbolo()+" -> ";
			
			for (int i = 0; i < producoes.size(); i++) {
				this.producoes += producoes.get(i).get_producao();
				
				if (producoes.size()-1 != i) {
					this.producoes += " | ";
				}
			}
			
			if (this.naoTerminais.size()-1 != c) {
				this.producoes += "\n";
			}
		}
	}
	
	/* Metodos Getter */
	public String get_nome() {
		return this.nome;
	}
	public String get_producoes() {
		return this.producoes;
	}
	public boolean get_compativeis(Gramatica gramatica) {
		return this.alfabeto.equals(gramatica.alfabeto);
	}
	public NaoTerminal get_inicial() {
		return this.inicial;
	}
	public ConjuntoAlfabeto get_alfabeto() {
		return this.alfabeto;
	}
	public ConjuntoNaoTerminal get_naoTerminais() {
		return this.naoTerminais;
	}
	public ConjuntoNaoTerminal get_ferteis() {
		ConjuntoNaoTerminal ferteis, naoTerminais;
		ferteis = new ConjuntoNaoTerminal();
		naoTerminais = this.naoTerminais;
		
		int tamanhoAntes = 0;
		
		do {
			tamanhoAntes = ferteis.size();
			
			/* Percorre todos os naoTerminais para buscar o conjunto
			 * de naoTerminais ferteis.
			 */
			for(int i = 0; i < naoTerminais.size(); i++) {
				NaoTerminal naoTerminal;
				naoTerminal = naoTerminais.get(i);
				
				ConjuntoObject<Producao> producoes;
				producoes = naoTerminal.get_producoes();
				
				boolean fertil;
				fertil = false;
				
				/* Para cada producao eh verificado se ela possui somente
				 * terminais ou se todos naoTerminais sao ferteis. Caso positivo,
				 * ela eh fertil e adicionada no conjunto de ferteis.
				 */
				for(int j = 0; j < producoes.size(); j++) {
					Producao producao;
					producao = producoes.get(j);
					
					boolean viva;
					viva = true;
					
					ConjuntoNaoTerminal naoTerminaisProducao;
					naoTerminaisProducao = producao.get_naoTerminais();
					
					//Se a producao soh possui terminais, entao naoTerminal eh fertil
					if(naoTerminaisProducao.isEmpty()) {
						ferteis.add(naoTerminal);
					} else {
						//Analisando se todos os nao terminais da producao sao ferteis
						for(int k = 0; k < naoTerminaisProducao.size(); k++) {
							NaoTerminal ntProducao = naoTerminaisProducao.get(k);
							
							// Se algum nao for fertil entao o naoTerminal nao eh fertil
							if(!ferteis.contains(ntProducao)) {
								viva = false;
								break;
							}
						}
					}
					if(viva) {
						fertil = true;
					}
				}
				if(fertil) {
					ferteis.add(naoTerminal);
				}
				
			}
			
			/* Enquanto novos naoTerminais ferteis estiverem sendo identificados,
			 * entao outros naoTerminais tambem poderao se tornar ferteis
			 * (a partir de suas producoes)
			 */
		} while(tamanhoAntes != ferteis.size());
		
		this.naoTerminais = ferteis;
		this.gerar_producoes_string();
		return ferteis;
	}
	
	protected void gerar_naoTerminais(String producoes) {
		int i;
		i = producoes.indexOf("\n");
		
		if (i > 0) {
			String naoTerminal, restante;
			naoTerminal = producoes.substring(0, i);
			restante = producoes.substring(i+1, producoes.length());
			
			this.gerar_naoTerminal(naoTerminal);
			this.gerar_naoTerminais(restante);
		} else {
			this.gerar_naoTerminal(producoes);
		}
	}
	protected void gerar_naoTerminal(String producoes) {
		// Producoes vazia sao invalidas
		if (producoes == "") {
			return;
		}
		
		int indiceFimSimboloNaoTerminal;
		indiceFimSimboloNaoTerminal = producoes.indexOf("->");
		
		// Nao representa um naoTerminal (ex.: "S -> *")
		if (indiceFimSimboloNaoTerminal < 0) {
			return;
		}
		
		String simboloNaoTerminal;
		simboloNaoTerminal = producoes.substring(0, indiceFimSimboloNaoTerminal-1);
		System.out.println(simboloNaoTerminal);
		NaoTerminal naoTerminal;
		naoTerminal = new NaoTerminal(this, simboloNaoTerminal);
		
		// O primeiro naoTerminal da gramatica eh o inicial
		if (this.get_inicial() == null) {
			this.set_inicial(naoTerminal);
		}
		
		naoTerminal = this.add_naoTerminal(naoTerminal);
		producoes = producoes.substring(indiceFimSimboloNaoTerminal+3, producoes.length());
		
		int i;
		do {
			// Seta producoes
			i = producoes.indexOf("|");
			
			String producao;
			producao = producoes;
			
			if (i > 0) {
				producao = producoes.substring(0,i);
			}
			
			Producao producaoNovo;
			producaoNovo = this.gerar_producao(producao);
			naoTerminal.add_producao(producaoNovo);
			
			producoes = producoes.substring(i+1, producoes.length());
		} while(i != -1);
	}
	protected Producao gerar_producao(String producao) {
		producao = this.formatar_producao(producao);
		
		Producao producaoNovo;
		producaoNovo = new Producao(this);
		producaoNovo.set_producao(producao);
		
		for(int j = 0; j < producao.length(); j++) {
			char simbol;
			simbol = producao.charAt(j);
			
			if (simbol == ' ') {
				continue;
			}
			
			/* Se for um terminal, adiciona no array / alfabeto e
			 * avanca o laco para proxima producao
			 */
			if (!caracter.is_letra_maiuscula(simbol)) {
				if (simbol != Gramatica.EPSILON) {
					this.add_alfabeto(simbol);
				}
				continue;
			}
			/* Caso contrario, ele eh um naoTerminal. O simbolo do
			 * naoTerminal pode possuir mais de um caracter, entao
			 * busca o seu fim e o adiciona na lista
			 */
			int simboloFim;
			simboloFim = producao.indexOf(" ", j);
			
			if (simboloFim < 0) {
				simboloFim = producao.length();
			}
			
			String simboloProducaoNaoTerminal;
			simboloProducaoNaoTerminal = producao.substring(j, simboloFim);
			
			NaoTerminal naoTerminalNovo;
			naoTerminalNovo = new NaoTerminal(this, simboloProducaoNaoTerminal);
			naoTerminalNovo = this.add_naoTerminal(naoTerminalNovo);
			
			j = simboloFim;
		}
		
		return producaoNovo;
	}
	protected String formatar_producao(String producao) {
		char primeiro, ultimo;
		primeiro = producao.charAt(0);
		ultimo = producao.charAt( producao.length()-1 );
		
		if (primeiro == ' ') {
			producao = producao.substring(1, producao.length());
		}
		if (ultimo == ' ') {
			producao = producao.substring(0, producao.length()-1);
		}
		
		return producao;
	}
}

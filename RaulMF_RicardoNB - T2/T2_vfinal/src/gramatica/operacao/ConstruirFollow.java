package gramatica.operacao;

import java.util.ArrayList;

import conjunto.Conjunto;
import conjunto.ConjuntoNaoTerminal;
import conjunto.ConjuntoObject;
import gramatica.Gramatica;
import gramatica.NaoTerminal;
import gramatica.Producao;

public class ConstruirFollow {
	public ConstruirFollow() {}
	
	public void construir(Gramatica gramatica) {
		ConstruirFirstT construirFirst;
		construirFirst = new ConstruirFirstT();
		construirFirst.construir_first(gramatica);
		
		ConjuntoNaoTerminal naoTerminais;
		naoTerminais = gramatica.get_naoTerminais();
		
		NaoTerminal inicial;
		inicial = gramatica.get_inicial();
		inicial.get_follows().add(Gramatica.FINAL_SENTENCA);
		
		boolean houveMudancas;
		do {
			houveMudancas = false;
			
			/* Obtem a soma do total de follows, de todos naoTerminais,
			 * antes de realizar qualquer operacao para obter follows
			 */
			int sizeFollowAntes;
			sizeFollowAntes = 0;
			for (int c = 0; c < naoTerminais.size(); c++) {
				NaoTerminal naoTerminal;
				naoTerminal = naoTerminais.get(c);
				
				sizeFollowAntes += naoTerminal.get_follows().size();
			}
			
			/* Para cada naoTerminal, eh feito a verificacao em todas suas
			 * producoes para detectar possiveis follows em algum dos possiveis
			 * naoTerminais que estejam em alguma das producoes
			 */
			for (int c = 0; c < naoTerminais.size(); c++) {
				NaoTerminal naoTerminal;
				naoTerminal = naoTerminais.get(c);
				
				Conjunto<Character> follows;
				follows = naoTerminal.get_follows();
				
				ConjuntoObject<Producao> producoes;
				producoes = naoTerminal.get_producoes();
				
				
				for (int i = 0; i < producoes.size(); i++) {
					Producao producao;
					producao = producoes.get(i);
					
					this.obter_follow(follows, producao);
				}
			}
			
			/* Obtem a soma do total de follows, de todos naoTerminais,
			 * depois de realizar operacoes de obtencao de follows
			 */
			int sizeFollowDepois;
			sizeFollowDepois = 0;
			for (int c = 0; c < naoTerminais.size(); c++) {
				NaoTerminal naoTerminal;
				naoTerminal = naoTerminais.get(c);
				
				sizeFollowDepois += naoTerminal.get_follows().size();
			}
			
			/* Se algum novo follow foi adicionado em algum naoTerminal,
			 * entao uma nova varredura, pois um novo follow pode surgir
			 * para algum naoTerminal a partir de um naoTerminal presente
			 * em uma de suas producoes
			 */
			if (sizeFollowAntes != sizeFollowDepois) {
				houveMudancas = true;
			}
		} while (houveMudancas);
		
	}
	protected void obter_follow(Conjunto<Character> follows, Producao producao) {
		ConjuntoNaoTerminal naoTerminaisEspera;
		naoTerminaisEspera = new ConjuntoNaoTerminal();
		
		ArrayList<Object> sequenciaSimbolos;
		sequenciaSimbolos = producao.get_sequencia_simbolos();
		
		/* Para cada simbolo da producao, do primeiro ao ultimo, ao encontrar
		 * um naoTerminal ele eh adicionado em um array. O simbolo seguinte a
		 * ele, se for terminal, sera seu follow, e se for naoTerminal, seus
		 * firsts serao follows. Caso algum first seja eplison, o naoTerminal
		 * permanece no array de espera, e caso contrario ele eh removido
		 * 
		 */
		for (int c = 0; c < sequenciaSimbolos.size(); c++) {
			Object simbolo;
			simbolo = sequenciaSimbolos.get(c);
			
			if (simbolo.getClass() != NaoTerminal.class) {
				char simboloChar;
				simboloChar = (char)simbolo;
				
				// Adiciona os terminais como follow dos naoTerminais em espera
				if (simboloChar != Gramatica.EPSILON) {
					while (naoTerminaisEspera.size() > 0) {
						NaoTerminal emEspera;
						emEspera = naoTerminaisEspera.get(0);
						emEspera.get_follows().add(simboloChar);
						
						naoTerminaisEspera.remove(emEspera);
					}
				}
				
				continue;
			}
			
			NaoTerminal simboloNaoTerminal;
			simboloNaoTerminal = (NaoTerminal)simbolo;
			
			Conjunto<Character> firsts;
			firsts = simboloNaoTerminal.get_firstsT();
			
			boolean possuiEplison;
			possuiEplison = false;
			
			/* Adiciona todos firsts do simboloNaoTerminal como follow
			 * dos naoTerminais em espera
			 * Ex: {A-> B
			 * 		B-> a | b}
			 * Entao A recebe {a,b} como follows
			 */
			for (int i = 0; i < firsts.size(); i++) {
				char first;
				first = firsts.get(i);
				
				// Caso algum dos first seja o eplison, entao permanece em espera
				if (first == Gramatica.EPSILON) {
					possuiEplison = true;
				}
				// Adiciona os first como follow
				else {
					for (int j = 0; j < naoTerminaisEspera.size(); j++) {
						NaoTerminal emEspera;
						emEspera = naoTerminaisEspera.get(j);
						emEspera.get_follows().add(first);
					}
				}
			}
			// Remove todos da espera caso o ultimo nao tivesse eplison como first
			if (!possuiEplison) {
				naoTerminaisEspera.removeAll();
			}
			naoTerminaisEspera.add(simboloNaoTerminal);
		}
		/* Caso existam naoTerminais em espera, eles recebem os follows
		 * do naoTerminal.
		 * Ex: {S-> ABC
		 * 		A-> &
		 * 		B-> &
		 * 		...}
		 * Entao A, B e C recebem os follows de S
		 */
		for (int c = 0; c < naoTerminaisEspera.size(); c++) {
			NaoTerminal emEspera;
			emEspera = naoTerminaisEspera.get(c);
			
			emEspera.get_follows().add(follows);
		}
	}
	
}

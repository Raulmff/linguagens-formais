package gramatica.operacao;

import java.util.ArrayList;

import conjunto.ConjuntoNaoTerminal;
import conjunto.ConjuntoObject;
import gramatica.Gramatica;
import gramatica.NaoTerminal;
import gramatica.Producao;

public class RecursaoEsquerda {
	protected ConstruirFirstNt construirFirstNt;
	protected ConjuntoNaoTerminal novosNaoTerminais;
	
	protected int contPortuga;
	
	public RecursaoEsquerda() {
		this.contPortuga = 0;
		this.construirFirstNt = new ConstruirFirstNt();
		
		this.novosNaoTerminais = new ConjuntoNaoTerminal();
	}
	
	public boolean get_possui_recursao_esquerda(Gramatica gramatica) {
		this.construirFirstNt.construir_first(gramatica);
		
		ConjuntoNaoTerminal naoTerminais;
		naoTerminais = gramatica.get_naoTerminais();
		
		for (int c = 0; c < naoTerminais.size(); c++) {
			NaoTerminal naoTerminal;
			naoTerminal = naoTerminais.get(c);
			
			ConjuntoNaoTerminal conjuntoFirstNt;
			conjuntoFirstNt = naoTerminal.get_firstsNt();
			
			if (conjuntoFirstNt.contains(naoTerminal)) {
				return true;
			}
		}
		
		return false;
	}
	public ConjuntoNaoTerminal get_naoTerminais_recursao(Gramatica gramatica) {
		this.construirFirstNt.construir_first(gramatica);
		
		ConjuntoNaoTerminal naoTerminaisRecursao;
		naoTerminaisRecursao = new ConjuntoNaoTerminal();
		
		ConjuntoNaoTerminal naoTerminais;
		naoTerminais = gramatica.get_naoTerminais();
		
		for (int c = 0; c < naoTerminais.size(); c++) {
			NaoTerminal naoTerminal;
			naoTerminal = naoTerminais.get(c);
			
			ConjuntoNaoTerminal conjuntoFirstNt;
			conjuntoFirstNt = naoTerminal.get_firstsNt();
			
			if (conjuntoFirstNt.contains(naoTerminal)) {
				naoTerminaisRecursao.add(naoTerminal);
			}
		}
		
		return naoTerminaisRecursao;
	}
	public void get_naoTerminais_recursoes(Gramatica gramatica, ConjuntoNaoTerminal naoTerminaisDireta, ConjuntoNaoTerminal naoTerminaisIndireta) {
		ConjuntoNaoTerminal naoTerminaisRecursao;
		naoTerminaisRecursao = this.get_naoTerminais_recursao(gramatica);
		
		for (int c = 0; c < naoTerminaisRecursao.size(); c++) {
			NaoTerminal naoTerminal;
			naoTerminal = naoTerminaisRecursao.get(c);
			
			ConjuntoObject<Producao> producoes;
			producoes = naoTerminal.get_producoes();
			
			for (int i = 0; i < producoes.size(); i++) {
				Producao producao;
				producao = producoes.get(i);
				
				ArrayList<Object> sequenciaSimbolos;
				sequenciaSimbolos = producao.get_sequencia_simbolos();
				
				Object simbolo;
				simbolo = sequenciaSimbolos.get(0);
				
				if (simbolo.getClass() == NaoTerminal.class) {
					NaoTerminal simboloNaoTerminal;
					simboloNaoTerminal = (NaoTerminal) simbolo;
					
					if (simboloNaoTerminal == naoTerminal) {
						naoTerminaisDireta.add(naoTerminal);
					} else {
						if (simboloNaoTerminal.get_firstsNt().contains(naoTerminal)) {
							naoTerminaisIndireta.add(naoTerminal);
						}
					}
				}
			}
		}
	}
	
	protected void remover_recursao_direta_naoTerminal(Gramatica gramatica, NaoTerminal naoTerminal) {
		ConjuntoObject<Producao> producoes, producoesComRecursao, producoesSemRecursao;
		producoesComRecursao = new ConjuntoObject<Producao>();
		producoesSemRecursao = new ConjuntoObject<Producao>();
		producoes = naoTerminal.get_producoes();
		
		/* Para cada producao eh verificado o primeiro simbolo da sequencia de simbolos.
		 * Se o primeiro for um naoTerminal(1) e igual ao naoTerminal(0) que deriva a producao,
		 * entao a producao eh removida do naoTerminal(0).
		 */
		for (int i = 0; i < producoes.size(); i++) {
			Producao producao;
			producao = producoes.get(i);
			
			ArrayList<Object> sequenciaSimbolos;
			sequenciaSimbolos = producao.get_sequencia_simbolos();
			
			Object simbolo;
			simbolo = sequenciaSimbolos.get(0);
			
			if (simbolo.getClass() == NaoTerminal.class) {
				NaoTerminal simboloNaoTerminal;
				simboloNaoTerminal = (NaoTerminal) simbolo;
				
				if (simboloNaoTerminal == naoTerminal) {
					producoesComRecursao.add(producao);
					
					producoes.remove(producao);
					i--;
				} else {
					producoesSemRecursao.add(producao);
				}
			} else {
				producoesSemRecursao.add(producao);
			}
		}
		
		// Se nao houver recursao entao o naoTerminal continua como era
		if (producoesComRecursao.size() == 0) {
			return;
		}
		
		NaoTerminal novoNaoTerminal;
		novoNaoTerminal = new NaoTerminal(gramatica); // ARRUMAR ISSO DEPOIS
		this.novosNaoTerminais.add(novoNaoTerminal);
		
		/* Para cada producao sem recursao eh adicionado o simbolo do novo naoTerminal
		 * no final da sequencia de simbolos
		 */
		for (int i = 0; i < producoesSemRecursao.size(); i++) {
			Producao producao;
			producao = producoesSemRecursao.get(i);
			
			// Se a producao eh epsilon entao remove o epsilong
			if (producao.get_producao().equals("&")) {
				producao.get_sequencia_simbolos().remove(0);
			}
			
			producao.add_simbolo(novoNaoTerminal);
			producao.gerar_producao();
		}
		
		/* Para cada producao com recursao eh adicionado o simbolo do novo naoTerminal
		 * no final da sequencia de simbolos e removido o naoTerminal que deriva a
		 * recursao/producao
		 */
		for (int i = 0; i < producoesComRecursao.size(); i++) {
			Producao producao;
			producao = producoesComRecursao.get(i);
			producao.gerar_producao();
			
			// Remove o naoTerminal recursivo
			producao.get_sequencia_simbolos().remove(0);
			
			// Adiciona o novoNaoTerminal
			producao.add_simbolo(novoNaoTerminal);
			producao.gerar_producao();
			
			novoNaoTerminal.add_producao(producao);
		}
		
		// Cria uma nova producao que deriva epsilon e add no novo naoTerminal
		Producao producaoEpsilon;
		producaoEpsilon = new Producao(novoNaoTerminal.get_gramatica());
		producaoEpsilon.add_simbolo('&');
		producaoEpsilon.set_producao("&");
		producaoEpsilon.gerar_producao();
		
		novoNaoTerminal.add_producao(producaoEpsilon);
	}
	
	public Gramatica remover_recursao_diretas(Gramatica gramatica) {
		return this.remover_recursao_diretas(gramatica, true);
	}
	protected Gramatica remover_recursao_diretas(Gramatica gramatica, boolean criarNovaGramatica) {
		Gramatica novaGramatica;
		
		if (criarNovaGramatica) {
			novaGramatica = new Gramatica(Gramatica.get_novo_nome(), gramatica);
		} else {
			novaGramatica = gramatica;
		}
		
		ConjuntoNaoTerminal naoTerminaisDireta;
		naoTerminaisDireta = new ConjuntoNaoTerminal();
		this.get_naoTerminais_recursoes(novaGramatica, naoTerminaisDireta, new ConjuntoNaoTerminal());
		
		for (int c = 0; c < naoTerminaisDireta.size(); c++) {
			NaoTerminal naoTerminal;
			naoTerminal = naoTerminaisDireta.get(c);
			
			ConjuntoObject<Producao> producoes, producoesComRecursao, producoesSemRecursao;
			producoesComRecursao = new ConjuntoObject<Producao>();
			producoesSemRecursao = new ConjuntoObject<Producao>();
			producoes = naoTerminal.get_producoes();
			
			for (int i = 0; i < producoes.size(); i++) {
				Producao producao;
				producao = producoes.get(i);
				
				ArrayList<Object> sequenciaSimbolos;
				sequenciaSimbolos = producao.get_sequencia_simbolos();
				
				Object simbolo;
				simbolo = sequenciaSimbolos.get(0);
				
				if (simbolo.getClass() == NaoTerminal.class) {
					NaoTerminal simboloNaoTerminal;
					simboloNaoTerminal = (NaoTerminal) simbolo;
					
					if (simboloNaoTerminal == naoTerminal) {
						producoesComRecursao.add(producao);
						
						producoes.remove(producao);
						i--;
						
					} else {
						producoesSemRecursao.add(producao);
					}
				} else {
					producoesSemRecursao.add(producao);
				}
			}
			
			NaoTerminal novoNaoTerminal;
			novoNaoTerminal = new NaoTerminal(novaGramatica); // ARRUMAR ISSO DEPOIS
			
			novaGramatica.add_naoTerminal(novoNaoTerminal);
			
			for (int i = 0; i < producoesSemRecursao.size(); i++) {
				Producao producao;
				producao = producoesSemRecursao.get(i);
				
				producao.add_simbolo(novoNaoTerminal);
				producao.gerar_producao();
			}
			
			for (int i = 0; i < producoesComRecursao.size(); i++) {
				Producao producao;
				producao = producoesComRecursao.get(i);
				
				producao.add_simbolo(novoNaoTerminal);
				novoNaoTerminal.add_producao(producao);
				
				producao.get_sequencia_simbolos().remove(0);
				producao.gerar_producao();
			}
			
			Producao producaoEpsilon;
			producaoEpsilon = new Producao(novoNaoTerminal.get_gramatica());
			producaoEpsilon.add_simbolo('&');
			producaoEpsilon.set_producao("&");
			
			novoNaoTerminal.add_producao(producaoEpsilon);
			this.novosNaoTerminais.add(novoNaoTerminal);
		}
		return novaGramatica;
	}
	public Gramatica remover_recursao_indireta(Gramatica gramatica) {
		this.novosNaoTerminais = new ConjuntoNaoTerminal();
		
		// Clona a gramatica e aponta a antiga como pai da clone
		Gramatica novaGramatica;
		novaGramatica = new Gramatica(Gramatica.get_novo_nome(), gramatica);
		novaGramatica.set_gramaticaPai(gramatica);
		
		ConjuntoNaoTerminal naoTerminais;
		naoTerminais = novaGramatica.get_naoTerminais();
		
		// Remove as recursoes diretas de todos naoTerminais
		this.remover_recursao_diretas(novaGramatica, false);
		
		for (int i = 0; i < naoTerminais.size(); i++) {
			NaoTerminal naoTerminalI;
			naoTerminalI = naoTerminais.get(i);
			
			if (this.novosNaoTerminais.contains(naoTerminalI)) {
				continue;
			}
			
			/* Para todo naoTerminal(I) verifica-se se existe alguma producao cujo primeiro
			 * simbolo seja um naoTerminal(J), o qual esteja em uma posicao anterior a do
			 * naoTerminal(I) na lista de naoTerminais da gramatica. Para essas producoes,
			 * elas sao removidas do naoTerminal(I) e cria-se outras. Essas novas producoes
			 * serao iguais a removida, exceto que, no lugar do primeiro simbolo
			 * (naoTerminal(J)), elas terao uma das producoes do naoTerminal(J). Ou seja,
			 * sera criada uma novaProducao para cada producao do naoTerminal(J). Por fim,
			 * cada novaProducao pode gerar recursoes diretas, entao remove-se, para cada
			 * naoTerminal(I), as possiveis recursoes diretas.
			 * Ex:
			 * Antes:
			 * S -> A a B | ba
			 * A -> S a
			 * 
			 * Depois (novasProducoes):
			 * S -> A a B | ba
			 * A -> A a B a | baa
			 * 
			 * Depois (remocao de recursoes diretas):
			 * S -> A a B | ba
			 * A -> baa A1
			 * A1 -> a B a A1 | &
			 */
			for (int j = 0; j < i; j++) {
				NaoTerminal naoTerminalJ;
				naoTerminalJ = naoTerminais.get(j);
				
				ConjuntoObject<Producao> producoesI;
				producoesI = naoTerminalI.get_producoes();
				
				for (int ii = 0; ii < producoesI.size(); ii++) {
					Producao producaoI;
					producaoI = producoesI.get(ii);
					
					NaoTerminal naoTerminalProducao;
					naoTerminalProducao = producaoI.get_primeiro_simbolo_naoTerminal();
					
					/* Se o primeiro simbolo da producao for um naoTerminal e
					 * esse naoTerminal eh o naoTerminal(J) entao remove essa
					 * producao e cria novas no lugar
					 */
					if (naoTerminalProducao != null) {
						if (naoTerminalProducao == naoTerminalJ) {
							// Remove a producao do naoTerminal(I)
							producoesI.remove(producaoI);
							producaoI.get_sequencia_simbolos().remove(0);
							producaoI.gerar_producao();
							ii--;
							
							// Producao(I) sem o primeiro simbolo (naoTerminal(J))
							String producaoIAntigaString;
							producaoIAntigaString = producaoI.get_producao();
							
							ConjuntoObject<Producao> producoesJ;
							producoesJ = naoTerminalJ.get_producoes();
							
							/* Cria uma novaProducao para cada producao do naoTerminal(J)
							 * e adiciona no naoTerminal(I)
							 */
							for (int jj = 0; jj < producoesJ.size(); jj++) {
								Producao producaoJ;
								producaoJ = producoesJ.get(jj);
								
								String novaProducaoString;
								novaProducaoString = "";
								
								// Desconsidera producoes(J) que derivem epsilon
								if (!producaoJ.get_producao().equals(Gramatica.EPSILON+"")) {
									novaProducaoString += producaoJ.get_producao()+" ";
								}
								
								novaProducaoString += producaoIAntigaString;
								
								Producao novaProducao;
								novaProducao = new Producao(naoTerminalI.get_gramatica());
								novaProducao.set_producao(novaProducaoString);
								
								// Adiciona a novaProducao no naoTerminal(I)
								producoesI.add(novaProducao);
							}
						}
					}
				}
			}
			
			// Remove possiveis novas recursoes diretas do naoTerminal(I)
			this.remover_recursao_direta_naoTerminal(novaGramatica, naoTerminalI);
		}
		
		// Adiciona os novos naoTerminais, criados pelas remocoes das recursoes diretas
		for (int c = 0; c < this.novosNaoTerminais.size(); c++) {
			NaoTerminal naoTerminal;
			naoTerminal = this.novosNaoTerminais.get(c);
			
			novaGramatica.add_naoTerminal(naoTerminal);
		}
		
		return novaGramatica;
	}
}

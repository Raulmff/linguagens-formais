package gramatica.operacao;

import java.util.ArrayList;

import conjunto.Conjunto;
import conjunto.ConjuntoNaoTerminal;
import conjunto.ConjuntoObject;
import gramatica.Gramatica;
import gramatica.NaoTerminal;
import gramatica.Producao;
import util.Caracter;

public class TransformarGramaticaPropria {
	protected Caracter caracter;
	protected String conjuntoNe, conjuntoNa, conjuntoNf, conjuntoVi;
	
	public TransformarGramaticaPropria() {
		this.caracter = new Caracter();
		this.conjuntoNe = "";
		this.conjuntoNa = "";
		this.conjuntoNf = "";
		this.conjuntoVi = "";
	}
	
	public String get_conjuntoNe() {
		return this.conjuntoNe;
	}
	public String get_conjuntoNa() {
		return this.conjuntoNa;
	}
	public String get_conjuntoNf() {
		return this.conjuntoNf;
	}
	public String get_conjuntoVi() {
		return this.conjuntoVi;
	}
	
	public ArrayList<Gramatica> get_gramatica_propria(Gramatica gramaticaOriginal) {
		ArrayList<Gramatica> gramaticasIntermediarias;
		gramaticasIntermediarias = new ArrayList<Gramatica>();
		
		//Buscando gramatica epsilon livre
		
		Gramatica gramaticaEpsilonLivre;
		gramaticaEpsilonLivre = this.get_epsilon_livre( gramaticaOriginal);
		
		//Buscando gramatica sem ciclos
		Gramatica gramaticaCiclosLivre;
		gramaticaCiclosLivre = this.get_ciclos_livre(gramaticaEpsilonLivre);
		
		// Gramatica Fertil
		Gramatica gramaticaFertil;
		gramaticaFertil = this.get_inferteis_livre(gramaticaCiclosLivre);
		
		// Gramatica Alcansavel
		Gramatica gramaticaAlcalsaveis;
		gramaticaAlcalsaveis = this.get_inalcansaveis_livre(gramaticaFertil);
		
		Gramatica gramaticaInutil;
		gramaticaInutil = this.get_producoesInuteis_livre(gramaticaAlcalsaveis);
		
		gramaticasIntermediarias.add(gramaticaEpsilonLivre);
		gramaticasIntermediarias.add(gramaticaCiclosLivre);
		gramaticasIntermediarias.add(gramaticaFertil);
		gramaticasIntermediarias.add(gramaticaAlcalsaveis);
		gramaticasIntermediarias.add(gramaticaInutil);
		
		gramaticaOriginal.add_derivada(gramaticaEpsilonLivre);
		gramaticaOriginal.add_derivada(gramaticaCiclosLivre);
		gramaticaOriginal.add_derivada(gramaticaFertil);
		gramaticaOriginal.add_derivada(gramaticaInutil);
		
		return gramaticasIntermediarias;
	}
	
	public Gramatica get_inferteis_livre(Gramatica gramatica) {
		Gramatica novaGramatica;
		novaGramatica = new Gramatica(Gramatica.get_novo_nome(), gramatica);
		
		ConjuntoNaoTerminal ferteis, naoTerminais;
		ferteis = new ConjuntoNaoTerminal();
		naoTerminais = novaGramatica.get_naoTerminais();
		
		int tamanhoAntes = 0;
		
		do {
			tamanhoAntes = ferteis.size();
			
			/* Percorre todos os naoTerminais para buscar o conjunto
			 * de naoTerminais ferteis.
			 */
			for(int i = 0; i < naoTerminais.size(); i++) {
				NaoTerminal naoTerminal;
				naoTerminal = naoTerminais.get(i);
				
				ConjuntoObject<Producao> producoes;
				producoes = naoTerminal.get_producoes();
				
				boolean fertil;
				fertil = false;
				
				/* Para cada producao eh verificado se ela possui somente
				 * terminais ou se todos naoTerminais sao ferteis. Caso positivo,
				 * ela eh fertil e adicionada no conjunto de ferteis.
				 */
				for(int j = 0; j < producoes.size(); j++) {
					Producao producao;
					producao = producoes.get(j);
					
					boolean viva;
					viva = true;
					
					ConjuntoNaoTerminal naoTerminaisProducao;
					naoTerminaisProducao = producao.get_naoTerminais();
					
					// Se a producao soh possui terminais, entao naoTerminal eh fertil
					if(naoTerminaisProducao.isEmpty()) {
						ferteis.add(naoTerminal);
					} else {
						// Analisando se todos os nao terminais da producao sao ferteis
						for(int k = 0; k < naoTerminaisProducao.size(); k++) {
							NaoTerminal ntProducao = naoTerminaisProducao.get(k);
							
							// Se algum nao for fertil entao o naoTerminal nao eh fertil
							if(!ferteis.contains(ntProducao)) {
								viva = false;
								break;
							}
						}
					}
					if(viva) {
						fertil = true;
					}
				}
				if(fertil) {
					ferteis.add(naoTerminal);
				}
				
			}
			
			/* Enquanto novos naoTerminais ferteis estiverem sendo identificados,
			 * entao outros naoTerminais tambem poderao se tornar ferteis
			 * (a partir de suas producoes)
			 */
		} while(tamanhoAntes != ferteis.size());
		
		this.conjuntoNf = "";
		
		for (int c = 0; c < ferteis.size(); c++) {
			if (!this.conjuntoNf.equals("")) {
				this.conjuntoNf += ", ";
			}
			
			NaoTerminal fertil;
			fertil = ferteis.get(c);
			
			conjuntoNf += fertil.get_simbolo();
		}
		
		this.conjuntoNf = "Nf = {"+this.conjuntoNf+"}";
		
		novaGramatica.set_naoTerminais(ferteis);
		novaGramatica.gerar_producoes_string();
		
		return this.get_producoesInuteis_livre(novaGramatica);
	}
	public Gramatica get_producoesInuteis_livre(Gramatica gramatica) {
		Gramatica novaGramatica;
		novaGramatica = new Gramatica( Gramatica.get_novo_nome() );
		novaGramatica.set_producoes( gramatica.get_producoes() );
		
		ConjuntoNaoTerminal ferteis;
		ferteis = novaGramatica.get_ferteis();
		
		ConjuntoNaoTerminal naoTerminais;
		naoTerminais = novaGramatica.get_naoTerminais();
		
		for (int c = 0; c < naoTerminais.size(); c++) {
			NaoTerminal naoTerminal;
			naoTerminal = naoTerminais.get(c);
			
			ConjuntoObject<Producao> producoes;
			producoes = naoTerminal.get_producoes();
			
			for (int i = 0; i < producoes.size(); i++) {
				Producao producao;
				producao = producoes.get(i);
				
				ConjuntoObject<NaoTerminal> naoTerminaisProducao;
				naoTerminaisProducao = producao.get_naoTerminais();
				
				boolean simboloUtil;
				simboloUtil = true;
				
				for (int j = 0; j < naoTerminaisProducao.size(); j++) {
					NaoTerminal naoTerminalProducao;
					naoTerminalProducao = naoTerminaisProducao.get(j);
					
					if (!ferteis.contains(naoTerminalProducao)) {
						simboloUtil = false;
						break;
					}
				}
				
				if (!simboloUtil) {
					producoes.remove(producao);
					i--;
				}
			}
			
		}
		
		novaGramatica.gerar_producoes_string();
		return novaGramatica;
	}
	public Gramatica get_inalcansaveis_livre(Gramatica gramatica) {
		Gramatica novaGramatica;
		novaGramatica = new Gramatica(gramatica);
		
		NaoTerminal naoTerminalInicial;
		naoTerminalInicial = novaGramatica.get_inicial();
		
		ConjuntoNaoTerminal naoTerminaisAlcansaveis;
		naoTerminaisAlcansaveis = new ConjuntoNaoTerminal();
		naoTerminaisAlcansaveis.add(naoTerminalInicial);
		
		String conjuntoViString;
		conjuntoViString = "";
		
		for (int c = 0; c < naoTerminaisAlcansaveis.size(); c++) {
			NaoTerminal naoTerminal;
			naoTerminal = naoTerminaisAlcansaveis.get(c);
			
			System.out.println(naoTerminal.get_simbolo());
			
			ConjuntoObject<Producao> producoes;
			producoes = naoTerminal.get_producoes();
			
			
			conjuntoViString = "";
			
			for (int i = 0; i < producoes.size(); i++) {
				Producao producao;
				producao = producoes.get(i);
				
				ConjuntoNaoTerminal naoTerminaisProducao;
				naoTerminaisProducao = producao.get_naoTerminais();
				
				Conjunto<Character> terminaisProducao;
				terminaisProducao = producao.get_terminais();
				
				for (int j = 0; j < naoTerminaisProducao.size(); j++) {
					if (!conjuntoViString.equals("")) {
						conjuntoViString += ", ";
					}
					NaoTerminal naoTerminalProducao;
					naoTerminalProducao = naoTerminaisProducao.get(j);
					
					conjuntoViString += naoTerminalProducao.get_simbolo();
				}
				
				for (int j = 0; j < terminaisProducao.size(); j++) {
					if (!conjuntoViString.equals("")) {
						conjuntoViString += ", ";
					}
					
					char terminalProducao;
					terminalProducao = terminaisProducao.get(j);
					
					conjuntoViString += terminalProducao;
				}
				
				naoTerminaisAlcansaveis.add(naoTerminaisProducao);
			}
			
			conjuntoViString = "Vi = {"+conjuntoViString+"}";
		}
		
		this.conjuntoVi = conjuntoViString;
		
		novaGramatica.set_naoTerminais(naoTerminaisAlcansaveis);
		novaGramatica.gerar_producoes_string();
		return novaGramatica;
	}
	public Gramatica get_ciclos_livre(Gramatica gramatica) {
		Gramatica novaGramatica;
		novaGramatica = new Gramatica(Gramatica.get_novo_nome(), gramatica);
		
		ConjuntoNaoTerminal naoTerminais;
		naoTerminais = novaGramatica.get_naoTerminais();
		
		String conjuntoNaString;
		conjuntoNaString = "";
		
		int sizeNaAntes, sizeNaDepois;
		do {
			this.conjuntoNa = "";
			
			sizeNaAntes = 0;
			for (int c = 0; c < naoTerminais.size(); c++) {
				NaoTerminal naoTerminal;
				naoTerminal = naoTerminais.get(c);
				
				sizeNaAntes += naoTerminal.get_conjuntoNa().size();
			}
			
			for (int c = 0; c < naoTerminais.size(); c++) {
				NaoTerminal naoTerminal;
				naoTerminal = naoTerminais.get(c);
				
				ConjuntoObject<Producao> producoes;
				producoes = naoTerminal.get_producoes();
				
				for (int i = 0; i < producoes.size(); i++) {
					Producao producao;
					producao = producoes.get(i);
					
					ConjuntoNaoTerminal naoTerminaisProducao;
					naoTerminaisProducao = producao.get_naoTerminais();
					
					Conjunto<Character> terminais;
					terminais = producao.get_terminais();
					
					if (naoTerminaisProducao.size() != 1 || terminais.size() != 0) {
						continue;
					}
					
					NaoTerminal naoTerminalProducao;
					naoTerminalProducao = naoTerminaisProducao.get(0);
					
					naoTerminal.add_naoTerminalNa( naoTerminalProducao.get_conjuntoNa() );
				}
			}
			
			sizeNaDepois = 0;
			for (int c = 0; c < naoTerminais.size(); c++) {
				NaoTerminal naoTerminal;
				naoTerminal = naoTerminais.get(c);
				
				ConjuntoNaoTerminal conjuntoNa;
				conjuntoNa = naoTerminal.get_conjuntoNa();
				
				sizeNaDepois += conjuntoNa.size();
				
				conjuntoNaString = "Na"+naoTerminal.get_simbolo()+" = {";
				
				for (int i = 0; i < conjuntoNa.size(); i++) {
					NaoTerminal naoTerminalNa;
					naoTerminalNa = conjuntoNa.get(i);
					
					conjuntoNaString += naoTerminalNa.get_simbolo();
					
					if (i != conjuntoNa.size()-1) {
						conjuntoNaString += ", ";
					}
				}
				conjuntoNaString += "}";
				
				this.conjuntoNa += conjuntoNaString+"\n";
			}
			
		} while(sizeNaAntes != sizeNaDepois);
		
		for (int c = 0; c < naoTerminais.size(); c++) {
			NaoTerminal naoTerminal;
			naoTerminal = naoTerminais.get(c);
			
			ConjuntoObject<Producao> producoes;
			producoes = naoTerminal.get_producoes();
			
			for (int i = 0; i < producoes.size(); i++) {
				Producao producao;
				producao = producoes.get(i);
				
				ConjuntoNaoTerminal naoTerminaisProducao;
				naoTerminaisProducao = producao.get_naoTerminais();
				
				Conjunto<Character> terminaisProducao;
				terminaisProducao = producao.get_terminais();
				
				if (naoTerminaisProducao.size() != 1 || terminaisProducao.size() != 0) {
					continue;
				}
				
				producoes.remove(producao);
				i--;
			}
		}
		
		for (int c = 0; c < naoTerminais.size(); c++) {
			NaoTerminal naoTerminal;
			naoTerminal = naoTerminais.get(c);
			
			ConjuntoNaoTerminal conjuntoNa;
			conjuntoNa = naoTerminal.get_conjuntoNa();
			
			for (int i = 0; i < conjuntoNa.size(); i++) {
				NaoTerminal naoTerminalNa;
				naoTerminalNa = conjuntoNa.get(i);
				
				ConjuntoObject<Producao> producoesNa;
				producoesNa = naoTerminalNa.get_producoes();
				
				for (int j = 0; j < producoesNa.size(); j++) {
					Producao producao;
					producao = producoesNa.get(j);
					
					naoTerminal.add_producao(producao.clone());
				}
			}
		}
		
		//this.printar_o_role(naoTerminais);
		novaGramatica.gerar_producoes_string();
		return novaGramatica;
	}
	
	public Gramatica get_epsilon_livre(Gramatica gramatica) {
		// Clona gramatica
		Gramatica novaGramatica;
		novaGramatica = new Gramatica(Gramatica.get_novo_nome(), gramatica);
		
		// Gera o conjunto Ne
		ConjuntoNaoTerminal conjuntoNe;
		conjuntoNe = this.get_epsilon_livre_conjuntoNe(novaGramatica);
		
		// Obtem os naoTerminais da gramatica
		ConjuntoNaoTerminal naoTerminais;
		naoTerminais = novaGramatica.get_naoTerminais();
		
		/* Para cada naoTerminal da gramatica sao percorridas suas
		 * producoes. Para cada producao eh verificado a existencia
		 * de possiveis novas producoes, geradas ao remover o epsilon
		 * de cada naoTerminal do conjunto Ne
		 */
		for (int c = 0; c < naoTerminais.size(); c++) {
			NaoTerminal naoTerminal;
			naoTerminal = naoTerminais.get(c);
			
			// Producoes do naoTerminal
			ConjuntoObject<Producao> producoes;
			producoes = naoTerminal.get_producoes();
			
			int sizeProducoes;
			sizeProducoes = producoes.size();
			
			/* Para cada producao tenta-se criar novas, derivadas da producao,
			 * ao remover um ou mais naoTerminais contidos em Ne. As producoes
			 * que derivam epsilon sao removidas
			 * 
			 */
			for (int i = 0; i < sizeProducoes; i++) {
				Producao producao;
				producao = producoes.get(i);
				
				// Remove a producao caso derive epsilon
				if (producao.get_producao().equals(Gramatica.EPSILON+"")) {
					producoes.remove(producao);
					i--;
					sizeProducoes--;
					continue;
				}
				
				/* Percorre a producao na tentativa de gerar novasProducoes,
				 * ao remover naoTerminais contidos em Ne, e adiciona essas
				 * novas producoes no naoTerminal que possui a producao que
				 * derivou as novas
				 */
				ConjuntoObject<Producao> novasProducoes;
				novasProducoes = this.get_epsilon_livre_novas_producoes(conjuntoNe, producao);
				
				naoTerminal.add_producoes(novasProducoes);
			}
		}
		
		// Cria novo estado inicial, caso o atual possua epsilon
		this.get_epsilon_livre_novoInicial(novaGramatica, conjuntoNe);
		
		novaGramatica.gerar_producoes_string();
		return novaGramatica;
	}
	protected void get_epsilon_livre_novoInicial(Gramatica novaGramatica, ConjuntoNaoTerminal conjuntoNe) {
		NaoTerminal inicial;
		inicial = novaGramatica.get_inicial();
		
		if (conjuntoNe.contains(inicial)) {
			NaoTerminal novoInicial;
			novoInicial = new NaoTerminal(novaGramatica); // MUDAR ISSO AQUI DEPOIS <<<<<<<<<
			
			// Cria uma producao que deriva epsilon
			Producao producaoEpsilon;
			producaoEpsilon = new Producao(novoInicial.get_gramatica());
			producaoEpsilon.add_simbolo(Gramatica.EPSILON);
			
			// Cria uma producao que deriva o antigo inicial
			Producao producaoAntigoInicial;
			producaoAntigoInicial = new Producao(novoInicial.get_gramatica());
			producaoAntigoInicial.add_simbolo(inicial);
			
			novoInicial.add_producao(producaoEpsilon);
			novoInicial.add_producao(producaoAntigoInicial);
			
			// Adiciona o novo inicial na gramatica
			novaGramatica.set_inicial(novoInicial);
			novaGramatica.add_naoTerminal(novoInicial);
		}
	}
	protected ConjuntoNaoTerminal get_epsilon_livre_conjuntoNe(Gramatica novaGramatica) {
		// Conjunto Ne: simbolos que derivam direta/indiretamente eplison
		ConjuntoNaoTerminal naoTerminaisNe;
		naoTerminaisNe = new ConjuntoNaoTerminal();
		
		ConjuntoNaoTerminal naoTerminais;
		naoTerminais = novaGramatica.get_naoTerminais();
		
		this.conjuntoNe = "";
		
		int sizeLivresAntes;
		do {
			sizeLivresAntes = naoTerminaisNe.size();
			
			for (int c = 0; c < naoTerminais.size(); c++) {
				NaoTerminal naoTerminal;
				naoTerminal = naoTerminais.get(c);
				
				// Caso naoTerminal ja esteja em Ne, entao sai da iteracao
				if (naoTerminaisNe.contains(naoTerminal)) {
					continue;
				}
				
				ConjuntoObject<Producao> producoes;
				producoes = naoTerminal.get_producoes();
				
				/* Para cada producao do naoTerminal, verifica se ele deriva
				 * eplison direta/indiretamente
				 * 
				 */
				for (int i = 0; i < producoes.size(); i++) {
					Producao producao;
					producao = producoes.get(i);
					
					Conjunto<Character> simbolosTerminais;
					simbolosTerminais = producao.get_terminais();
					
					// Deriva epsilon diretamente
					if (simbolosTerminais.contains(Gramatica.EPSILON)) {
						naoTerminaisNe.add(naoTerminal);
					}
					
					/* Se a producao nao possuir terminais, entao pelos
					 * naoTerminais pode ser derivado eplison
					 */
					if (producao.get_terminais().size() == 0) {
						boolean possuiEplison;
						possuiEplison = true;
						
						ConjuntoNaoTerminal naoTerminaisProducao;
						naoTerminaisProducao = producao.get_naoTerminais();
						
						/* Para cada naoTerminal da producao eh verificado se
						 * esta contido no conjunto Ne (deriva direta/indiretamente
						 * o eplison). Se algum deles nao estiver contido entao a producao
						 * nao vai derivar eplison
						 */
						for (int j = 0; j < naoTerminaisProducao.size(); j++) {
							NaoTerminal naoTerminalProducao;
							naoTerminalProducao = naoTerminaisProducao.get(j);
							
							if (!naoTerminaisNe.contains(naoTerminalProducao)) {
								possuiEplison = false;
								break;
							}
						}
						
						if (possuiEplison) {
							naoTerminaisNe.add(naoTerminal);
						}
					}
				}
			}
			
			/* Constroi o conjuntoNe intermediario, percorrendo todos os
			 * naoTerminais do conjuntoNe e concatenando seu simbolo em
			 * uma string. A string final sera a representacao do conjuntoNe
			 */
			if (sizeLivresAntes < naoTerminaisNe.size()) {
				conjuntoNe = "Ne = {";
				
				for (int c = 0; c < naoTerminaisNe.size(); c++) {
					NaoTerminal naoTerminalNe;
					naoTerminalNe = naoTerminaisNe.get(c);
					
					conjuntoNe += naoTerminalNe.get_simbolo();
					
					if (c != naoTerminaisNe.size()-1) {
						conjuntoNe += ", ";
					}
				}
				conjuntoNe += "}";
			}
		// Enquanto novos naoTerminais forem add no conjunto Ne
		} while (sizeLivresAntes < naoTerminaisNe.size());
		
		return naoTerminaisNe;
	}
	protected ConjuntoObject<Producao> get_epsilon_livre_novas_producoes(ConjuntoNaoTerminal conjuntoNe, Producao producao) {
		String producaoString;
		producaoString = producao.get_producao();
		
		ConjuntoObject<Producao> novasProducoes;
		novasProducoes = new ConjuntoObject<Producao>();
		
		/* Percorre todos os simbolos da producao, na tentativa de encontrar
		 * um naoTerminal que esteja contido no conjunto Ne. Ao encontrar,
		 * divide a producao em duas e cria uma nova com as duas partes da
		 * antiga, com excessao do naoTerminal contido no conjunto Ne.
		 */
		for (int c = 0; c < producaoString.length(); c++) {
			char simbolo;
			simbolo = producaoString.charAt(c);
			
			// Desconsidera simbolos terminais
			if (!this.caracter.is_letra_maiuscula(simbolo)) {
				continue;
			}
			
			// Encontrou um naoTerminal
			for (int j = 0; j < conjuntoNe.size(); j++) {
				NaoTerminal naoTerminalNe;
				naoTerminalNe = conjuntoNe.get(j);
				
				// NaoTerminalNe
				String simboloNaoTerminalNe;
				simboloNaoTerminalNe = naoTerminalNe.get_simbolo();
				
				int fimNaoTerminal;
				fimNaoTerminal = producaoString.indexOf(" ", c);
				
				if (fimNaoTerminal < 0) {
					fimNaoTerminal = producaoString.length();
				}
				
				// NaoTerminal da producao
				String possivelSimboloEquals;
				possivelSimboloEquals = producaoString.substring(c, fimNaoTerminal);
				
				/* Se o naoTerminalNe eh o mesmo naoTerminal da producao,
				 * entao quebra a producao em duas e forma uma nova, que
				 * nao possuira esse naoTerminal
				 */
				if (possivelSimboloEquals.equals(simboloNaoTerminalNe)) {
					String novaProducaoString;
					novaProducaoString = "";
					novaProducaoString += producaoString.substring(0, c);
					novaProducaoString += producaoString.substring(fimNaoTerminal);
					
					/* Caso em que a novaProducao eh vazia.
					 * Ex:
					 * producao: A
					 * novaProducao: 
					 */
					if (novaProducaoString.equals("")) {
						continue;
					}
					
					Producao novaProducao;
					novaProducao = new Producao(naoTerminalNe.get_gramatica());
					novaProducao.set_producao(novaProducaoString);
					
					/* Gera, na novaProducao, o array de simbolos que representa
					 * a sequencia de simbolos
					 */
					//this.get_epsilon_livre_producao_sequenciaSimbolos(producao, novaProducao);
					novasProducoes.add(novaProducao);
				}
			}
		}
		
		/* A novaProducao pode conter outros simbolos contidos em Ne
		 * Ex:
		 * producao: S -> a S b S.
		 * novaProducao: S -> a b S
		 */
		int sizeNovasProducoes;
		sizeNovasProducoes = novasProducoes.size();
		for (int c = 0; c < sizeNovasProducoes; c++) {
			Producao novaProducao;
			novaProducao = novasProducoes.get(c);
			
			novasProducoes.add( this.get_epsilon_livre_novas_producoes(conjuntoNe, novaProducao) );
		}
		
		return novasProducoes;
	}
	protected void get_epsilon_livre_producao_sequenciaSimbolos(Producao producaoDerivou, Producao producao) {
		ConjuntoNaoTerminal naoTerminais;
		naoTerminais = producaoDerivou.get_naoTerminais();
		
		String producaoString;
		producaoString = producao.get_producao();
		
		for (int k = 0; k < producaoString.length(); k++) {
			char simbolo;
			simbolo = producaoString.charAt(k);
			
			if (simbolo == ' ') {
				continue;
			}
			
			if (!this.caracter.is_letra_maiuscula(simbolo)) {
				producao.add_simbolo(simbolo);
				continue;
			}
			
			for (int j = 0; j < naoTerminais.size(); j++) {
				NaoTerminal naoTerminal;
				naoTerminal = naoTerminais.get(j);
				
				String simboloNaoTerminal;
				simboloNaoTerminal = naoTerminal.get_simbolo();
				
				int i;
				i = producaoString.indexOf(" ", k);
				
				if (i < 0) {
					i = producaoString.length();
				}
				
				String possivelSimboloEquals;
				possivelSimboloEquals = producaoString.substring(k, i);
				
				if (possivelSimboloEquals.equals(simboloNaoTerminal)) {
					producao.add_simbolo(naoTerminal);
					break;
				}
			}
		}
	}
	
	public void printar_o_role(ConjuntoNaoTerminal naoTerminais) {
		for (int c = 0; c < naoTerminais.size(); c++) {
			NaoTerminal naoTerminal;
			naoTerminal = naoTerminais.get(c);
			
			ConjuntoNaoTerminal naoTerminaisNa;
			naoTerminaisNa = naoTerminal.get_conjuntoNa();
			
			System.out.print("N"+naoTerminal.get_simbolo().toLowerCase()+" = {");
			for (int i = 0; i < naoTerminaisNa.size(); i++) {
				NaoTerminal naoTerminalNa;
				naoTerminalNa = naoTerminaisNa.get(i);
				
				System.out.print(naoTerminalNa.get_simbolo());
				
				if (i != naoTerminaisNa.size()-1) {
					System.out.print(", ");
				}
			}
			System.out.println("}");
		}
	}
	
}
package gramatica.operacao;

import java.util.ArrayList;

import conjunto.ConjuntoNaoTerminal;
import conjunto.ConjuntoObject;
import gramatica.Gramatica;
import gramatica.NaoTerminal;
import gramatica.Producao;

public class OperarGramatica {
	protected Fatoracao fatoracao;
	protected ConstruirFirstT construirFirst;
	protected ConstruirFollow construirFollow;
	protected ConstruirFirstNt construirFirstNt;
	protected RecursaoEsquerda recursaoEsquerda;
	protected TransformarGramaticaPropria transformarPropria;
	
	public OperarGramatica() {
		this.fatoracao = new Fatoracao();
		this.construirFirst = new ConstruirFirstT();
		this.construirFollow = new ConstruirFollow();
		this.construirFirstNt = new ConstruirFirstNt();
		this.recursaoEsquerda = new RecursaoEsquerda();
		this.transformarPropria = new TransformarGramaticaPropria();
	}
	
	//Metodo que verifica se uma gramatica eh finita ou nao
	public boolean get_gramatica_finita(Gramatica gramatica) {
		//Pegando o simbolo inical da gramatica
		NaoTerminal inicial = gramatica.get_inicial();
		
		//Pegando as producoes do inicial
		ConjuntoObject<Producao> producoes_inicial = inicial.get_producoes();
		
		//Criando tres conjuntos
		//aVisitar armazena os nao terminais a serem visitados
		//visitados armazena os nao terminais ja visitados - abaixo
		//infinitos armazena os nao terminais que possuem infinitude - abaixo
		ConjuntoNaoTerminal visitados, aVisitar, infinitos;
		aVisitar = new ConjuntoNaoTerminal();
		
		//Percorrendo as producoes do inicial para iniciar a busca de alguma infinitude
		for(int i = 0; i < producoes_inicial.size(); i++) {
			Producao producao = producoes_inicial.get(i);
			
			//Buscando os nao terminais presentes na producao
			ConjuntoNaoTerminal naoTerminaisProducao = producao.get_naoTerminais();
			
			//Percorrendo os nao terminais da producao para adicionar
			for(int j = 0; j < naoTerminaisProducao.size(); j++) {
				NaoTerminal gerado = naoTerminaisProducao.get(j);
				
				//Adicionando cada nao terminal gerado no conjunto para visitar
				if(!aVisitar.contains(gerado)) {
					aVisitar.add(gerado);
				}
			}
		}
		
		//Visitando todos os nao terminais produzidos pelo simbolo inicial
		for(int i = 0; i < aVisitar.size(); i++) {
			//Cria um novo conjunto para iniciar recursao
			visitados = new ConjuntoNaoTerminal();
			//Adiciona o estado inicial nos visitados
			visitados.add(inicial);
			
			//Cria um novo conjunto para inicial recursao
			infinitos = new ConjuntoNaoTerminal();
			
			NaoTerminal atual = aVisitar.get(i);
			
			//Chama o metodo para cada nao terminal produzido pelo inicial
			this.visitar_nao_terminal(atual, visitados, infinitos);
			
			//Se nenhum nao terminal infinito foi encontrado, entao eh finita
			if(!infinitos.isEmpty()) {
				return false;
			}
		}
		
		//Retorna verdadeiro se nenhum nao terminal infinito foi encontrado
		return true;
	}
	
	protected void visitar_nao_terminal(NaoTerminal atual, ConjuntoNaoTerminal visitados, ConjuntoNaoTerminal infinitos) {
		System.out.println("NT visitado:" + atual.get_simbolo());
		
		ConjuntoNaoTerminal aVisitar = new ConjuntoNaoTerminal();
		
		//Caso o nao terminal seja revisitado, gramatica eh infinita
		if(visitados.contains(atual)) {
			infinitos.add(atual);
			return;
		}
		
		//Se nao for revisitado, entao o adiciona nos visitados
		visitados.add(atual);
		
		//Pegando as producoes do atual
		ConjuntoObject<Producao> producoes_atual = atual.get_producoes();
		
		for(int i = 0; i < producoes_atual.size(); i++) {
			Producao producao = producoes_atual.get(i);
			
			//Buscando os nao terminais presentes na producao
			ConjuntoNaoTerminal naoTerminaisProducao = producao.get_naoTerminais();
			
			//Percorrendo os nao terminais da producao para adicionar
			for(int j = 0; j < naoTerminaisProducao.size(); j++) {
				NaoTerminal gerado = naoTerminaisProducao.get(j);
				
				//Se um nao terminal se gera em uma producao
				if(gerado == atual) {
					infinitos.add(atual);
					return;
				} else {
					//Chamanda recursiva para visita dos nao terminais produzidos
					if(!aVisitar.contains(gerado)) {
						aVisitar.add(gerado);
						this.visitar_nao_terminal(gerado, visitados, infinitos);
					}
				}
			}
		}
	}
	
	public void construir_first(Gramatica gramatica) {
		this.construirFirst.construir_first(gramatica);
	}
	public void construir_first_nt(Gramatica gramatica) {
		this.construirFirstNt.construir_first(gramatica);
	}
	public void construir_follow(Gramatica gramatica) {
		this.construirFollow.construir(gramatica);
	}
	
	public boolean get_linguagem_vazia(Gramatica gramatica) {
		ConjuntoNaoTerminal ferteis;
		ferteis = gramatica.get_ferteis();
		
		// Se nao existir nenhum naoTerminal fertil, entao eh vazia
		if (ferteis.size() == 0) {
			return true;
		}
		
		NaoTerminal inicial;
		inicial = gramatica.get_inicial();
		
		// Se o inicial nao esta nos ferteis entao eh morto
		if(!ferteis.contains(inicial)) {
			return true;
		}
		
		return false;
	}
	public boolean get_fatorada(Gramatica gramatica) {
		return this.fatoracao.get_is_fatorada(gramatica);
	}
	public boolean get_is_fatorada(Gramatica gramatica) {
		return this.fatoracao.get_is_fatorada(gramatica);
	}
	public Gramatica get_fatoravel_n_passos(Gramatica gramatica, int n_passos) {
		gramatica = this.fatoracao.remover_fatoracao_n_passos(gramatica, n_passos);
		return gramatica;
	}
	public boolean get_possui_recursao_esquerda(Gramatica gramatica) {
		return this.recursaoEsquerda.get_possui_recursao_esquerda(gramatica);
	}
	public void get_recursao_esquerda_tipo(Gramatica gramatica, ConjuntoNaoTerminal naoTerminaisRecursaoDireta, ConjuntoNaoTerminal naoTerminaisRecursaoIndireta) {
		this.recursaoEsquerda.get_naoTerminais_recursoes(gramatica, naoTerminaisRecursaoDireta, naoTerminaisRecursaoIndireta);
	}
	public Gramatica get_remover_recursao_esquerda_direta(Gramatica gramatica) {
		return this.recursaoEsquerda.remover_recursao_diretas(gramatica);
	}
	
	public Gramatica get_remover_recursao_esquerda_indireta(Gramatica gramatica) {
		ArrayList<Gramatica> gramaticas;
		gramaticas = this.transformarPropria.get_gramatica_propria(gramatica);
		
		return this.recursaoEsquerda.remover_recursao_indireta(gramaticas.get( gramaticas.size()-1 ));
		//return this.recursaoEsquerda.remover_recursao_indireta(gramatica);
	}
}

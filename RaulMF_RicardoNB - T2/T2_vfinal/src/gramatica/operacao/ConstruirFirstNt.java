package gramatica.operacao;

import java.util.ArrayList;

import conjunto.Conjunto;
import conjunto.ConjuntoNaoTerminal;
import conjunto.ConjuntoObject;
import gramatica.Gramatica;
import gramatica.NaoTerminal;
import gramatica.Producao;

public class ConstruirFirstNt {
	protected ConstruirFirstT construirFirst;
	
	public ConstruirFirstNt() {
		this.construirFirst = new ConstruirFirstT();
	}
	
	public void construir_first(Gramatica gramatica) {
		this.construirFirst.construir_first(gramatica);
		
		ConjuntoNaoTerminal naoTerminais;
		naoTerminais = gramatica.get_naoTerminais();
		
		boolean novosFirsts;
		
		do {
			novosFirsts = false;
			
			/* Para cada naoTerminal eh feito uma varredura para obtencao
			 * de novos firsts para seu conjunto de firsts
			 */
			for (int c = 0; c < naoTerminais.size(); c++) {
				NaoTerminal naoTerminal;
				naoTerminal = naoTerminais.get(c);
				
				int totalFirstsAntesConstruir;
				totalFirstsAntesConstruir = naoTerminal.get_firstsNt().size();
				
				// Obtem os firsts do naoTerminal atual
				this.construir_firsts_naoTerminal(naoTerminal);
				
				int totalFirstsDepoisConstruir;
				totalFirstsDepoisConstruir = naoTerminal.get_firstsNt().size();
				
				/* Se algum novo first foi detectado para o naoTerminal, entao
				 * pode existir novos firsts para outros naoTerminais (a partir
				 * desse naoTerminal atual)
				 */
				if (totalFirstsAntesConstruir != totalFirstsDepoisConstruir) {
					novosFirsts = true;
				}
			}
			/* Enquanto novos firsts forem detectados, para cada naoTerminal,
			 * entao novos firsts podem ser detectados para os outros naoTerminais
			 */
		} while(novosFirsts);
	}
	protected void construir_firsts_naoTerminal(NaoTerminal naoTerminal) {
		// Obtem firsts do naoTerminal
		ConjuntoNaoTerminal firsts;
		firsts = naoTerminal.get_firstsNt();
		
		ConjuntoObject<Producao> producoes;
		producoes = naoTerminal.get_producoes();
		
		/* Para cada producao eh obtido o first dela, atraves
		 * do terminal ou dos firsts de naoTerminais
		 */
		for (int c = 0; c < producoes.size(); c++) {
			Producao producao;
			producao = producoes.get(c);
			
			// Obtem firsts da producao
			ConjuntoNaoTerminal firstsProducao;
			firstsProducao = this.obter_firsts_producao(producao);
			
			firsts.add(firstsProducao);
		}
	}
	public ConjuntoNaoTerminal obter_firsts_producao(Producao producao) {
		ConjuntoNaoTerminal retornoFirsts;
		retornoFirsts = new ConjuntoNaoTerminal();
		
		ArrayList<Object> sequenciaSimbolos;
		sequenciaSimbolos = producao.get_sequencia_simbolos();
		
		//Percorre todos os simbolos da producao, do primeiro ao ultimo.
		for (int k = 0; k < sequenciaSimbolos.size(); k++) {
			Object simbolo;
			simbolo = sequenciaSimbolos.get(k);
			
			// Se encontrou um terminal ou eplison
			if (simbolo.getClass() != NaoTerminal.class) {
				return retornoFirsts;
			}
			
			// Caso contrario: eh um naoTerminal
			NaoTerminal simboloNaoTerminal;
			simboloNaoTerminal = (NaoTerminal)simbolo;
			
			retornoFirsts.add(simboloNaoTerminal);
			
			boolean possuiEplison;
			possuiEplison = false;
			
			Conjunto<Character> firstsT;
			firstsT = simboloNaoTerminal.get_firstsT();
			
			/* Percorre todos os first do naoTerminal (sao todos terminais) e
			 * adiciona como first da producao. Se for um eplison, ele eh
			 * adicionado apenas se nao existirem mais simbolos na producao.
			 */
			for (int c = 0; c < firstsT.size(); c++) {
				char simboloFirst;
				simboloFirst = firstsT.get(c);
				
				// Simbolo Eplison
				if (simboloFirst == Gramatica.EPSILON) {
					possuiEplison = true;
				}
			}
			
			ConjuntoNaoTerminal firstsNt;
			firstsNt = simboloNaoTerminal.get_firstsNt();
			
			for (int c = 0; c < firstsNt.size(); c++) {
				NaoTerminal naoTerminalNt;
				naoTerminalNt = firstsNt.get(c);
				
				retornoFirsts.add(naoTerminalNt);
			}
			
			/* Se o naoTerminal possuia eplison, entao o proximo simbolo
			 * tambem sera um possivel first. Caso contrario, a producao
			 * ja tem todos os possiveis first.
			 */
			if (!possuiEplison) {
				return retornoFirsts;
			}
		}
		
		return retornoFirsts;
	}
}
package gramatica.operacao;

import conjunto.Conjunto;
import conjunto.ConjuntoNaoTerminal;
import conjunto.ConjuntoObject;
import gramatica.Gramatica;
import gramatica.NaoTerminal;
import gramatica.Producao;

public class Fatoracao {
	protected int contador;
	
	public Fatoracao() {
		this.contador = 0;
	}
	
	public Gramatica remover_fatoracao_n_passos(Gramatica gramatica, int n_passos) {
		Gramatica novaGramatica;
		novaGramatica = new Gramatica(gramatica);
		
		ConjuntoNaoTerminal naoFatorados;
		/* Enquanto o numero de passos for menor do que o solicitado e
		 * ainda existir uma naoFatoracao na gramatica
		 */
		naoFatorados = this.get_nao_fatorados(novaGramatica);
		
		for (int c = 0; c < naoFatorados.size(); c++) {
			NaoTerminal naoTerminalNaoFatorado;
			naoTerminalNaoFatorado = naoFatorados.get(c);
			
			this.remover_fatoracao_direta_naoTerminal(novaGramatica, naoTerminalNaoFatorado);
		}
		
		for (int i = 0; i < n_passos; i++) {
			naoFatorados = this.get_nao_fatorados(novaGramatica);
			
			for (int c = 0; c < naoFatorados.size(); c++) {
				NaoTerminal naoTerminalNaoFatorado;
				naoTerminalNaoFatorado = naoFatorados.get(c);
				
				this.remover_fatoracao_indireta_naoTerminal(novaGramatica, naoTerminalNaoFatorado);
				this.remover_fatoracao_direta_naoTerminal(novaGramatica, naoTerminalNaoFatorado);
			}
			
		}
		
		if (naoFatorados.size() == 0) {
			return novaGramatica;
		}
		
		return null;
	}
	protected NaoTerminal remover_fatoracao_indireta_naoTerminal(Gramatica gramatica, NaoTerminal naoTerminal) {
		/*
		//Para cada producao(0) do naoTerminal(0)
		//	
		//	Se o primeiro simbolo for um naoTerminal(1)
		//		
		//		novasProducoes
		//		Remove primeiro simbolo da producao(0)
		//		Remove producao(0) do naoTerminal(0)
		//		
		//		Para cada producao(1) do naoTerminal(1)
		//			
		//			novaProducao
		//			
		//			Se for producao(1) epsilon
		//			
		//				Se producao(0) estiver vazia
		//					novaProducao = epsilon
		//				
		//				SENAO
		//					novaProducao = producao(0)[de 1 ate n]
		//				FIM
		//			SENAO
		//				novaProducao
		//				novaProducao = producao(1) + producao(0)[de 1 ate n]
		//			Fim
		//			
		//			Add novaProducao em novasProducoes
		//		Fim
		//		
		//		Add novasProducoes no naoTerminal(0)
		//	Fim
		/Fim
		 */
		
		ConjuntoObject<Producao> producoes;
		producoes = naoTerminal.get_producoes();
		
		ConjuntoObject<Producao> novasProducoes;
		novasProducoes = new ConjuntoObject<Producao>();
		
		int sizeProducoes;
		sizeProducoes = producoes.size();
		
		//Para cada producao(0) do naoTerminal(0)
		for (int c = 0; c < sizeProducoes; c++) {
			Producao producao;
			producao = producoes.get(c);
			
			Object simboloInicial;
			simboloInicial = producao.get_simbolo(0);
			
			if (simboloInicial.getClass() != NaoTerminal.class) {
				continue;
			}
			
			NaoTerminal naoTerminalProducao;
			naoTerminalProducao = (NaoTerminal) simboloInicial;
			
			producoes.remove(producao);
			producao.remove(0);
			c--;
			sizeProducoes--;
			
			ConjuntoObject<Producao> producoesNaoTerminalProducao;
			producoesNaoTerminalProducao = naoTerminalProducao.get_producoes();
			
			for (int i = 0; i < producoesNaoTerminalProducao.size(); i++) {
				Producao producaoNaoTerminalProducao;
				producaoNaoTerminalProducao = producoesNaoTerminalProducao.get(i);
				
				Producao novaProducao;
				novaProducao = new Producao(gramatica);
	
				Object simbolo;
				simbolo = producaoNaoTerminalProducao.get_simbolo(0);
				
				if (simbolo.getClass() != NaoTerminal.class) {
					char simboloTerminal;
					simboloTerminal = (char)simbolo;
					
					if (simboloTerminal == Gramatica.EPSILON) {
						if (producao.size() == 0) {
							novaProducao.add_simbolo('&');
						} else {
							for (int g = 0; g < producao.size(); g++) {
								if (producao.get_simbolo(g).getClass() == NaoTerminal.class) {
									NaoTerminal nt;
									nt = (NaoTerminal) producao.get_simbolo(g);
								}
								
								novaProducao.add_simbolo(producao.get_simbolo(g));
							}
						}
					} else {
						for (int g = 0; g < producaoNaoTerminalProducao.size(); g++) {
							novaProducao.add_simbolo(producaoNaoTerminalProducao.get_simbolo(g));
						}
						for (int g = 0; g < producao.size(); g++) {
							novaProducao.add_simbolo(producao.get_simbolo(g));
						}
					}
				} else {
					for (int g = 0; g < producaoNaoTerminalProducao.size(); g++) {
						novaProducao.add_simbolo(producaoNaoTerminalProducao.get_simbolo(g));
					}
					for (int g = 0; g < producao.size(); g++) {
						novaProducao.add_simbolo(producao.get_simbolo(g));
					}
				}
				
				novasProducoes.add(novaProducao);
			}
		}
		
		naoTerminal.add_producoes(novasProducoes);
		return naoTerminal;
	}
	
	protected boolean remover_fatoracao_direta_naoTerminal(Gramatica gramatica, NaoTerminal naoTerminal) {
		ConjuntoObject<Producao> producoes;
		producoes = naoTerminal.get_producoes();
		
		boolean possuiDireta;
		possuiDireta = false;
		
		/* Percorre todas as producoes do naoTerminal para buscar uma outra
		 * producao, do mesmo naoTerminal, que inicie com o mesmo simbolo.
		 * Caso isso ocorra entao existe uma naoFatoracao direta nesse naoTerminal,
		 * que deve ser removida.
		 */
		for (int c = 0; c < producoes.size(); c++) {
			Producao producao1;
			producao1 = producoes.get(c);
			
			Object simbolo1;
			if (producao1.size() == 0) {
				continue;
			}
			
			simbolo1 = producao1.get_simbolo(0);
			
			// Conjunto de producoes que iniciam com o mesmo simbolo
			ConjuntoObject<Producao> producoesFirstIgual;
			producoesFirstIgual = new ConjuntoObject<Producao>();
			producoesFirstIgual.add(producao1);
			
			for (int i = 0; i < producoes.size(); i++) {
				// Producao1 e Producao2 nao podem ser a mesma
				if (i == c) {
					continue;
				}
				
				Producao producao2;
				producao2 = producoes.get(i);
				
				Object simbolo2;
				
				if (producao2.size() == 0) {
					continue;
				}
				
				simbolo2 = producao2.get_simbolo(0);
				
				// Producao1 e Producao2 iniciam com o mesmo simbolo
				if (simbolo1 == simbolo2) {
					producoesFirstIgual.add(producao2);
				}
			}
			
			// Caso exista uma naoFatoracao, entao remove
			if (producoesFirstIgual.size() > 1) {
				possuiDireta = true;
				this.remover_fatoracao_direta_producoes(gramatica, naoTerminal, producoesFirstIgual);
			}
		}
		
		return possuiDireta;
	}
	
	protected void remover_fatoracao_direta_producoes(Gramatica gramatica, NaoTerminal naoTerminal, ConjuntoObject<Producao> producoesIguais) {
		this.contador++;
		
		NaoTerminal naoTerminalNovo;
		naoTerminalNovo = new NaoTerminal(gramatica);
		
		/* O naoTerminal antigo recebera uma novaProducao no lugar das naoFatoradas, a qual ira
		 * derivar o primeiro simbolo dessas producoes (todas iniciam com o mesmo simbolo) junto
		 * com o novoNaoTerminal
		 */
		Producao novaProducaoFatorada;
		novaProducaoFatorada = new Producao(naoTerminal.get_gramatica());
		novaProducaoFatorada.add_simbolo( producoesIguais.get(0).get_simbolo(0) );
		novaProducaoFatorada.add_simbolo(naoTerminalNovo);
		novaProducaoFatorada.gerar_producao();
		
		naoTerminal.add_producao(novaProducaoFatorada);
		
		ConjuntoObject<Producao> producoes;
		producoes = naoTerminal.get_producoes();
		
		for (int c = 0; c < producoesIguais.size(); c++) {
			Producao producao;
			producao = producoesIguais.get(c);
			
			Object primeiroSimbolo;
			if (producao.size() == 0) {
				continue;
			}
			primeiroSimbolo = producao.get_simbolo(0);
			
			producao.remove(0);
			
			if (producao.size() == 0) {
				producao.add_simbolo('&');
			}
			
			/* O novo naoTerminal contera todas as producoes naoFatoradas, com a excessao do
			 * primeiro simbolo, e o naoTerminal antigo deixara de conter essas producoes
			 */
			producoes.remove(producao);
			
			naoTerminalNovo.add_producao(producao);
		}
		
		gramatica.add_naoTerminal(naoTerminalNovo);
	}
	protected ConjuntoNaoTerminal get_nao_fatorados(Gramatica gramatica) {
		ConjuntoNaoTerminal naoFatorados;
		naoFatorados = new ConjuntoNaoTerminal();
		
		ConstruirFirstT construirFirst;
		construirFirst = new ConstruirFirstT();
		construirFirst.construir_first(gramatica);
		
		ConjuntoNaoTerminal naoTerminais;
		naoTerminais = gramatica.get_naoTerminais();
		
		/* Obtem todos os simbolos first de cada producao, de cada naoTerminal,
		 * e adiciona em um conjunto. Caso algum first se remita, para alguma
		 * producao do naoTerminal, entao o naoTerminal nao esta fatorado, e
		 * portanto o naoTerminal eh adicionado em um conjunto a ser retornado.
		 */
		for (int c = 0; c < naoTerminais.size(); c++) {
			NaoTerminal naoTerminal;
			naoTerminal = naoTerminais.get(c);
			
			ConjuntoObject<Producao> producoes;
			producoes = naoTerminal.get_producoes();
			
			Conjunto<Character> simbolosProducaoTotal;
			simbolosProducaoTotal = new Conjunto<Character>();
			
			boolean fatorado;
			fatorado = true;
			
			// Para cada naoTerminal eh realizada a verificacao de suas producoes
			for (int i = 0; i < producoes.size(); i++) {
				Producao producao;
				producao = producoes.get(i);
				
				Conjunto<Character> simbolosProducao;
				simbolosProducao = construirFirst.obter_firsts_producao(producao);
				/*
				System.out.print("First("+naoTerminal.get_simbolo()+"): {");
				for (int k = 0; k < simbolosProducao.size(); k++) {
					char simboloProducao;
					simboloProducao = simbolosProducao.get(k);
					
					System.out.print(simboloProducao);
					if (k != simbolosProducao.size()) {
						System.out.print(", ");
					}
				}
				System.out.println("}");
				*/
				/* Para cada first da producao eh verificado se existe alguma
				 * outra producao que o possua como first
				 */
				for (int j = 0; j < simbolosProducao.size(); j++) {
					char simboloProducao;
					simboloProducao = simbolosProducao.get(j);
					
					// Se o simbolo first se repete
					if (simbolosProducaoTotal.contains(simboloProducao)) {
						naoFatorados.add(naoTerminal);
						fatorado = false;
						break;
					}
				}
				if (!fatorado) {
					break;
				}
				
				/* Adiciona os simbolos first da producao ao conjunto de firsts
				 * total das producoes do naoTerminal
				 */
				simbolosProducaoTotal.add(simbolosProducao);
			}
		}
		
		return naoFatorados;
	}
	public boolean get_is_fatorada(Gramatica gramatica) {
		ConstruirFirstT construirFirst;
		construirFirst = new ConstruirFirstT();
		construirFirst.construir_first(gramatica);
		
		ConjuntoNaoTerminal naoTerminais;
		naoTerminais = gramatica.get_naoTerminais();
		
		/* Obtem todos os simbolos first de cada producao, de cada naoTerminal,
		 * e adiciona em um conjunto. Caso algum first se remita, para alguma
		 * producao em toda a gramatica, entao a gramatica nao esta fatorada
		 */
		for (int c = 0; c < naoTerminais.size(); c++) {
			NaoTerminal naoTerminal;
			naoTerminal = naoTerminais.get(c);
			
			ConjuntoObject<Producao> producoes;
			producoes = naoTerminal.get_producoes();
			
			Conjunto<Character> simbolosProducaoTotal;
			simbolosProducaoTotal = new Conjunto<Character>();
			
			// Para cada naoTerminal eh realizada a verificacao de suas producoes
			for (int i = 0; i < producoes.size(); i++) {
				Producao producao;
				producao = producoes.get(i);
				
				Conjunto<Character> firstsProducao;
				firstsProducao = construirFirst.obter_firsts_producao(producao);
				
				/* Para cada first da producao eh verificado se existe alguma
				 * outra producao que o possua como first
				 */
				for (int j = 0; j < firstsProducao.size(); j++) {
					char simboloProducao;
					simboloProducao = firstsProducao.get(j);
					
					// Se o simbolo first se repete
					if (simbolosProducaoTotal.contains(simboloProducao)) {
						return false;
					}
				}
				
				simbolosProducaoTotal.add(firstsProducao);
			}
		}
		
		return true;
	}
}

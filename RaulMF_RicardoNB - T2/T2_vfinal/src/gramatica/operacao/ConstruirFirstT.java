package gramatica.operacao;

import java.util.ArrayList;

import conjunto.Conjunto;
import conjunto.ConjuntoNaoTerminal;
import conjunto.ConjuntoObject;
import gramatica.Gramatica;
import gramatica.NaoTerminal;
import gramatica.Producao;

public class ConstruirFirstT {
	public ConstruirFirstT() {}
	
	public void construir_first(Gramatica gramatica) {
		ConjuntoNaoTerminal naoTerminais;
		naoTerminais = gramatica.get_naoTerminais();
		
		boolean novosFirsts;
		
		do {
			novosFirsts = false;
			
			/* Para cada naoTerminal eh feito uma varredura para obtencao
			 * de novos firsts para seu conjunto de firsts
			 */
			for (int c = 0; c < naoTerminais.size(); c++) {
				NaoTerminal naoTerminal;
				naoTerminal = naoTerminais.get(c);
				
				int totalFirstsAntesConstruir;
				totalFirstsAntesConstruir = naoTerminal.get_firstsT().size();
				
				// Obtem os firsts do naoTerminal atual
				this.construir_firsts_naoTerminal(naoTerminal);
				
				int totalFirstsDepoisConstruir;
				totalFirstsDepoisConstruir = naoTerminal.get_firstsT().size();
				
				/* Se algum novo first foi detectado para o naoTerminal, entao
				 * pode existir novos firsts para outros naoTerminais (a partir
				 * desse naoTerminal atual)
				 */
				if (totalFirstsAntesConstruir != totalFirstsDepoisConstruir) {
					novosFirsts = true;
				}
			}
			
			/* Enquanto novos firsts forem detectados, para cada naoTerminal,
			 * entao novos firsts podem ser detectados para os outros naoTerminais
			 */
		} while(novosFirsts);
	}
	protected void construir_firsts_naoTerminal(NaoTerminal naoTerminal) {
		// Obtem firsts do naoTerminal
		Conjunto<Character> firsts;
		firsts = naoTerminal.get_firstsT();
		
		ConjuntoObject<Producao> producoes;
		producoes = naoTerminal.get_producoes();
		
		/* Para cada producao eh obtido o first dela, atraves
		 * do terminal ou dos firsts de naoTerminais
		 */
		for (int c = 0; c < producoes.size(); c++) {
			Producao producao;
			producao = producoes.get(c);
			
			// Obtem firsts da producao
			Conjunto<Character> firstsProducao;
			firstsProducao = this.obter_firsts_producao(producao);
			
			firsts.add(firstsProducao);
		}
	}
	public Conjunto<Character> obter_firsts_producao(Producao producao) {
		Conjunto<Character> retornoFirsts;
		retornoFirsts = new Conjunto<Character>();
		
		ArrayList<Object> sequenciaSimbolos;
		sequenciaSimbolos = producao.get_sequencia_simbolos();
		
		//"S -> A C | C e B | B a\nA -> a A | B C\nC -> c C | &\nB -> b B | A B | &";
		//Percorre todos os simbolos da producao, do primeiro ao ultimo.
		for (int k = 0; k < sequenciaSimbolos.size(); k++) {
			Object simbolo;
			simbolo = sequenciaSimbolos.get(k);
			
			// Se encontrou um terminal ou eplison, entao ele eh o first da producao
			if (simbolo.getClass() != NaoTerminal.class) {
				char simboloChar;
				simboloChar = (char)simbolo;
				
				retornoFirsts.add(simboloChar);
				return retornoFirsts;
			}
			
			// Caso contrario: eh um naoTerminal
			boolean possuiEplison;
			possuiEplison = false;
			
			NaoTerminal simboloNaoTerminal;
			simboloNaoTerminal = (NaoTerminal)simbolo;
			
			Conjunto<Character> firstsNaoTerminal;
			firstsNaoTerminal = simboloNaoTerminal.get_firstsT();
			
			/* Percorre todos os first do naoTerminal (sao todos terminais) e
			 * adiciona como first da producao. Se for um eplison, ele eh
			 * adicionado apenas se nao existirem mais simbolos na producao.
			 */
			for (int c = 0; c < firstsNaoTerminal.size(); c++) {
				char simboloFirst;
				simboloFirst = firstsNaoTerminal.get(c);
				
				// Simbolo Eplison
				if (simboloFirst == Gramatica.EPSILON) {
					if (k+1 == sequenciaSimbolos.size()) {
						retornoFirsts.add(simboloFirst);
					}
					possuiEplison = true;
				}
				// Simbolo Terminal
				else {
					retornoFirsts.add(simboloFirst);
				}
			}
			/* Se o naoTerminal possuia eplison, entao o proximo simbolo
			 * tambem sera um possivel first. Caso contrario, a producao
			 * ja tem todos os possiveis first.
			 */
			if (!possuiEplison) {
				return retornoFirsts;
			}
		}
		
		return retornoFirsts;
	}
}
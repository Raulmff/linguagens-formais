package gramatica;

import java.util.ArrayList;

import conjunto.Conjunto;
import conjunto.ConjuntoNaoTerminal;
import util.Caracter;

public class Producao implements Cloneable {
	protected String producao;
	protected Gramatica gramatica;
	
	protected Conjunto<Character> terminais;
	protected ConjuntoNaoTerminal naoTerminais;
	protected ArrayList<Object> sequenciaSimbolos;
	
	protected Caracter caracter;
	
	protected boolean gerandoSequencia;
	
	public Producao(Gramatica gramatica) {
		this.gerandoSequencia = false;
		this.gramatica = gramatica;
		this.producao = "";
		this.caracter = new Caracter();
		this.terminais = new Conjunto<Character>();
		this.naoTerminais = new ConjuntoNaoTerminal();
		this.sequenciaSimbolos = new ArrayList<Object>();
	}
	
	@Override
	public Producao clone() {
		try {
			return (Producao) super.clone();
		} catch (CloneNotSupportedException e) {
			System.out.println("errow");
			return null;
		}
	}
	
	public void set_producao(String producao) {
		char primeiro, ultimo;
		primeiro = producao.charAt(0);
		ultimo = producao.charAt(producao.length()-1);
		
		if (primeiro == ' ') {
			producao = producao.substring(1);
		}
		
		if (ultimo == ' ') {
			producao = producao.substring(0, producao.length()-1);
		}
		
		this.producao = producao.replaceAll("  ", " ");
		this.gerandoSequencia = true;
		this.gerar_sequencia_simbolos(this.gramatica.get_naoTerminais());
		this.gerandoSequencia = false;
	}
	private void gerar_sequencia_simbolos(ConjuntoNaoTerminal naoTerminais) {
		this.sequenciaSimbolos = new ArrayList<Object>();
		this.naoTerminais = new ConjuntoNaoTerminal();
		this.terminais = new Conjunto<Character>();
		
		for(int j = 0; j < producao.length(); j++) {
			char simbol;
			simbol = producao.charAt(j);
			
			if (simbol == ' ') {
				continue;
			}
			
			/* Se for um terminal, adiciona no array / alfabeto e
			 * avanca o laco para proxima producao
			 */
			if (!caracter.is_letra_maiuscula(simbol)) {
				this.add_simbolo(simbol);
				continue;
			}
			/* Caso contrario, ele eh um naoTerminal. O simbolo do
			 * naoTerminal pode possuir mais de um caracter, entao
			 * busca o seu fim e o adiciona na lista
			 */
			int simboloFim;
			simboloFim = producao.indexOf(" ", j);
			
			if (simboloFim < 0) {
				simboloFim = producao.length();
			}
			
			String simboloProducaoNaoTerminal;
			simboloProducaoNaoTerminal = producao.substring(j, simboloFim);
			
			NaoTerminal naoTerminalNovo;
			naoTerminalNovo = new NaoTerminal(this.gramatica, simboloProducaoNaoTerminal);
			naoTerminalNovo = this.gramatica.add_naoTerminal(naoTerminalNovo);
			
			this.add_simbolo(naoTerminalNovo);
			
			j = simboloFim;
		}
	}
	
	public void gerar_producao() {
		this.producao = "";
		
		for (int c = 0; c < this.sequenciaSimbolos.size(); c++) {
			Object simbolo;
			simbolo = this.sequenciaSimbolos.get(c);
			
			if (c != 0) {
				this.producao += " ";
			}
			
			if (simbolo.getClass() == NaoTerminal.class) {
				NaoTerminal simboloNaoTerminal;
				simboloNaoTerminal = (NaoTerminal) simbolo;
				
				this.producao += simboloNaoTerminal.get_simbolo();
			} else {
				char simboloTerminal;
				simboloTerminal = (char) simbolo;
				
				this.producao += simboloTerminal;
			}
		}
	}
	public void add_simbolo(Object simbolo) {
		if (simbolo.getClass() == NaoTerminal.class) {
			simbolo = this.add_naoTerminal((NaoTerminal) simbolo);
		} else {
			simbolo = this.add_terminal((char) simbolo);
		}
		
		this.sequenciaSimbolos.add(simbolo);
		
		if (!this.gerandoSequencia) {
			this.gerar_producao();
		}
	}
	private char add_terminal(char terminal) {
		return this.terminais.add(terminal);
	}
	private NaoTerminal add_naoTerminal(NaoTerminal naoTerminal) {
		return this.naoTerminais.add(naoTerminal);
	}
	
	public String get_producao() {
		return this.producao;
		
		// Para fins de testes
		/*
		String producao;
		producao = "";
		
		for (int c = 0; c < this.sequenciaSimbolos.size(); c++) {
			Object simboloObject;
			simboloObject = this.sequenciaSimbolos.get(c);
			
			String simbolo;
			simbolo = "";
			
			if (simboloObject.getClass() == NaoTerminal.class) {
				simbolo += ((NaoTerminal)simboloObject).get_simbolo();
			} else {
				simbolo += (char)simboloObject;
			}
			
			if (!producao.equals("") && (simboloObject.getClass() == NaoTerminal.class) ) {
				producao += " ";
			}
			producao += simbolo;
		}
		return producao;
		*/
	}
	public ConjuntoNaoTerminal get_naoTerminais() {
		return this.naoTerminais;
	}
	public ArrayList<Object> get_sequencia_simbolos() {
		return this.sequenciaSimbolos;
	}
	public Object get_simbolo(int c) {
		return this.sequenciaSimbolos.get(c);
	}
	public void remove(int c) {
		this.sequenciaSimbolos.remove(c);
		this.gerar_producao();
	}
	public int size() {
		return this.sequenciaSimbolos.size();
	}
	
	public NaoTerminal get_primeiro_simbolo_naoTerminal() {
		Object simbolo;
		if (this.sequenciaSimbolos.size() == 0) {
			return null;
		}
		
		simbolo = this.sequenciaSimbolos.get(0);
		
		if (simbolo.getClass() == NaoTerminal.class) {
			return (NaoTerminal) simbolo;
		}
		return null;
	}
	
	
	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}
		
		String classObject, classThis;
		classObject = object.getClass().toString();
		classThis = this.getClass().toString();
		
		if (!classObject.equals(classThis)) {
			return false;
		}
		
		Producao objeto;
		objeto = (Producao)object;
		
		if (this.producao.equals(objeto.producao)) {
			return true;
		}
		
		return false;
	}
	/*
	@Override
	public String toString() {
		return this.producao;
	}
	*/
	public Conjunto<Character> get_terminais() {
		return this.terminais;
	}
}

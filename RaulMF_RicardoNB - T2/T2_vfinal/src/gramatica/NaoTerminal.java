package gramatica;
import java.util.ArrayList;

import conjunto.Conjunto;
import conjunto.ConjuntoNaoTerminal;
import conjunto.ConjuntoObject;
import util.AlfabetoPortugues;

public class NaoTerminal {
	protected String simbolo;
	
	protected Gramatica gramatica;
	protected Conjunto<Character> firstT, follow;
	protected ConjuntoObject<Producao> producoes;
	protected ConjuntoNaoTerminal conjuntoNa, firstNt;
	protected AlfabetoPortugues alfabetoPortugues;
	
	public NaoTerminal(Gramatica gramatica, String simbolo) {
		this.gramatica = gramatica;
		
		this.simbolo = simbolo.replaceAll(" ", "");
		
		if (!this.simbolo.equals(simbolo)) {
			System.out.println("NaoTerminal(): \""+simbolo+"\" para \""+this.simbolo+"\"");
		}
		
		this.alfabetoPortugues = new AlfabetoPortugues();
		
		this.firstT = new Conjunto<Character>();
		this.follow = new Conjunto<Character>();
		this.producoes = new ConjuntoObject<Producao>();
		this.conjuntoNa = new ConjuntoNaoTerminal();
		this.firstNt = new ConjuntoNaoTerminal();
		this.conjuntoNa.add(this);
	}
	public NaoTerminal(Gramatica gramatica) {
		this(gramatica, ",");
		
		char primeiraLetra;
		primeiraLetra = this.alfabetoPortugues.get_next_simbolo();
		
		String simbolo;
		simbolo = primeiraLetra+"";
		
		int contador;
		contador = -1;
		
		while (this.gramatica.existe_nome_naoTerminal(simbolo)) {
			char letra;
			letra = this.alfabetoPortugues.get_next_simbolo();
			
			if (letra == primeiraLetra) {
				contador++;
			}
			
			simbolo = letra + "";
			
			if (contador >= 0) {
				simbolo += contador;
			}
		}
		
		this.simbolo = simbolo;
	}
	
 	public void add_naoTerminalNa(ConjuntoNaoTerminal conjuntoNa) {
 		for (int c = 0; c < conjuntoNa.size(); c++) {
			NaoTerminal naoTerminalNa;
			naoTerminalNa = conjuntoNa.get(c);
			
			this.conjuntoNa.add(naoTerminalNa);
		}
 	}
 	
	public Producao add_producao(Producao producao) {
		Producao p;
		p = this.producoes.add(producao);
		
		return p;
	}
	public void add_producoes(ConjuntoObject<Producao> producoes) {
		for (int c = 0; c < producoes.size(); c++) {
			Producao producao;
			producao = producoes.get(c);
			
			this.producoes.add(producao);
		}
	}
	
	public Gramatica get_gramatica() {
		return this.gramatica;
	}
	public String get_simbolo() {
		return this.simbolo;
	}
	public Conjunto<Character> get_firstsT() {
		return this.firstT;
	}
	public ConjuntoNaoTerminal get_firstsNt() {
		return this.firstNt;
	}
	
	public Conjunto<Character> get_follows() {
		return this.follow;
	}
	public ConjuntoNaoTerminal get_conjuntoNa() {
		return this.conjuntoNa;
	}
	public ConjuntoObject<Producao> get_producoes() {
		return this.producoes;
	}
	public boolean get_possui_eplison() {
		for (int c = 0; c < this.producoes.size(); c++) {
			Producao producao;
			producao = producoes.get(c);
			
			Conjunto<Character> terminais;
			terminais = producao.get_terminais();
			
			if (terminais.contains(Gramatica.EPSILON)) {
				return true;
			}
		}
		
		return false;
	}
	
	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}
		
		String classObject, classThis;
		classObject = object.getClass().toString();
		classThis = this.getClass().toString();
		
		if (!classObject.equals(classThis)) {
			return false;
		}
		
		NaoTerminal objeto;
		objeto = (NaoTerminal)object;
		
		if (objeto.get_simbolo().equals(this.get_simbolo())) {
			return true;
		}
		
		return false;
	}
	
	public void print() {
		System.out.print(simbolo+" -> ");
		for (int c = 0; c < this.producoes.size(); c++) {
			System.out.print(this.producoes.get(c).get_producao());
			
			if (this.producoes.size()-1 != c) {
				System.out.print(" | ");
			}
		}
		System.out.println();
	}
	public void print2() {
		System.out.print(simbolo+" -> ");
		for (int c = 0; c < this.producoes.size(); c++) {
			ArrayList<Object> objetos;
			objetos = this.producoes.get(c).get_sequencia_simbolos();
			
			for (int i = 0; i < objetos.size(); i++) {
				Object objeto;
				objeto = objetos.get(i);
				
				if (objeto.getClass() == NaoTerminal.class) {
					NaoTerminal naoTerminal;
					naoTerminal = (NaoTerminal)objeto;
					
					System.out.print(naoTerminal.get_simbolo());
				} else {
					System.out.print((char)objeto);
				}
				
				if (i != objetos.size()-1) {
					System.out.print(" ");
				}
			}
			
			if (this.producoes.size()-1 != c) {
				System.out.print(" | ");
			}
		}
		System.out.println();
	}
}

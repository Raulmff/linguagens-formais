import java.util.ArrayList;

import conjunto.Conjunto;
import conjunto.ConjuntoNaoTerminal;
import conjunto.ConjuntoObject;
import gramatica.Gramatica;
import gramatica.NaoTerminal;
import gramatica.Producao;
import gramatica.operacao.Fatoracao;
import gramatica.operacao.OperarGramatica;
import gramatica.operacao.TransformarGramaticaPropria;
import view.window.Manager;

public class Main {
	/*
S -> a | B | &
A -> a | B | &
B -> a | B | &
C -> a | B | &
D -> a | B | &
E -> a | B | &
F -> a | B | &
G -> a | B | &
H -> a | B | &
I -> a | B | &
J -> a | B | &
K -> a | B | &
L -> a | B | &
M -> a | B | &
N -> a | B | &
O -> a | B | &
P -> a | B | &
Q -> a | B | &
R -> a | B | &
T -> a | B | &
U -> a | B | &
V -> a | B | &
W -> a | B | &
Y -> a | B | &
X -> a | B | &
Z -> a | B | &
S0 -> a | B | &
A0 -> a | B | &
B0 -> a | B | &
C0 -> a | B | &
D0 -> a | B | &
E0 -> a | B | &
F0 -> a | B | &
G0 -> a | B | &
H0 -> a | B | &
I0 -> a | B | &
J0 -> a | B | &
K0 -> a | B | &
L0 -> a | B | &
M0 -> a | B | &
N0 -> a | B | &
O0 -> a | B | &
P0 -> a | B | &
Q0 -> a | B | &
R0 -> a | B | &
T0 -> a | B | &
U0 -> a | B | &
V0 -> a | B | &
W0 -> a | B | &
Y0 -> a | B | &
X0 -> a | B | &
Z0 -> a | B | &
*/
/*
S -> A a S | &
A -> S A a | B b | C d
B -> C B c | S A B a
C -> B c | S C a | A b | &
 */
	/*
	 * Testes realizados na interface:
	 * [OK] NaoTerminal criado no programa nao possui mesmo nome dos informados pelo usuario
	 * [OK] Mostra se eh finita/infinita
	 * [OK] Lista de gramaticas fica com rolagem (adicionei umas 60 gramaticas e deu boa)
	 * [OK] Add/salva gramatica
	 * [OK] Mostra se eh fatorada
	 * [OK] Remove as gramaticas "proprias" ao salvar edicao (somente da tela da gramatica, mas se elas foram adicionadas na lista de gramaticas elas continuam la)
	 */
	
	/* Testes realizados nos algoritmos
	 * [Arrumar] Construir gramatica: dois terminais seguidos sem espaco
	 * [Arrumar] Gerar Ne: dois terminais estao sendo separados por um espaco
	 * [Arrumar] Se linguagem for vazia ele nao pega follow
	 * [Arrumar] Epsilon livre criando producao com dois epsilon
	 * [???????] Fatorada esta correta?
	 */
	
	/* 
	 * [OK] 1.1. Editar textualmente GLC
	 * [OK] 2.1. Verificar se L(G) eh vazia
	 * [OK] 2.2. Verificar se L(G) eh finita/infinita
	 * [OK] 3.1. Transformar uma GLC em GLC Própria
	 * [So GERAR] 3.1.2.    Gerar Gramatica NF intermediario
	 * [So GERAR] 3.1.3.    Gerar Gramatica Vi intermediario
	 * [So GERAR] 3.1.4.    Gerar Gramatica Ne intermediario
	 * [So GERAR] 3.1.5.    Gerar Gramatica Na intermediario
	 * [So GERAR] 3.1.6.    Gerar Gramatica Epsilon-livre interm.
	 * [So GERAR] 3.1.7.    Gerar Gramatica ciclos-livre interm.
	 * [So GERAR] 3.1.8.    Gerar Gramatica inferteis-livre interm.
	 * [OK] 4.1. Calcular First(A)
	 * [OK] 4.2. Calcular Follow(A)
	 * [OK] 4.3. Calcular First-NT(A) para todo A ∈ Vn
	 * [OK] 5.1. Verificar se G está fatorada
	 * [  ] 5.2  Verificar se G é fatorável em n passos
	 * [OK] 6.1. Verificar se G possui (ou não) recursao a esquerda
	 * [OK] 6.1.1.    Identificar os não-terminais recursivos a esquerda
	 * [OK] 6.1.2.    Identificar os tipo das recursões (direta e/ou indireta)
	 * [OK] 6.2.1 Eliminar as recursões a esquerda diretas
	 * [OK] 6.2.2 Eliminar as recursões a esquerda indiretas
	 */
	
	public static void main(String[] args) {
		Manager manager;
		manager = new Manager();
		manager.visible_principal();
		
		OperarGramatica operarGramatica;
		operarGramatica = new OperarGramatica();
		
		//Main.testar_fatoracao_n_passos(operarGramatica, 50);
		//Main.testar_recursao_indireta(operarGramatica);
		//Main.testar_recursao_direta(operarGramatica);
		//Main.testar_recursao_tipo(operarGramatica);
		//Main.testar_possui_recursao_esquerda(operarGramatica);
		//Main.testar_first_nt(operarGramatica);
		//Main.testar_eh_fatorada(operarGramatica);
		//Main.testar_gramatica_propria(operarGramatica);
		//Main.testar_conjunto_ne(operarGramatica);
		//Main.testar_conjunto_na(operarGramatica);
		//Main.testar_conjunto_ne(operarGramatica);
		//Main.testar_follow(operarGramatica);
		//Main.testar_first(operarGramatica);
		//Main.testar_vazio(operarGramatica);
		//Main.testar_finitude(operarGramatica);
	}
	
	public static void testar_fatoracao_n_passos(OperarGramatica operarGramatica, int n_passos) {
		String producoes;
		//producoes = "S -> a S k | a B z\nB -> aeeeo | hlyui";
		producoes = "P -> D L | L\nD -> d D | &\nL -> L ; C | L + P | C | P\nC -> V = e | i ( E )\nV -> i [ E ] | i\nE -> e , E | e";
		producoes = "P -> D L | L \nD -> d D | & \nL -> L ; C | C \nC -> V = e | i ( E ) \nV -> i [ E ] | i \nE -> e, E | e";
		producoes = "P -> D L | L \nD -> d D | & \nL -> L ; C | C \nC -> V = e | i ( E ) | i D | i P | i E\nV -> i [ E ] | i \nE -> e, E | e";
		
		Gramatica gramatica;
		gramatica = new Gramatica();
		gramatica.set_producoes(producoes);
		
		System.out.println(producoes);
		
		System.out.println("---------");
		
		for (int k = 1; k < 10; k++) {
			System.out.println("K: "+k);
			n_passos = k;
			Gramatica g;
			g = operarGramatica.get_fatoravel_n_passos(gramatica, n_passos);
			
			if (g != null) {
				gramatica = g;
				
				ConjuntoNaoTerminal naoTerminais;
				naoTerminais = gramatica.get_naoTerminais();
				
				for (int c = 0; c < naoTerminais.size(); c++) {
					NaoTerminal naoTerminal;
					naoTerminal = naoTerminais.get(c);
					
					naoTerminal.print();
					//naoTerminal.print2();
				}
				
				/*	S -> a G1
					B -> a | h
					G1 -> S | B
				 * 
					S -> a G1
					B -> a | h
					G1 -> a G1 S | a B | h B
				 * S e o B nao foram retirados da G1
				 * 
				 * */
				
			} else {
				System.out.println("errow");
				
			}
			
			System.out.println("-----");
		}
	}
	public static void testar_recursao_indireta(OperarGramatica operarGramatica) {
		String producoes;
		producoes = "E -> E + T | E - T | T, T -> T * F | T / F | F, F -> F ^ P | P, P -> - P | + P | ( E ) | id";
		//producoes = "S -> B d | &, B -> A b | B c, A -> S a | &";
		//producoes = "K -> B d | &, S -> B d, B -> A b | B c | b, A -> S a | a";
		//producoes = "P -> D L | L\nD -> d D | &\nL -> L ; C | L + P | C | P\nC -> V = exp | id ( E )\nV -> id [ E ] | id\nE -> exp , E | exp";
		producoes = "S -> A a S | &\nA -> S A a | B b | C d\nB -> C B c | S A B a\nC -> B c | S C a | A b | &";
		producoes = "S -> A a | a, A -> B a, B -> S a | a";
		producoes = producoes.replaceAll(", ", "\n");
		
		System.out.println(producoes);
		System.out.println("\n");
		
		Gramatica gramatica;
		gramatica = new Gramatica(Gramatica.get_novo_nome());
		gramatica.set_producoes(producoes);
		
		gramatica = operarGramatica.get_remover_recursao_esquerda_indireta(gramatica);
		
		ConjuntoNaoTerminal naoTerminais;
		naoTerminais = gramatica.get_naoTerminais();
		
		for (int c = 0; c < naoTerminais.size(); c++) {
			NaoTerminal naoTerminal;
			naoTerminal = naoTerminais.get(c);
			
			naoTerminal.print();
		}
	}
	
	public static void testar_recursao_direta(OperarGramatica operarGramatica) {
		String producoes;
		producoes = "E -> E + T | E - T | T, T -> T * F | T / F | F, F -> F ^ P | P, P -> - P | + P | ( E ) | id";
		producoes = "S -> B d | &, B -> A b | B c, A -> S a | &";
		//producoes = "P -> D L | L\nD -> d D | &\nL -> L ; C | L + P | C | P\nC -> V = exp | id ( E )\nV -> id [ E ] | id\nE -> exp , E | exp";
		producoes = "S -> A a S | &\nA -> S A a | B b | C d\nB -> C B c | S A B a\nC -> B c | S C a | A b | &";
		producoes = "S -> A a | a, A -> B a, B -> S a | a";
		producoes = producoes.replaceAll(", ", "\n");
		
		System.out.println(producoes);
		System.out.println("\n");
		
		Gramatica gramatica;
		gramatica = new Gramatica(Gramatica.get_novo_nome());
		gramatica.set_producoes(producoes);
		
		gramatica = operarGramatica.get_remover_recursao_esquerda_direta(gramatica);
		
		ConjuntoNaoTerminal naoTerminais;
		naoTerminais = gramatica.get_naoTerminais();
		
		for (int c = 0; c < naoTerminais.size(); c++) {
			NaoTerminal naoTerminal;
			naoTerminal = naoTerminais.get(c);
			
			naoTerminal.print();
		}
	}
	
	public static void testar_recursao_tipo(OperarGramatica operarGramatica) {
		String producoes;
		producoes = "S -> A a S | &\nA -> S A a | B b | C d\nB -> C B c | S A B a\nC -> B c | S C a | A b | &";
		producoes = producoes.replaceAll(", ", "\n");
		
		System.out.println(producoes);
		Gramatica gramatica;
		gramatica = new Gramatica(Gramatica.get_novo_nome());
		gramatica.set_producoes(producoes);
		
		ConjuntoNaoTerminal naoTerminaisRecursaoDireta, naoTerminaisRecursaoIndireta;
		naoTerminaisRecursaoDireta = new ConjuntoNaoTerminal();
		naoTerminaisRecursaoIndireta = new ConjuntoNaoTerminal();
		
		operarGramatica.get_recursao_esquerda_tipo(gramatica, naoTerminaisRecursaoDireta, naoTerminaisRecursaoIndireta);
		
		System.out.print("Direta = {");
		for (int c = 0; c < naoTerminaisRecursaoDireta.size(); c++) {
			NaoTerminal naoTerminal;
			naoTerminal = naoTerminaisRecursaoDireta.get(c);
			
			System.out.print(naoTerminal.get_simbolo());
			
			if (c != naoTerminaisRecursaoDireta.size()-1) {
				System.out.print(", ");
			}
		}
		System.out.println("}\n\n");
		
		System.out.print("Indireta = {");
		for (int c = 0; c < naoTerminaisRecursaoIndireta.size(); c++) {
			NaoTerminal naoTerminal;
			naoTerminal = naoTerminaisRecursaoIndireta.get(c);
			
			System.out.print(naoTerminal.get_simbolo());
			
			if (c != naoTerminaisRecursaoIndireta.size()-1) {
				System.out.print(", ");
			}
		}
		System.out.println("}");
	}
	public static void testar_possui_recursao_esquerda(OperarGramatica operarGramatica) {
		String producoes;
		producoes = "E -> E + T | E - T | T, T -> T * F | T / F | F, F -> F ^ P | P, P -> - P | + P | ( E ) | id";
		producoes = "S -> A a S | &\nA -> S A a | B b | C d\nB -> C B c | S A B a\nC -> B c | S C a | A b | &";
		producoes = "S -> B d | &, B -> A b | B c, A -> S a | &";
		
		producoes = "S -> A a A | a, A -> S a";
		
		producoes = producoes.replaceAll(", ", "\n");
		
		System.out.println(producoes);
		Gramatica gramatica;
		gramatica = new Gramatica(Gramatica.get_novo_nome());
		gramatica.set_producoes(producoes);
		
		System.out.println(operarGramatica.get_possui_recursao_esquerda(gramatica));
	}
	
	public static void testar_first_nt(OperarGramatica operarGramatica) {
		String producoes;
		//producoes = "S -> A b | A B c\nB -> b B | A d | &\nA -> a A | &";
		//producoes = "S -> A B C\nA -> a A | &\nB -> b B | A C d\nC -> c C | &";
		//producoes = "S1 -> a S1 | S2 b A | c B | A B\nS2 -> d | &\nA -> e B\nB -> f A | g | &";
		producoes = "E -> T E1\nE1 -> + T E1 | &\nT -> F T1\nT1 -> F T1 | &\nF -> ( E ) | a";
		//producoes = "E -> T E1\nE1 -> + T E1 | &\nT -> F T1\nT1 -> * F T1 | &\nF -> ( E ) | i";
		//producoes = "S -> A b C D | E F\nA -> a A | &\nC -> E C F | c\nD -> C D | d D d | &\nE -> e E | &\nF -> F S | f F | g";
		//producoes = "S -> A a A | a\nA -> S a";
		producoes = "E -> E + T | E - T | T, T -> T * F | T / F | F, F -> F ^ P | P, P -> - P | + P | ( E ) | id";
		producoes = "S -> A S | &\nA -> & | S A a | B b | C d\nB -> C B c | S A B a\nC -> B c | S C a | A b | &";
		producoes = producoes.replaceAll(", ", "\n");
		
		
		System.out.println(producoes);
		System.out.println();
		
		Gramatica gramatica;
		gramatica = new Gramatica();
		gramatica.set_producoes(producoes);
		
		operarGramatica.construir_first_nt(gramatica);
		
		ConjuntoNaoTerminal naoTerminais;
		naoTerminais = gramatica.get_naoTerminais();
		
		for (int i = 0; i < naoTerminais.size(); i++) {
			NaoTerminal naoTerminal;
			naoTerminal = naoTerminais.get(i);
			
			ConjuntoNaoTerminal firsts;
			firsts = naoTerminal.get_firstsNt();
			
			System.out.print(naoTerminal.get_simbolo()+": ");
			for (int c = 0; c < firsts.size(); c++) {
				NaoTerminal first;
				first = firsts.get(c);
				
				System.out.print(first.get_simbolo()+" ");
			}
			
			System.out.println();
		}
	}
	public static void testar_eh_fatorada(OperarGramatica operarGramatica) {
		String producoes;
		producoes = "P -> D L | L\nD -> d D | &\nL -> L ; C | C\nC -> V = exp | id ( E )\nV -> id [ E ] | id\nE -> exp , E | exp";
		producoes = "S -> a S b | A C\nA -> a A b | a D | b E\nC -> c C | &\nD -> a D | &\nE -> b E | &";
		producoes = "C -> if E then C1 else C | if E then C | com\nC1 -> if E then C1 else C1 | com\nE -> exp";
		producoes = "S -> A B | C\nA -> a A b | a b\nB -> c B d | c d\nC -> a C d | a D d\nD -> b D c | b c";
		
		System.out.println(producoes);
		
		Gramatica gramatica;
		gramatica = new Gramatica(Gramatica.get_novo_nome());
		gramatica.set_producoes(producoes);
		
		Fatoracao fatoracao;
		fatoracao = new Fatoracao();
		
		System.out.println(fatoracao.get_is_fatorada(gramatica));
	}
	public static void testar_gramatica_propria(OperarGramatica operarGramatica) {
		String producoes;
		producoes = "S -> A a S | &\nA -> S A a | B b | C d\nB -> C B c | S A B a\nC -> B c | S C a | A b | &"; //OK
		producoes = "E -> E + T | E - T | T, T -> T * F | T / F | F, F -> F ^ P | P, P -> - P | + P | ( E ) | id"; //OK
		producoes = "S -> B d | &, B -> A b | B c, A -> S a | &"; //OK
		producoes = "P -> K L | b K L e\nK -> c K | T V\nT -> t T | &\nV -> v V | &\nL -> L C | C\nC -> P | com | &"; //OK
		producoes = "S -> A b B | A D\nA -> a A | B\nB -> S B D | C D\nC -> c C | A S | &\nD -> d D | &"; //OK
		
		
		producoes = producoes.replaceAll(", ", "\n");
		
		System.out.println(producoes);
		
		TransformarGramaticaPropria transformar;
		transformar = new TransformarGramaticaPropria();
		
		Gramatica gramatica;
		gramatica = new Gramatica();
		gramatica.set_producoes(producoes);
		
		ArrayList<Gramatica> gramaticas;
		gramaticas = transformar.get_gramatica_propria(gramatica);
		
		Gramatica gramaticaPropria;
		gramaticaPropria = gramaticas.get( gramaticas.size()-1 );
		
		ConjuntoNaoTerminal naoTerminais;
		naoTerminais = gramaticaPropria.get_naoTerminais();
		
		System.out.println("Gramatica propria: ");
		for (int c = 0; c < naoTerminais.size(); c++) {
			NaoTerminal naoTerminal;
			naoTerminal = naoTerminais.get(c);
			
			naoTerminal.print();
		}
		
		System.out.println(":)");
	}
	public static void testar_conjunto_vi(OperarGramatica operarGramatica) {
		
	}
	public static void testar_conjunto_nf(OperarGramatica operarGramatica) {
		
	}
	public static void testar_conjunto_na(OperarGramatica operarGramatica) {
		String producoes;
		producoes = "P -> P | a | S\nS -> S | a | J | P\nJ -> S | P";
		producoes = "S1 -> P | &\n"
				+ "P -> K L | b K L e | K | L | b K e | b L e | b e\n"
				+ "K -> c K | T V | c | T | V\n"
				+ "T -> t T | t\nV -> v V | v\n"
				+ "L -> L C | C | L\n"
				+ "C -> P | com";
		
		producoes = "S1 -> S | &\n"
				+ "S -> A b B | A D | A b | b | b B | A | D\n"
				+ "A -> a A | B | a\n"
				+ "B -> S B D | C D | S | B | D | S B | S D | B D | C\n"
				+ "C -> c C | A S | c | A | S\n"
				+ "D -> d D | d";
		
		System.out.println("--------------------------------------");
		
		ArrayList<ArrayList<String>> conjuntosNa;
		conjuntosNa = new ArrayList<ArrayList<String>>();
		
		TransformarGramaticaPropria transformar;
		transformar = new TransformarGramaticaPropria();
		
		Gramatica gramatica;
		gramatica = new Gramatica();
		gramatica.set_producoes(producoes);
		
		gramatica = transformar.get_ciclos_livre(gramatica);
		
		System.out.println("--------------------------------------");
		ConjuntoNaoTerminal naoTerminais;
		naoTerminais = gramatica.get_naoTerminais();
		
		for (int c = 0; c < naoTerminais.size(); c++) {
			NaoTerminal naoTerminal;
			naoTerminal = naoTerminais.get(c);
			
			naoTerminal.print();
		}
	}
	public static void testar_conjunto_ne(OperarGramatica operarGramatica) {
		String p;
		//p = "P -> K L | b K L e\nK -> c K | T V\nT -> t T | &\nV -> v V | &\nL -> L C | C\nC -> P | c | &";
		//p = "S -> A b B | A D\nA -> a A | B\nB -> S B D | C D\nC -> c C | A S | &\nD -> d D | &";
		
		
		//p = "S -> A a S | &\nA -> S A a | B b | C d\nB -> C B c | S A B a\nC -> B c | S C a | A b | &";
		
		
		/*p =   "S -> A a S | &\n"
			+ "A -> S A a | B b | C d\n"
			+ "B -> C B c | S A B a\n"
			+ "C -> B c | S C a | A b | &";*/
		
		p =   "S -> A a S b S | &\n"
				+ "A -> S A a C d | S A a C | B b | C d\n"
				+ "B -> C B c | S A B a\n"
				+ "C -> B c | S C a | A b | &";
		
		System.out.println(p);
		
		/*
		p = "S -> B d | &\nB -> A b | B c\nA -> S a | &";
		
		p = "S -> B d | &\n"
		  + "B -> A b | B c\n"
		  + "A -> S a | &";
		*/
		
		ArrayList<String> conjuntosNe;
		conjuntosNe = new ArrayList<String>();
		
		TransformarGramaticaPropria transformar;
		transformar = new TransformarGramaticaPropria();
		
		Gramatica gramatica;
		gramatica = new Gramatica();
		gramatica.set_producoes(p);
		
		System.out.println("-----------------");
		gramatica = transformar.get_epsilon_livre(gramatica);
		
		ConjuntoNaoTerminal naoTerminais;
		naoTerminais = gramatica.get_naoTerminais();
		
		System.out.println("-----------------");
		
		// Nao comentei porque nem precisa analisar isso aqui, eh so um printzao
		for (int c = 0; c < naoTerminais.size(); c++) {
			NaoTerminal naoTerminal;
			naoTerminal = naoTerminais.get(c);
			
			ConjuntoObject<Producao> producoes;
			producoes = naoTerminal.get_producoes();
			
			System.out.print(naoTerminal.get_simbolo()+" -> ");
			for (int i = 0; i < producoes.size(); i++) {
				Producao producao;
				producao = producoes.get(i);
				
				for (int g = 0; g < producao.get_sequencia_simbolos().size(); g++) {
					Object simbolo;
					simbolo = producao.get_sequencia_simbolos().get(g);
					
					if (simbolo.getClass() == NaoTerminal.class) {
						System.out.print(((NaoTerminal)simbolo).get_simbolo());
					} else {
						System.out.print((char)simbolo);
					}
					System.out.print(" ");
				}
				if (producoes.size()-1 != i) {
					System.out.print("| ");
				}
			}
			
			System.out.println();
		}
	}
	public static void testar_finitude(OperarGramatica operarGramatica) {
		String producao;
		//infinitas:
		producao = "S -> A b | A B c\nB -> b B | A d | &\nA -> a A | &";
		producao = "S -> A B C\nA -> a A | &\nB -> b B | A C d\nC -> c C | &";
		producao = "S1 -> a S1 | S2 b A | c B | A B\nS2 -> d | &\nA -> e B\nB -> f A | g | &";
		producao = "E -> T E1\nE1 -> + T E1 | &\nT -> F T1\nT1 -> F T1 | &\nF -> ( E ) | a";
		producao = "A1 -> A2 A3\nA2 -> a | A3\nA3 -> A3";
		producao = "S -> a A\nA -> aA | a | aB\nB -> aB | a ";
		producao = "S -> a A\nA -> aA | a | aB\nB -> aB | aA ";
		producao = "S -> A\nA -> aA | a | aB\nB -> a";
		producao = "S -> aA\nA -> aB | &\nB -> aA\n";
		//finitas:
		producao = "S -> A B\nA -> aB | a\nB -> b";
		producao = "S -> a A a | a A b | b A a | b A b | A\nA -> & | a a | b b | a b | b a";
		producao = "S -> A | B\nA -> a\nB -> A";
		
		producao = "S -> A\nA -> S | a";
		
		Gramatica gramatica;
		gramatica = new Gramatica();
		gramatica.set_producoes(producao);
		
		boolean finita;
		finita = operarGramatica.get_gramatica_finita(gramatica);
		
		if(finita) {
			System.out.println("Finita");
		} else {
			System.out.println("Infinita");
		}
	}
	public static void testar_follow(OperarGramatica operarGramatica) {
		String producoes;
		//producoes = "S -> A b | A B c\nB -> b B | A d | &\nA -> a A | &";
		//producoes = "S -> A B C\nA -> a A | &\nB -> b B | A C d\nC -> c C | &";
		//producoes = "S1 -> a S1 | S2 b A | c B | A B\nS2 -> d | &\nA -> e B\nB -> f A | g | &";
		//producoes = "E -> T E1\nE1 -> + T E1 | &\nT -> F T1\nT1 -> F T1 | &\nF -> ( E ) | a";
		//producoes = "E -> T E1\nE1 -> + T E1 | &\nT -> F T1\nT1 -> * F T1 | &\nF -> ( E ) | i";
		
		//producoes = "";
		producoes = "E -> T E1\nE1 -> + T E1 | &\nT -> F T1\nT1 -> * F T1 | &\nF -> ( E ) | id";
		producoes = "S -> A b C D | E F\nA -> a A | &\nC -> E C F | c\nD -> C D | d D d | &\nE -> e E | &\nF -> F S | f F | g";
		
		Gramatica gramatica;
		gramatica = new Gramatica();
		gramatica.set_producoes(producoes);
		
		operarGramatica.construir_follow(gramatica);
		
		ConjuntoNaoTerminal naoTerminais;
		naoTerminais = gramatica.get_naoTerminais();
		
		for (int i = 0; i < naoTerminais.size(); i++) {
			NaoTerminal naoTerminal;
			naoTerminal = naoTerminais.get(i);
			
			Conjunto<Character> follows;
			follows = naoTerminal.get_follows();
			
			System.out.print(naoTerminal.get_simbolo()+": ");
			for (int c = 0; c < follows.size(); c++) {
				char follow;
				follow = follows.get(c);
				
				System.out.print(follow+" ");
			}
			
			System.out.println();
		}
	}
	public static void testar_vazio(OperarGramatica operarGramatica) {
		String producoes;
		//producao = "S -> A b | A B c\nB -> b B | A d | &\nA -> a A | &";
		//producao = "S -> A B C\nA -> a A | &\nB -> b B | A C d\nC -> c C | &";
		//producao = "S1 -> a S1 | S2 b A | c B | A B\nS2 -> d | &\nA -> e B\nB -> f A | g | &";
		//producao = "E -> T E1\nE1 -> + T E1 | &\nT -> F T1\nT1 -> F T1 | &\nF -> ( E ) | a";
		//producao = "E -> T E1\nE1 -> + T E1 | &\nT -> F T1\nT1 -> * F T1 | &\nF -> ( E ) | i";
		producoes = "A1 -> A2 A3\nA2 -> a | A3\nA3 -> A3";
		
		Gramatica gramatica;
		gramatica = new Gramatica();
		gramatica.set_producoes(producoes);
		
		boolean vazia;
		vazia = operarGramatica.get_linguagem_vazia(gramatica);
		
		System.out.println(vazia);
	}
	public static void testar_first(OperarGramatica operarGramatica) {
		String producoes;
		//producao = "S -> A b | A B c\nB -> b B | A d | &\nA -> a A | &";
		//producao = "S -> A B C\nA -> a A | &\nB -> b B | A C d\nC -> c C | &";
		//producao = "S1 -> a S1 | S2 b A | c B | A B\nS2 -> d | &\nA -> e B\nB -> f A | g | &";
		//producao = "E -> T E1\nE1 -> + T E1 | &\nT -> F T1\nT1 -> F T1 | &\nF -> ( E ) | a";
		producoes = "E -> T E1\nE1 -> + T E1 | &\nT -> F T1\nT1 -> * F T1 | &\nF -> ( E ) | i";
		producoes = 
				"S -> A C | C e B | B a\n"
			  + "A -> a A | B C\n"
			  + "C -> c C | &\n"
			  + "B -> b B | A B | &";
		
		Gramatica gramatica;
		gramatica = new Gramatica();
		gramatica.set_producoes(producoes);
		
		operarGramatica.construir_first(gramatica);
		
		ConjuntoNaoTerminal naoTerminais;
		naoTerminais = gramatica.get_naoTerminais();
		
		for (int i = 0; i < naoTerminais.size(); i++) {
			NaoTerminal naoTerminal;
			naoTerminal = naoTerminais.get(i);
			
			Conjunto<Character> firsts;
			firsts = naoTerminal.get_firstsT();
			
			System.out.print(naoTerminal.get_simbolo()+": ");
			for (int c = 0; c < firsts.size(); c++) {
				char first;
				first = firsts.get(c);
				
				System.out.print(first+" ");
			}
			
			System.out.println();
		}
	}
}

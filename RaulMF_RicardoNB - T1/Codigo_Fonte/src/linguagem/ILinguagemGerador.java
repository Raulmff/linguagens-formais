package linguagem;

public interface ILinguagemGerador extends ILinguagem {
	public void set_producoes(String producao);
	public String get_producoes();
}

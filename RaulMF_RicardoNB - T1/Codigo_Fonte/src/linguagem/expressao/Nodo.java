package linguagem.expressao;

import java.util.ArrayList;

public class Nodo {
	private char simbolo;
	private Nodo filhoEsquerda;
	private Nodo filhoDireita;
	
	private boolean direitaIsCostura;
	
	public Nodo(char simbolo) {
		this.simbolo = simbolo;
		this.filhoEsquerda = null;
		this.filhoDireita = null;
		
		this.direitaIsCostura = false;
	}
	
	public char get_simbolo() {
		return this.simbolo;
	}
	
	public void set_filhoEsquerda(Nodo filhoEsquerda) {
		this.filhoEsquerda = filhoEsquerda;
	}
	public void set_filhoDireita(Nodo filhoDireita, boolean direitaIsCostura) {
		this.filhoDireita = filhoDireita;
		this.direitaIsCostura = direitaIsCostura;
	}
	
	public Nodo get_filhoEsquerda() {
		return this.filhoEsquerda;
	}
	public Nodo get_filhoDireita() { 
		return this.filhoDireita;
	}
	
	public boolean get_direita_isCostura() {
		return this.direitaIsCostura;
	}
	
	public boolean get_isLambda() {
		return (this.simbolo == '&');
	}
	
	public void descer(ArrayList<Nodo> composicao) {
		if (this.filhoEsquerda == null) {
			composicao.add(this);
			return;
		}
		this.filhoEsquerda.descer(composicao);
		
		if (this.get_descida_isSubirCostura()) {
			this.filhoDireita.subir(composicao);
			return;
		}
		if (this.get_descida_isDescerDireita()) {
			this.filhoDireita.descer(composicao);
			return;
		}
	}
	private void descer_direita_ate_fim(ArrayList<Nodo> composicao) {
		if (this.get_direita_isCostura()) {
			if (this.filhoDireita.get_isLambda()) {
				composicao.add(this.filhoDireita);
				return;
			}
			
			this.filhoDireita.subir(composicao);
			return;
		}
		
		this.filhoDireita.descer_direita_ate_fim(composicao);
	}
	
	public void subir(ArrayList<Nodo> composicao) {
		switch (this.simbolo) {
			case '*':
				this.filhoEsquerda.descer(composicao);
				this.filhoDireita.subir(composicao);
				
				return;
			case '?':
				this.filhoDireita.subir(composicao);
				
				return;
			case '|':
				this.filhoDireita.descer_direita_ate_fim(composicao);
				
				return;
			case '.':
				this.filhoDireita.descer(composicao);
				return;
			default:
				composicao.add(this);
				return;
		}
	}
	
	private boolean get_descida_isSubirCostura() {
		switch (this.simbolo) {
			case '*':
				return true;
			case '?':
				return true;
			case '|':
				return false;
			case '.':
				return false;
			default:
				return false;
		}
	}
	private boolean get_descida_isDescerDireita() {
		switch (this.simbolo) {
			case '*':
				return false;
			case '?':
				return false;
			case '|':
				return true;
			case '.':
				return false;
			default:
				return false;
		}
	}
	
	
	
	
	
	
	
	
	public String print_bonito(Nodo no) {
		String print;
		print = no+"";
		
		int i;
		i = print.indexOf("@");
		
		return print.substring(i+3);
	}
	
	public void print() {
		System.out.print(this.simbolo+"("+this.print_bonito(this)+")"+" | ");
		
		System.out.print("E: ");
		if (this.filhoEsquerda != null) {
			System.out.print(this.filhoEsquerda.simbolo+"("+this.print_bonito(this.filhoEsquerda)+")");
		} else {
			System.out.print("null     ");
		}
		
		System.out.print(" | D: ");
		
		if (this.filhoDireita != null) {
			System.out.print(this.filhoDireita.simbolo+"("+this.print_bonito(this.filhoDireita)+")");
		} else {
			System.out.print("null     ");
		}
		
		System.out.println();
		
		if (this.filhoEsquerda != null) {
			this.filhoEsquerda.print();
		}
		if (this.filhoDireita != null) {
			if (!this.get_direita_isCostura()) {
				this.filhoDireita.print();
			}
		}
	}
	
}

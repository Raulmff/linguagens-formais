package linguagem.expressao;

import java.util.ArrayList;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import linguagem.ILinguagem;
import linguagem.ILinguagemGerador;

public class Expressao implements ILinguagemGerador {
	private String nome, producoes;
	
	private static int novoNome = 0;
	
	public static boolean valida_expressao(String exp) {
		try {
            Pattern.compile(exp);
        } catch (PatternSyntaxException exception) {
            return false;
        }
		return true;
	}
	
	public Expressao(String nome) {
		this.nome = nome;
		this.producoes = "";
	}
	
	@Override
	public String get_nome() {
		return this.nome;
	}
	@Override
	public void set_producoes(String producoes) {
		this.producoes = producoes;
	}

	@Override
	public String get_producoes() {
		return this.producoes;
	}

	@Override
	public void set_nome(String nome) {
		this.nome = nome;
	}

	public static String get_novo_nome() {
		String nome;
		nome = "ER_"+Expressao.novoNome;
		
		Expressao.novoNome++;
		
		return nome;
	}

	@Override
	public boolean get_compativeis(ILinguagem i) {
		return false;
	}

	@Override
	public ArrayList<Character> get_alfabeto() {
		return null;
	}
	
	public static String explicitar_concatenacao(String expressao) {
		expressao = expressao.replaceAll(" ", "");
		expressao = expressao.replaceAll("\n", "");
		
		for (int i = 0; i < expressao.length()-1; i++) {
			char anterior = expressao.charAt(i);
			char proximo = expressao.charAt(i+1);
			
			String antes, depois;
			antes = "";
			depois = expressao;
			
			//Caso letra seguido de letra
			if(Character.isLetter(anterior) && Character.isLetter(proximo) ||
					Character.isDigit(anterior) && Character.isDigit(proximo)) {
				antes = expressao.substring(0, i+1);
				depois = expressao.substring(i+1);
			} else if (anterior == ')' && proximo == '(') {
				antes = expressao.substring(0, i+1);
				depois = expressao.substring(i+1);
			} else if (anterior == '*' && ( proximo == '(' ||  
					( Character.isLetter(proximo) || Character.isLetter(proximo) ))) {
				antes = expressao.substring(0, i+1);
				depois = expressao.substring(i+1);
			} else if (anterior == '?' && ( proximo == '(' || (Character.isLetter(proximo) || Character.isDigit(proximo) ) ) )  {
				antes = expressao.substring(0, i+1);
				depois = expressao.substring(i+1);
			} else if ((Character.isLetter(anterior) || Character.isDigit(anterior))  && proximo == '(') {
				antes = expressao.substring(0, i+1);
				depois = expressao.substring(i+1);
			}
			
			if(!antes.equals("")) {
				expressao = antes + "." + depois;
			}
			
		}
		
		
		return expressao;
	}
	
	private int obterPrecedencia(char operador) {
        if (operador == '*' || operador == '?') {
            return 1;
        } else if (operador == '.') {
            return 2;
        } else if (operador == '|') {
            return 3;
        }
        return -1;
    }
	
	public static int get_fecha_parenteses(String expressao, int abre) {
		return Expressao.get_fecha_parenteses(expressao, abre, abre+1);
	}
	public static int get_fecha_parenteses(String expressao, int abre, int fecha) {
		if (fecha <= 0) {
			return -1;
		}
		
		int fecha2;
		fecha2 = expressao.indexOf(")", fecha);
		
		int abre2;
		abre2 = expressao.indexOf("(", abre+1);
		
		System.out.println(expressao+" | "+abre+" | "+abre2+" | "+fecha2);
		
		if (abre2 > fecha2 || abre2 < 0) {
			System.out.println("retorna");
			return fecha2;
		}
		
		fecha2 += 1;
		
		return Expressao.get_fecha_parenteses(expressao, abre2, fecha2);
	}
	
	public Nodo criar_arvore(String expressao) {
		int indiceAbre, indiceFecha;
		indiceAbre = expressao.indexOf("(");
		indiceFecha = Expressao.get_fecha_parenteses(expressao, indiceAbre);
		//System.out.println("expressao: \""+expressao+"\"");
		
		if (indiceAbre > -1) {
			String exp2;
			exp2 = expressao.substring(indiceAbre+1, indiceFecha);
			
			System.out.println("abre: "+(indiceAbre+1));
			System.out.println("fecha: "+indiceFecha);
			
			Expressao subExpressao;
			subExpressao = new Expressao("");
			subExpressao.set_producoes(exp2);
			
			System.out.println("exp2: \""+exp2+"\"");
			
			Nodo nod;
			nod = subExpressao.criar_arvore(exp2);
			
			String exp3;
			exp3 = expressao.substring(indiceFecha+1);
			
			System.out.println("exp3: \""+exp3+"\"");
			
			Nodo pai;
			
			if (!exp3.equals("")) {
				char exp4;
				exp4 = exp3.substring(0, 1).charAt(0);
				
				pai = new Nodo(exp4);
				pai.set_filhoEsquerda(nod);
				
				System.out.println("exp4: \""+exp4+"\"");
				
				if ((exp4 == '*' || exp4 == '?') && exp3.length() > 1) {
					//System.out.println("oi");
					Nodo pai2;
					pai2 = new Nodo( exp3.substring(1,2).charAt(0) );
					pai2.set_filhoEsquerda(pai);
					
					Expressao subExpressao2;
					subExpressao2 = new Expressao("");
					subExpressao2.set_producoes(exp3.substring(2));
					
					Nodo direita;
					direita = subExpressao2.criar_arvore(subExpressao2.get_producoes());
					
					pai2.set_filhoDireita(direita, false);
					pai = pai2;
				} else if (exp4 != '*' && exp4 != '?') {
					Expressao subExpressao2;
					subExpressao2 = new Expressao("");
					subExpressao2.set_producoes(exp3.substring(1));
					
					Nodo direita;
					direita = subExpressao2.criar_arvore(subExpressao2.get_producoes());
					
					pai.set_filhoDireita(direita, false);
				}
			} else {
				pai = nod;
			}
			
			return pai;
		}
		
		int expressaoLength;
		expressaoLength = expressao.length();
		
		if (expressaoLength == 0) {
			return null;
		}
		if (expressaoLength == 1) {
			char simboloNodo;
			simboloNodo = expressao.charAt(0);
			
			Nodo nodo;
			nodo = new Nodo(simboloNodo);
			return nodo;
		}
		
		int posicaoMenorPrecedencia;
		posicaoMenorPrecedencia = this.get_indice_precedencia(expressao, 0);
		
		String expEsquerda, expDireita;
		expEsquerda = expressao.substring(0, posicaoMenorPrecedencia);
		expDireita = expressao.substring(posicaoMenorPrecedencia+1);
		
		char simboloNodo;
		simboloNodo = expressao.charAt(posicaoMenorPrecedencia);
		
		Nodo filhoEsquerda, filhoDireita;
		filhoDireita = this.criar_arvore(expDireita);
		filhoEsquerda = this.criar_arvore(expEsquerda);
		
		Nodo nodoPai;
		nodoPai = new Nodo(simboloNodo);
		nodoPai.set_filhoDireita(filhoDireita, false);
		nodoPai.set_filhoEsquerda(filhoEsquerda);
		
		return nodoPai;
	}
	
	public void criar_costura(Nodo raiz) {
		Nodo lambda;
		lambda = new Nodo('&');
		
		ArrayList<Nodo> pais;
		pais = new ArrayList<Nodo>();
		pais.add(lambda);
		
		this.set_costura(pais, raiz);
	}
	
	private void set_costura(ArrayList<Nodo> pais, Nodo nodo) {
		Nodo filhoEsquerda, filhoDireita;
		filhoEsquerda = nodo.get_filhoEsquerda();
		filhoDireita = nodo.get_filhoDireita();
		
		if (filhoEsquerda == null) {
			Nodo costura;
			costura = this.get_costura(pais);
			
			nodo.set_filhoDireita(costura, true);
			return;
		} else {
			pais.add(nodo);
			this.set_costura(pais, filhoEsquerda);
		}
		
		if (filhoDireita == null) {
			Nodo costura;
			costura = this.get_costura(pais);
			
			nodo.set_filhoDireita(costura, true);
		} else  {
			this.set_costura(pais, filhoDireita);
		}
	}
	
	private Nodo get_costura(ArrayList<Nodo> pais) {
		int indiceCostura;
		indiceCostura = pais.size()-1;
		
		Nodo costura;
		costura = pais.get(indiceCostura);
		
		pais.remove(indiceCostura);
		
		return costura;
	}
	
	private int get_indice_precedencia(String operacao, int begin) {
		int posicaoMenor, nivelPrecedencia;
		posicaoMenor = -2;
		nivelPrecedencia = -2;
		
		for (int c = begin; c < operacao.length(); c++) {
			char simbolo;
			simbolo = operacao.charAt(c);
			
			int precedencia;
			precedencia = this.obterPrecedencia(simbolo);
			
			if (precedencia > nivelPrecedencia) {
				nivelPrecedencia = precedencia;
				posicaoMenor = c;
			}
		}
		
		return posicaoMenor;
	}
}

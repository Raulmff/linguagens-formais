package linguagem.expressao;

import java.util.ArrayList;

import linguagem.automato.Estado;

public class Composicao {
	private Estado estado;
	private char simboloAlfabeto;
	private ArrayList<Nodo> nodos;
	
	public Composicao(char simbolo) {
		this.nodos = new ArrayList<Nodo>();
		this.estado = new Estado( "sera_alterado_depois" );
		this.simboloAlfabeto = simbolo;
	}
	
	public void add_nodo(Nodo nodo) {
		this.nodos.add(nodo);
	}
	
	public boolean get_isLambda() {
		return (this.simboloAlfabeto == '&');
	}
	
	public char get_simboloAlfabeto() {
		return this.simboloAlfabeto;
	}
	
	public Estado get_estado() {
		return this.estado;
	}
	public ArrayList<Nodo> get_nodos() {
		return this.nodos;
	}
	
	public Composicao get_equals(ArrayList<Composicao> composicoes) {
		for (int c = 0; c < composicoes.size(); c++) {
			Composicao composicaoComparar;
			composicaoComparar = composicoes.get(c);
			
			if (this.get_equals(composicaoComparar)) {
				return composicaoComparar;
			}
		}
		
		return null;
	}
	
	public boolean get_equals(Composicao composicaoEstado) {
		if (composicaoEstado.simboloAlfabeto != this.simboloAlfabeto) {
			return false;
		}
		if (composicaoEstado.size_nodos() != this.size_nodos()) {
			return false;
		}
		
		ArrayList<Nodo> nodosFolhaComparar;
		nodosFolhaComparar = composicaoEstado.nodos;
		
		for (int c = 0; c < this.size_nodos(); c++) {
			Nodo nodo;
			nodo = this.nodos.get(c);
			
			if (!nodosFolhaComparar.contains(nodo)) {
				return false;
			}
		}
		
		return true;
	}
	
	public int size_nodos() {
		return this.nodos.size();
	}
	
	public void print() {
		for (int c = 0; c < this.nodos.size(); c++) {
			System.out.print(this.nodos.get(c).get_simbolo()+",");
		}
	}
}

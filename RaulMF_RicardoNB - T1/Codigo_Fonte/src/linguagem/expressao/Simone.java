package linguagem.expressao;

import java.util.ArrayList;

import linguagem.AlfabetoPortugues;
import linguagem.EOperacao;
import linguagem.automato.Automato;
import linguagem.automato.Estado;
import linguagem.automato.Transicao;

public class Simone {
	private ArrayList<Composicao> composicoes;
	private Automato automato;
	
	private AlfabetoPortugues alfabetoPortugues;
	private ArrayList<Character> alfabeto;
	
	public Simone() {
		this.composicoes = new ArrayList<Composicao>();
		this.alfabetoPortugues = new AlfabetoPortugues();
		this.alfabeto = new ArrayList<Character>();
	}
	
	public Automato gerar_automato(Expressao expressao) {
		this.automato = new Automato(Automato.get_novo_nome());
		this.automato.set_gerador(expressao, null, EOperacao.ER_PARA_AF);
		
		String exp;
		exp = Expressao.explicitar_concatenacao(expressao.get_producoes());
		
		Nodo raiz;
		raiz = expressao.criar_arvore(exp);
		expressao.criar_costura(raiz);
		
		this.gerar_automato(raiz);
		
		this.automato.set_alfabeto(this.alfabeto);
		return this.automato;
	}
	
	private void gerar_automato(Nodo raiz) {
		ArrayList<Nodo> nodosDescida;
		nodosDescida = new ArrayList<Nodo>();
		
		raiz.descer(nodosDescida);
		
		Estado estadoInicial;
		estadoInicial = new Estado("alterado_depois");
		estadoInicial.set_isInicial(true);
		this.automato.add_estado(estadoInicial);
		
		this.metodo_2(estadoInicial, nodosDescida);
		this.gerar_automato();
		
		this.renomear_estados();
	}
	
	private void gerar_automato() {
		for (int i = 0; i < this.composicoes.size(); i++) {
			Composicao composicao;
			composicao = this.composicoes.get(i);
			
			ArrayList<Nodo> nodosSubidas;
			nodosSubidas = new ArrayList<Nodo>();
			
			ArrayList<Nodo> nodosComposicao;
			nodosComposicao = composicao.get_nodos();
			
			// Para cada composicao do Simone, faz a subida dos nodos
			for (int j = 0; j < nodosComposicao.size(); j++) {
				ArrayList<Nodo> nodosSubida;
				nodosSubida = new ArrayList<Nodo>();
				
				Nodo nodoComposicao;
				nodoComposicao = nodosComposicao.get(j);
				
				Nodo nodoDireita;
				nodoDireita = nodoComposicao.get_filhoDireita();
				
				if (nodoDireita != null) {
					nodoDireita.subir(nodosSubida);
				}
				
				for (int k = 0; k < nodosSubida.size(); k++) {
					Nodo nodoSubida;
					nodoSubida = nodosSubida.get(k);
					
					if (!nodosSubidas.contains(nodoSubida)) {
						nodosSubidas.add(nodoSubida);
					}
				}
			}
			
			Estado estadoComposicao;
			estadoComposicao = composicao.get_estado();
			this.add_estado(estadoComposicao);
			
			this.metodo_2(estadoComposicao, nodosSubidas);
		}
	}
	
	public void metodo_2(Estado estadoNovo, ArrayList<Nodo> nodosSobeDesce) {
		ArrayList<Character> alfabeto;
		ArrayList<Composicao> composicoesNovo;
		
		alfabeto = new ArrayList<Character>();
		composicoesNovo = new ArrayList<Composicao>();
		
		/*
		 * Para cada nodo da subida ou da descida, verifico se
		 * existe uma composicao que ja contenha o simbolo do nodo, se ja contem essa composicao (1a,2a,4a) ou (5b, 7b), ...
		 * entao eu adiciono o nodo nela, se nao eu crio uma nova composicao pra adicionar
		 * o nodo
		 */
		
		for (int i = 0; i < nodosSobeDesce.size(); i++) {
			Nodo nodoDescida;
			nodoDescida = nodosSobeDesce.get(i);
			
			char simboloNodo;
			simboloNodo = nodoDescida.get_simbolo();
			
			if (!alfabeto.contains(simboloNodo)) {
				if (!this.alfabeto.contains(simboloNodo) && simboloNodo != '&') {
					this.alfabeto.add(simboloNodo);
				}
				alfabeto.add(simboloNodo);
				Composicao composicaoNovo;
				composicaoNovo = new Composicao(simboloNodo);
				
				composicoesNovo.add(composicaoNovo);
			}
			
			int indiceComposicao;
			indiceComposicao = alfabeto.indexOf(simboloNodo); // Os array estao sincronizados
			
			Composicao composicaoBuscado;
			composicaoBuscado = composicoesNovo.get(indiceComposicao);
			composicaoBuscado.add_nodo(nodoDescida);
		}
		
		for (int i = 0; i < composicoesNovo.size(); i++) {
			Composicao composicaoNovo;
			composicaoNovo = composicoesNovo.get(i);
			
			Composicao composicaoEquals;
			composicaoEquals = composicaoNovo.get_equals(this.composicoes);
			
			char simboloAlfabeto;
			simboloAlfabeto = composicaoNovo.get_simboloAlfabeto();
			
			if (simboloAlfabeto == '&') {
				estadoNovo.set_isFinal(true);
				continue;
			}
			
			if (composicaoEquals == null) {
				this.composicoes.add(composicaoNovo);
			} else {
				composicaoNovo = composicaoEquals;
			}
			
			Estado destino;
			destino = composicaoNovo.get_estado();
			
			this.add_estado(destino);
			
			Transicao transicao;
			transicao = new Transicao();
			transicao.set_inicio(estadoNovo);
			transicao.set_destino(destino);
			transicao.set_entrada(simboloAlfabeto);
			
			estadoNovo.add_transicao(transicao);
		}
	}
	
	private void add_estado(Estado destino) {
		ArrayList<Estado> estados;
		estados = this.automato.get_estados();
		
		if (!estados.contains(destino)) {
			this.automato.add_estado(destino);
		}
	}
	
	private void renomear_estados() {
		ArrayList<Estado> estados;
		estados = this.automato.get_estados();
		
		Estado inicial;
		inicial = this.automato.get_inicial();
		inicial.set_simbolo( alfabetoPortugues.get_next_simbolo()+"" );
		
		for (int c = 0; c < estados.size(); c++) {
			Estado estado;
			estado = estados.get(c);
			
			if (estado == inicial) {
				continue;
			}
			
			estado.set_simbolo( alfabetoPortugues.get_next_simbolo()+"" );
		}
	}
}

package linguagem.expressao;

import java.util.ArrayList;

public class ArvoreCosturada {
	public ArvoreCosturada() {
		
	}
	
	public Nodo criar_arvore(String expressao) {
		int expressaoLength;
		expressaoLength = expressao.length();
		
		if (expressaoLength == 0) {
			return null;
		}
		if (expressaoLength == 1) {
			char simboloNodo;
			simboloNodo = expressao.charAt(0);
			
			Nodo nodo;
			nodo = new Nodo(simboloNodo);
			return nodo;
		}
		
		int posicaoMenorPrecedencia;
		posicaoMenorPrecedencia = this.get_indice_precedencia(expressao, 0);
		
		String expEsquerda, expDireita;
		expEsquerda = expressao.substring(0, posicaoMenorPrecedencia);
		expDireita = expressao.substring(posicaoMenorPrecedencia+1);
		
		char simboloNodo;
		simboloNodo = expressao.charAt(posicaoMenorPrecedencia);
		
		Nodo filhoEsquerda, filhoDireita;
		filhoDireita = this.criar_arvore(expDireita);
		filhoEsquerda = this.criar_arvore(expEsquerda);
		
		Nodo nodoPai;
		nodoPai = new Nodo(simboloNodo);
		nodoPai.set_filhoDireita(filhoDireita, false);
		nodoPai.set_filhoEsquerda(filhoEsquerda);
		
		this.criar_costura(nodoPai);
		
		return nodoPai;
	}
	private void criar_costura(Nodo raiz) {
		Nodo lambda;
		lambda = new Nodo('&');
		
		ArrayList<Nodo> pais;
		pais = new ArrayList<Nodo>();
		pais.add(lambda);
		
		this.set_costura(pais, raiz);
	}
	
	private void set_costura(ArrayList<Nodo> pais, Nodo nodo) {
		Nodo filhoEsquerda, filhoDireita;
		filhoEsquerda = nodo.get_filhoEsquerda();
		filhoDireita = nodo.get_filhoDireita();
		
		if (filhoEsquerda == null) {
			Nodo costura;
			costura = this.get_costura(pais);
			
			nodo.set_filhoDireita(costura, true);
			return;
		} else {
			pais.add(nodo);
			this.set_costura(pais, filhoEsquerda);
		}
		
		if (filhoDireita == null) {
			Nodo costura;
			costura = this.get_costura(pais);
			
			nodo.set_filhoDireita(costura, true);
		} else  {
			this.set_costura(pais, filhoDireita);
		}
	}
	private Nodo get_costura(ArrayList<Nodo> pais) {
		int indiceCostura;
		indiceCostura = pais.size()-1;
		
		Nodo costura;
		costura = pais.get(indiceCostura);
		
		pais.remove(indiceCostura);
		
		return costura;
	}
	
	private int obterPrecedencia(char operador) {
        if (operador == '*' || operador == '?') {
            return 1;
        } else if (operador == '.') {
            return 2;
        } else if (operador == '|') {
            return 3;
        }
        return -1;
    }
	private int get_indice_precedencia(String operacao, int begin) {
		int posicaoMenor, nivelPrecedencia;
		posicaoMenor = -2;
		nivelPrecedencia = -2;
		
		for (int c = begin; c < operacao.length(); c++) {
			char simbolo;
			simbolo = operacao.charAt(c);
			
			int precedencia;
			precedencia = this.obterPrecedencia(simbolo);
			
			if (precedencia > nivelPrecedencia) {
				nivelPrecedencia = precedencia;
				posicaoMenor = c;
			}
		}
		
		return posicaoMenor;
	}
}

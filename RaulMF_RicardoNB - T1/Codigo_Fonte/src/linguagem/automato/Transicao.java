package linguagem.automato;
public class Transicao {
	private char entrada;
	private Estado destino;
	private Estado inicio;
	
	public Transicao() {
		this.entrada = ' ';
		this.destino = null;
		this.inicio = null;
	}
	
	public void set_inicio(Estado e) {
		this.inicio = e;
	}
	public void set_destino(Estado e) {
		this.destino = e;
	}
	public void set_entrada(char entrada) {
		this.entrada = entrada;
	}
	
	public boolean get_igual(Transicao transicao) {
		//System.out.println(transicao.entrada);
		//System.out.println(this.entrada);
		if (transicao.entrada != this.entrada) {
			return false;
		}
		if (transicao.destino.get_simbolo() != this.destino.get_simbolo()) {
			return false;
		}
		if (transicao.inicio.get_simbolo() != this.inicio.get_simbolo()) {
			return false;
		}
		
		return true;
	}
	
	public char get_entrada() {
		return this.entrada;
	}
	public Estado get_inicio() {
		return this.inicio;
	}
	public Estado get_destino() {
		return this.destino;
	}
}

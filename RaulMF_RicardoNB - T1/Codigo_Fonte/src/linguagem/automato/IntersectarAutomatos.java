package linguagem.automato;

import java.util.ArrayList;

import linguagem.EOperacao;

public class IntersectarAutomatos {

	public IntersectarAutomatos() {}
	
	//Metodo que retorna a interseccao de dois automatos minimizados
	public ArrayList<Automato> get_interseccao(Automato a1_antigo, Automato a2_antigo) {
		Automato a1 = new Automato(a1_antigo);
		Automato a2 = new Automato(a2_antigo);
		
		//Complementando os automatos
		a1 = a1.complementar_automato();
		a1.set_gerador(a1_antigo, null, EOperacao.COMPLEMENTO);
		
		a2 = a2.complementar_automato();
		a2.set_gerador(a2_antigo, null, EOperacao.COMPLEMENTO);
		
		//Chamando a uniao para retornar a uniao dos complementos
		UnirAutomatos uniao = new UnirAutomatos();
		
		ArrayList<Automato> retornoUniao;
		retornoUniao = uniao.get_uniao(a1, a2);
		
		ArrayList<Automato> retornoIntersecao;
		retornoIntersecao = new ArrayList<Automato>();
		retornoIntersecao.add(a1_antigo);
		retornoIntersecao.add(a2_antigo);
		retornoIntersecao.add(a1);
		retornoIntersecao.add(a2);
		retornoIntersecao.addAll(retornoUniao);
		
		Automato automato;
		automato = retornoIntersecao.get(retornoIntersecao.size()-1);
		
		Automato automatoInterseccao;
		automatoInterseccao = automato.complementar_automato();
		automatoInterseccao.set_gerador(a1_antigo, a2_antigo, EOperacao.INTERSECCAO);
		
		retornoIntersecao.add(automatoInterseccao);
		
		MinimizarAutomato minimizar;
		minimizar = new MinimizarAutomato();
		
		Automato automatoMinimo;
		automatoMinimo = minimizar.get_minimizacao(automatoInterseccao);
		automatoMinimo.set_gerador(automatoInterseccao, null, EOperacao.MINIMIZACAO);
		retornoIntersecao.add(automatoMinimo);
		
		return retornoIntersecao;
	}

}

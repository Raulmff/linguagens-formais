package linguagem.automato;

import java.util.ArrayList;

import linguagem.AlfabetoPortugues;
import linguagem.EOperacao;

public class UnirAutomatos {
	public UnirAutomatos() {}
	
	//Metodo para retornar a uniao de dois automatos minimizados
	public ArrayList<Automato> get_uniao(Automato a1_antigo, Automato a2_antigo) {
		ArrayList<Automato> retorno;
		retorno = new ArrayList<Automato>();
		
		//VERIFICAR SE ALFABETOS SAO IGUAIS
		
		Automato a1, a2;
		a1 = new Automato(a1_antigo);
		a2 = new Automato(a2_antigo);
		
		//Para mudar o nome dos estados
		AlfabetoPortugues alf = new AlfabetoPortugues();
		
		//Criando novo estado inicial (algoritmo da uniao)
		Estado inicial;
		inicial = new Estado(String.valueOf(alf.get_next_simbolo()));
		inicial.set_isInicial(true);
		
		//Criando o novo automato resultante da uniao
		Automato a3;
		a3 = new Automato( Automato.get_novo_nome() );
		a3.set_alfabeto(a1_antigo.get_alfabeto());
		a3.add_estado(inicial);
		
		//Adicionando estados com nomes alterados ao novo automato (algoritmo da uniao)
		for (int i = 0; i < a1.get_estados().size(); i++) {
			Estado e;
			e = a1.get_estados().get(i);
			e.set_simbolo(String.valueOf(alf.get_next_simbolo()));
			
			a3.add_estado(e);
		}
		for (int i = 0; i < a2.get_estados().size(); i++) {
			Estado e;
			e = a2.get_estados().get(i);
			e.set_simbolo(String.valueOf(alf.get_next_simbolo()));
			
			a3.add_estado(e);
		}
		
		//Verificando se o inicial deve ser final ou nao (algoritmo da uniao)
		Estado inicialA1;
		inicialA1 = a1.get_inicial();
		inicialA1.set_isInicial(false);
		
		Estado inicialA2;
		inicialA2 = a2.get_inicial();
		inicialA2.set_isInicial(false);
		
		if(inicialA1.get_isFinal() || inicialA2.get_isFinal()) {
			inicial.set_isFinal(true);
		}
		
		//Criando as novas transicoes do novo estado inicial
		//Copiando transicoes dos antigos estados iniciais
		for (int i = 0; i < a3.get_alfabeto().size(); i++) {
			char simboloAlfabeto;
			simboloAlfabeto = a3.get_alfabeto().get(i);
			
			for (int j = 0; j < inicialA1.get_transicoes().size(); j++) {
				Transicao transicao;
				transicao = inicialA1.get_transicoes().get(j);
				
				if (transicao.get_entrada() == simboloAlfabeto) {
					Transicao transicaoNovo;
					transicaoNovo = new Transicao();
					transicaoNovo.set_inicio(inicial);
					transicaoNovo.set_entrada(transicao.get_entrada());
					transicaoNovo.set_destino(transicao.get_destino());
					
					inicial.add_transicao(transicaoNovo);
				}
			}
			for (int k = 0; k < inicialA2.get_transicoes().size(); k++) {
				Transicao transicao;
				transicao = inicialA2.get_transicoes().get(k);
				
				if (transicao.get_entrada() == simboloAlfabeto) {
					Transicao transicaoNovo;
					transicaoNovo = new Transicao();
					transicaoNovo.set_inicio(inicial);
					transicaoNovo.set_entrada(transicao.get_entrada());
					transicaoNovo.set_destino(transicao.get_destino());
					
					inicial.add_transicao(transicaoNovo);
				}
			}
		}
		
		a3.set_gerador(a1_antigo, a2_antigo, EOperacao.UNIAO);
		retorno.add(a3);
		
		DeterminizarAutomato determinizar;
		determinizar = new DeterminizarAutomato();
		
		Automato a4;
		a4 = determinizar.get_determinizacao(a3);
		a4.set_gerador(a3, null, EOperacao.DETERMINIZACAO);
		retorno.add(a4);
		
		MinimizarAutomato m3;
		m3 = new MinimizarAutomato();
		
		Automato a5;
		a5 = m3.get_minimizacao(a4);
		a5.set_gerador(a4, null, EOperacao.MINIMIZACAO);
		retorno.add(a5);
		
		return retorno;
	}

}

package linguagem.automato;
import java.util.ArrayList;

public class Estado {
	private String simbolo;
	private boolean isInicial;
	private boolean isFinal;
	private ArrayList<Transicao> transicoes;
	
	/* Metodos Contrutor */
	public Estado(String simbolo) {
		this.simbolo = simbolo;
		this.transicoes = new ArrayList<Transicao>();
		this.isInicial = false;
		this.isFinal = false;
	}
	
	/* Metodos add */
	public void add_transicao(Transicao transicao) {
		if ( !this.get_contains(transicao) ) {
			this.transicoes.add(transicao);
		}
	}
	public void remove_transicao(Transicao transicao) {
		this.transicoes.remove(transicao);
	}
	
	/* Metodos Setter */
	public void set_simbolo(String s) {
		this.simbolo = s;
	}
	public void set_isFinal(boolean s) {
		this.isFinal = s;
	}
	public void set_isInicial(boolean s) {
		this.isInicial = s;
	}
	public void set_transicoes(ArrayList<Transicao> transicoes) {
		this.transicoes = transicoes;
	}
	
	/* Metodos Getter */
	public String get_simbolo() {
		return this.simbolo;
	}
	public boolean get_isFinal() {
		return this.isFinal;
	}
	public boolean get_isInicial() {
		return this.isInicial;
	}
	public boolean get_contains(Transicao transicaoBuscada) {
		for (int c = 0; c < this.transicoes.size(); c++) {
			Transicao transicao;
			transicao = this.transicoes.get(c);
			
			if (transicao.get_igual(transicaoBuscada)) {
				return true;
			}
		}
		
		return false;
	}
	public boolean get_igual(Estado estado) {
		String simbolo;
		simbolo = this.simbolo+"";
		
		String simbolo2;
		simbolo2 = estado.get_simbolo();
		
		if (simbolo2.length() != simbolo.length()) {
			return false;
		}
		if (simbolo2.equals(simbolo)) {
			return true;
		}
		
		int n_ocorrencias, total_ocorrencias;
		n_ocorrencias = 0;
		total_ocorrencias = 0;
		
		do {
			total_ocorrencias = 0;
			String simbolo_1;
			
			if (simbolo.indexOf(",") < 0) {
				break;
			}
			
			simbolo_1 = simbolo.substring(0, simbolo.indexOf(","));
			simbolo = simbolo.substring(simbolo.indexOf(",")+1);
			
			if (simbolo2.indexOf(simbolo_1) >= 0) {
				n_ocorrencias++;
			} else {
				return false;
			}
		} while(!simbolo.equals(""));
		
		if (n_ocorrencias == total_ocorrencias && total_ocorrencias > 0) {
			return true;
		}
		
		return false;
	}
	public ArrayList<Transicao> get_transicoes() {
		return this.transicoes;
	}
	
	// Remover depois
	public void print() {
		if (this.isInicial) {
			System.out.print(">");
		}
		if (this.isFinal) {
			System.out.print("*");
		}
		System.out.print(this.simbolo);
		System.out.println(":");
		
		for (int c = 0; c < this.transicoes.size(); c++) {
			Transicao transicao = transicoes.get(c);
			System.out.print(transicao.get_inicio().get_simbolo()+"->(");
			System.out.print(transicao.get_entrada()+")->");
			System.out.println(transicao.get_destino().get_simbolo());
		}
	}
}

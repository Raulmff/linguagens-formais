package linguagem.automato;

import java.util.ArrayList;

import linguagem.EOperacao;

public class DiferencaAutomatos {
	public DiferencaAutomatos() {}
	
	//Metodo para retornar a diferenca entre dois automatos
	public ArrayList<Automato> get_diferenca(Automato a1_antigo, Automato a2_antigo) {
		Automato a1 = new Automato(a1_antigo);
		Automato a2 = new Automato(a2_antigo);
		
		//Preparando primeiro automato
		MinimizarAutomato m1 = new MinimizarAutomato();
		a1 = m1.get_minimizacao(a1);
		a1.set_gerador(a1_antigo, null, EOperacao.MINIMIZACAO);
		a1.print();
		
		//Preparando segundo automato
		MinimizarAutomato m2 = new MinimizarAutomato();
		a2 = m2.get_minimizacao(a2);
		a2.set_gerador(a2_antigo, null, EOperacao.MINIMIZACAO);
		a2_antigo = a2;
		a2 = a2.complementar_automato(); //Complementando o segundo pela teoria L1 - L2 = L1 inter ~L2
		a2.set_gerador(a2_antigo, null, EOperacao.COMPLEMENTO);
		
		//Chama a interseccao para buscar a diferenca
		IntersectarAutomatos inter = new IntersectarAutomatos();
		
		ArrayList<Automato> retornoIntersecao;
		retornoIntersecao = inter.get_interseccao(a1, a2);
		
		ArrayList<Automato> retornoDiferenca;
		retornoDiferenca = new ArrayList<Automato>();
		retornoDiferenca.add(a1_antigo);
		retornoDiferenca.add(a2_antigo);
		retornoDiferenca.add(a1);
		retornoDiferenca.add(a2);
		retornoDiferenca.addAll(retornoIntersecao);
		
		Automato automato;
		automato = retornoDiferenca.get(retornoDiferenca.size()-1);
		automato.set_gerador(a1_antigo, a2_antigo, EOperacao.DIFERENCA);
		
		MinimizarAutomato minimizar;
		minimizar = new MinimizarAutomato();
		
		Automato automatoMinimo;
		automatoMinimo = minimizar.get_minimizacao(automato);
		automatoMinimo.set_gerador(automato, null, EOperacao.MINIMIZACAO);
		retornoDiferenca.add(automatoMinimo);
		
		return retornoDiferenca;
	}

}

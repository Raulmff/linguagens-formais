package linguagem.automato;

import java.util.ArrayList;

import linguagem.EOperacao;

public class MinimizarAutomato {
	public MinimizarAutomato() {}
	
	public Automato get_minimizacao(Automato automatoOriginal) {
		DeterminizarAutomato determinizar;
		determinizar = new DeterminizarAutomato();
		
		// Para minimizar o automato, ele deve ser determinizado antes
		Automato automatoAfd;
		automatoAfd = determinizar.get_determinizacao(automatoOriginal);
		
		Estado inicialAfd, inicialMin;
		inicialAfd = automatoOriginal.get_inicial();
		inicialMin = new Estado(inicialAfd.get_simbolo());
		
		Automato automatoMinimo;
		automatoMinimo = new Automato( automatoAfd.get_nome() );
		automatoMinimo.copia_alfabeto(automatoAfd);
		automatoMinimo.add_estado(inicialMin);
		
		this.remover_inalcansaveis(automatoMinimo, automatoAfd);
		this.remover_inferteis(automatoMinimo);
		
		Automato automatoMinimoFinal;
		automatoMinimoFinal = this.remover_estados_duplicados(automatoMinimo);
		automatoMinimoFinal.set_gerador(automatoOriginal, null, EOperacao.MINIMIZACAO);
		return automatoMinimoFinal;
	}
	
	private void remover_inalcansaveis(Automato automatoMinimo, Automato automatoOriginal) {
		ArrayList<Estado> estadosFinais;
		estadosFinais = automatoOriginal.get_finais();
		
		Estado estadoInicial;
		estadoInicial = automatoOriginal.get_inicial();
		
		ArrayList<Estado> alcansaveis;
		alcansaveis = new ArrayList<Estado>();
		alcansaveis.add(estadoInicial);
		
		ArrayList<Estado> finaisAlcansaveis;
		finaisAlcansaveis = new ArrayList<Estado>();
		
		// O estado inicial sempre sera alcancavel
		if (estadosFinais.contains(estadoInicial)) {
			finaisAlcansaveis.add(estadoInicial);
		}
		
		// ENQUANTO: forem adicionados novos estados como alcancaveis
		for (int i = 0; i < alcansaveis.size(); i++) {
			Estado alcansavel;
			alcansavel = alcansaveis.get(i);
			
			ArrayList<Transicao> transicoes;
			transicoes = alcansavel.get_transicoes();
			
			/*
			 * Percorre todas as transicoes dos estados alcancaveis, e
			 * para todo estado alcancado pelos estados ja marcados como
			 * alcancaveis, entao marca esses estados alcancados pelos
			 * alcancaveis como tambem sendo alcancaveis.
			 * Ex: seja B um estado alcancavel, e B alcanca C, entao C
			 * marca C como alcalcavel
			 */
			for (int j = 0; j < transicoes.size(); j++) {
				Transicao transicao;
				transicao = transicoes.get(j);
				
				Estado destino;
				destino = transicao.get_destino();
				
				// SE: os estados alcancaveis pelos alcancaveis ainda nao foi marcado
				if (!alcansaveis.contains(destino)) {
					// Adiciona como alcancavel
					alcansaveis.add(destino);
				}
				// Dos "novos" alcancaveis, se ele for final
				if (!finaisAlcansaveis.contains(destino) && destino.get_isFinal()) {
					// Adiciona como um alcancavel que eh final
					finaisAlcansaveis.add(destino);
				}
			}
		}
		
		// Atualiza os estados do automato
		automatoMinimo.set_estados(alcansaveis);
		automatoMinimo.set_finais(finaisAlcansaveis);
	}
	private void remover_inferteis(Automato automatoMinimo) {
		// Todos os estados finais sao ferteis (considerando que ja foram removidos os inalcancaveis)
		ArrayList<Estado> ferteis;
		ferteis = new ArrayList<Estado>(automatoMinimo.get_finais());
		
		ArrayList<Estado> estados;
		estados = automatoMinimo.get_estados();
		
		int numeroNovosFerteis;
		do {
			numeroNovosFerteis = 0;
			
			// Percorre todos os estados para encontrar os ferteis
			for (int j = 0; j < estados.size(); j++) {
				Estado possivelFertil;
				possivelFertil = estados.get(j);
				
				// Verifica se o estado ja foi marcado como fertil
				if (ferteis.contains(possivelFertil)) {
					// Se ja foi marcado, entao verifica o proximo
					continue;
				}
				
				/* 
				 * PARA: os estados que nao estao marcados como ferteis, verifica
				 * as transicoes desses estados. Se eles transitam para um estado
				 * fertil entao tambem sao ferteis.
				 */
				for (int c = 0; c < possivelFertil.get_transicoes().size(); c++) {
					Transicao transicao;
					transicao = possivelFertil.get_transicoes().get(c);
					
					Estado destino;
					destino = transicao.get_destino();
					
					// SE: o estado possivelFertil contem transicao para um estado fertil
					if (ferteis.contains( destino )) {
						// Adiciona o possivelFertil como fertil
						ferteis.add(possivelFertil);
						numeroNovosFerteis++;
						break;
					}
				}
				
			}
			/*
			 * ENQUANTO: novos ferteis forem adicionados, entao refaz a busca
			 * por novos ferteis. Ex:
			 * Se A transita para B, e B transita para um estado fertil C.
			 * A eh verificado, mas como B ainda nao eh fertil entao A nao eh fertil.
			 * B eh verificado, e como C eh fertil entao B eh fertil.
			 * Uma nova busca identifica que A eh fertil porque B virou fertil.
			 */
		} while(numeroNovosFerteis > 0);
		
		// Percorre os estados ferteis
		for (int i = 0; i < ferteis.size(); i++) {
			Estado fertil;
			fertil = ferteis.get(i);
			
			ArrayList<Transicao> novasTransicoes;
			novasTransicoes = new ArrayList<Transicao>();
			
			/*
			 * Percorre todas as transicoes de cada estado fertil para verificar
			 * se a transicao leva a um estado fertil. Se a transicao nao levar,
			 * entao eh descartada.
			 */
			for (int j = 0; j < fertil.get_transicoes().size(); j++) {
				Transicao transicao;
				transicao = fertil.get_transicoes().get(j);
				
				Estado destino;
				destino = transicao.get_destino();
				
				// SE: a transicao leva a um estado fertil
				if (ferteis.contains(destino)) {
					// O estado continua com essa transicao
					novasTransicoes.add(transicao);
				}
			}
			
			fertil.set_transicoes(novasTransicoes);
		}
		
		/*
		 * Atualiza os estados do automato. Os estados finais nao precisam ser atualizados
		 * pois todo estado final eh fertil.
		 */
		automatoMinimo.set_estados(ferteis);
	}
	private Automato remover_estados_duplicados(Automato automato) {
		ArrayList<Estado> finais;
		finais = automato.get_finais();
		
		ArrayList<Estado> naoFinais;
		naoFinais = automato.get_naoFinais();
		
		/*
		 * SE: o automato possui todos os estados como sendo equivalentes,
		 * sendo assim, como tera apenas 1 estado, esse estado sera o proprio
		 * estado inicial
		 */
		if (naoFinais.size() == 0) {
			return this.get_automato_estado_unico(automato);
		}
		
		/*
		 * Sao 3 listas de conjunto de estados. A primeira corresponde, inicialmente,
		 * a dois conjuntos de estados: um contendo os finais, e outro contendo o restante.
		 * A segunda lista corresponde a lista atualizada de conjuntos apos verificar a
		 * igualdade dos estados para cada simbolo do alfabeto. A terceira corresponde a
		 * lista atualizada apos a verificacao de todos os simbolos do alfabeto.
		 */
		ArrayList<ArrayList<Estado>> conjuntos1, conjuntos2, conjuntos;
		conjuntos1 = new ArrayList<ArrayList<Estado>>();
		conjuntos1.add(finais);
		conjuntos1.add(naoFinais);
		
		ArrayList<Character> alfabeto;
		alfabeto = automato.get_alfabeto();
		
		do {
			conjuntos = conjuntos1;
			
			// PARA: cada simbolo do alfabeto, verifica a igualdade dos estados
			for (int i = 0; i < alfabeto.size(); i++) {
				char simboloAlfabeto;
				simboloAlfabeto = alfabeto.get(i);
				
				conjuntos2 = new ArrayList<ArrayList<Estado>>();
				
				/*
				 * PARA: cada conjunto da lista 1, verifica se os estados desse
				 * conjunto possuem transicoes "iguais" para dado simbolo do alfabeto
				 */
				for (int j = 0; j < conjuntos1.size(); j++) {
					ArrayList<Estado> conjunto;
					conjunto = conjuntos1.get(j);
					
					conjuntos2.add(conjunto);
					
					/* SE o conjunto nao tiver mais de 1 estado, nao faz sentido
					 * verificar a igualdade entre os estados do conjunto
					 */
					if (conjunto.size() <= 1) {
						// Verifica o proximo conjunto
						continue;
					}
					
					// Possivel novo conjunto da lista 2
					ArrayList<Estado> conjuntoNovo;
					conjuntoNovo = new ArrayList<Estado>();
					
					/* 
					 * A partir do primeiro estado de cada conjunto da lista 1, verifica para
					 * qual conjunto de estados esse estado transita para um determinado simbolo
					 * do alfabeto.
					 */
					ArrayList<Estado> conjuntoDestino1;
					conjuntoDestino1 = this.get_conjunto(conjuntos1, conjunto.get(0), simboloAlfabeto);
					
					/*
					 * PARA: cada conjunto da lista 1, verifica se possuem transicao, com dado simbolo
					 * do alfabeto, para o mesmo conjunto de estados do primeiro estado do conjunto.
					 */
					for (int c = 1; c < conjunto.size(); c++) {
						Estado estadoC;
						estadoC = conjunto.get(c);
						
						ArrayList<Estado> conjuntoDestino2;
						conjuntoDestino2 = this.get_conjunto(conjuntos1, estadoC, simboloAlfabeto);
						
						// PARA: cada estado do conjunto, se ele nao possui transicao para o mesmo conjunto que o primeiro estado
						if (conjuntoDestino1 != conjuntoDestino2) {
							/* 
							 * Remove ele do conjunto e coloca no novo conjunto (pois esse estado
							 * nao eh equivalente ao primeiro estado do conjunto)
							 */
							conjuntoNovo.add(estadoC);
							conjunto.remove(estadoC);
							c--;
							
							/* O novo conjunto somente eh adicionado na lista atualizada caso
							 * contenha pelo menos um estado nao equivalente
							 */
							if (!conjuntos2.contains(conjuntoNovo)) {
								conjuntos2.add(conjuntoNovo);
							}
						}
					}
				}
				
				// Atualiza a lista de conjuntos caso um novo conjunto tenha sido adicionado
				if (conjuntos1.size() != conjuntos2.size()) {
					conjuntos1 = conjuntos2;
				}
			}
			/* Apos verificar a igualdade de estados, para todos os simbolos do alfabeto, se algum novo estado
			 * foi adicionado, entao refaz a varredura
			 */
		} while(conjuntos.size() != conjuntos1.size());
		
		// Cria o novo automato sem estados duplicados
		Automato automatoMinimo;
		automatoMinimo = new Automato( automato.get_nome() );
		automatoMinimo.set_alfabeto(alfabeto);
		
		// Obtem 1 estado de cada conjunto (pois o restante dos estados do conjunto sao equivalentes a ele)
		for (int c = 0; c < conjuntos.size(); c++) {
			ArrayList<Estado> conjunto;
			conjunto = conjuntos.get(c);
			
			// Obtem um estado com base em sua importancia (Ex: estado inicial)
			Estado estado;
			estado = this.get_seleciona_estado(conjunto);
			
			if (estado.get_isInicial()) {
				automatoMinimo.set_inicial(estado);
			}
			
			automatoMinimo.add_estado(estado);
		}
		
		/* Altera o destino de cada transicao dos estados, de modo que apontem para o estado selecionado
		 * de cada conjunto
		 */
		this.altera_destino_transicoes(conjuntos);
		
		return automatoMinimo;
	}
	
	private void altera_destino_transicoes(ArrayList<ArrayList<Estado>> conjuntos) {
		for (int i = 0; i < conjuntos.size(); i++) {
			ArrayList<Estado> conjunto;
			conjunto = conjuntos.get(i);
			
			/*
			 * PARA: cada estado do novo automato (estado principal de cada conjunto), verifica
			 * para qual conjunto de estados ele transita. Obtendo esse conjunto, faz o estado
			 * transitar para o estado principal do conjunto (estado presente no novo automato).
			 */
			for (int j = 0; j < conjunto.size(); j++) {
				Estado principal;
				principal = this.get_seleciona_estado(conjunto);
				
				for (int c = 0; c < principal.get_transicoes().size(); c++) {
					Transicao transicao;
					transicao = principal.get_transicoes().get(c);
					
					char simboloAlfabeto;
					simboloAlfabeto = transicao.get_entrada();
					
					ArrayList<Estado> conjuntoDestino;
					conjuntoDestino = this.get_conjunto(conjuntos, principal, simboloAlfabeto);
					
					Estado destino;
					destino = this.get_seleciona_estado(conjuntoDestino);
					transicao.set_destino(destino);
				}
			}
		}
		
	}
	private ArrayList<Estado> get_conjunto(ArrayList<ArrayList<Estado>> lista, Estado estado, char simboloAlfabeto) {
		ArrayList<Transicao> transicoes;
		transicoes = estado.get_transicoes();
		
		Estado destino;
		destino = null;
		
		/*
		 * Se o estado possui transicao com determinado simbolo do alfabeto,
		 * Obtem o estadoDestino dessa transicao, e a partir dele retorna o grupo
		 * a qual ele pertence.
		 */
		for (int c = 0; c < transicoes.size(); c++) {
			Transicao transicao;
			transicao = transicoes.get(c);
			
			if (transicao.get_entrada() == simboloAlfabeto) {
				destino = transicao.get_destino();
				
				for (int i = 0; i < lista.size(); i++) {
					ArrayList<Estado> conjunto;
					conjunto = lista.get(i);
					
					if (conjunto.contains(destino)) {
						return conjunto;
					}
				}
			}
		}
		
		/* 
		 * SE o estado nao possuir transicao para o simbolo do alfabeto,
		 * entao retorna um conjunto null
		 */
		return null;
	}
	/*
	 * Retorna o estado inicial (caso ele exista) ou o primeiro estado do conjunto
	 */
	private Estado get_seleciona_estado(ArrayList<Estado> conjunto) {
		for (int c = 0; c < conjunto.size(); c++) {
			Estado estado;
			estado = conjunto.get(c);
			
			if (estado.get_isInicial()) {
				return estado;
			}
		}
		
		return conjunto.get(0);
	}
	
	private Automato get_automato_estado_unico(Automato automato) {
		String nome;
		nome = automato.get_nome();
		
		Estado inicial;
		inicial = automato.get_inicial();
		
		Automato minimo;
		minimo = new Automato(nome);
		minimo.add_estado(inicial);
		minimo.set_alfabeto(automato.get_alfabeto());
		
		ArrayList<Transicao> array;
		array = inicial.get_transicoes();
		
		/*
		 * Percorre todas as transicoes do estado e as modificada
		 * para transitarem para o proprio estado (unico estado do
		 * automato minimo gerado)
		 */
		for (int c = 0; c < array.size(); c++) {
			Transicao transicao;
			transicao = array.get(c);
			transicao.set_destino(inicial);
		}
		
		return minimo;
	}
}

package linguagem.automato;

import java.util.ArrayList;

import linguagem.EOperacao;

public class DeterminizarAutomato {
	public DeterminizarAutomato() {}
	
	public Automato get_determinizacao(Automato automatoAfnd) {
		ArrayList<Character> alfabeto;
		alfabeto = automatoAfnd.get_alfabeto();
		
		Estado inicialAfnd;
		inicialAfnd = automatoAfnd.get_inicial();
		
		String nomeAutomatoAfd;
		nomeAutomatoAfd = Automato.get_novo_nome();
		
		Automato automatoAfd;
		automatoAfd = new Automato(nomeAutomatoAfd);
		automatoAfd.set_alfabeto(alfabeto);
		automatoAfd.set_gerador(automatoAfnd, null, EOperacao.DETERMINIZACAO);
		
		this.determinizacao_estado_inicial(automatoAfd, alfabeto, inicialAfnd);
		
		/*
		 * Percorre todos os estadosAFD para gerar ou instanciar (caso ja existam)
		 * os estadosDestino para o qual os estadosAFD transitam. Caso sejam gerados
		 * novos estados, eles sao adicionados ao AFD e sao tambem iterados por esse
		 * loop
		 */
		for (int i = 0; i < automatoAfd.get_estados().size(); i++) {
			Estado estadoAfd;
			estadoAfd = automatoAfd.get_estados().get(i);
			
			/*
			 * Obtem os estadosDestino das transicoes AFND, para dado simbolo
			 * do alfabeto, para posteriormente gerar as transicoes AFD
			 */
			for (int j = 0; j < alfabeto.size(); j++) {
				char simboloAlfabeto;
				simboloAlfabeto = alfabeto.get(j);
				
				String simboloDestinoAfd;
				simboloDestinoAfd = "";
				
				for (int k = 0; k < automatoAfnd.get_estados().size(); k++) {
					Estado estadoAfnd;
					estadoAfnd = automatoAfnd.get_estados().get(k);
					
					/*
					 * SE: o estado AFD foi derivado do estado AFND, entao passa,
					 * mas se nao for derivado, entao cai no if
					 * Ex: {A,B} foi gerado pelos estados AFND A e B, entao passa,
					 * mas {A,B} nao foi derivado do estado AFND C, entao cai no if
					 */
					if (estadoAfd.get_simbolo().indexOf(estadoAfnd.get_simbolo()) < 0) {
						// Busca o proximo possivel estado AFND que gere estadoAFD
						continue;
					}
					
					if (estadoAfnd.get_isFinal()) {
						estadoAfd.set_isFinal(true);
					}
					
					ArrayList<Transicao> transicoes;
					transicoes = estadoAfnd.get_transicoes();
					
					simboloDestinoAfd += this.get_simbolo_afd(transicoes, simboloAlfabeto);
				}
				
				/* SE: o estadoInicial AFND nao possui transicoes, para dado
				 * simbolo do alfabeto, entao passa para o proximo simbolo do
				 * alfabeto
				 */
				if (simboloDestinoAfd.equals("")) {
					continue;
				}
				
				// Adiciona as transicoes ao estadoAFD
				this.add_transicao_afd(automatoAfd, estadoAfd, simboloDestinoAfd, simboloAlfabeto);
			}
		}
		
		return automatoAfd;
	}
	
	private void determinizacao_estado_inicial(Automato automatoAfd, ArrayList<Character> alfabeto, Estado inicialAfnd) {
		// O estado inicial AFND tambem sera estado inicial AFD
		String nomeInicial;
		nomeInicial = "{"+inicialAfnd.get_simbolo()+"}";
		
		Estado inicialAfd;
		inicialAfd = new Estado(nomeInicial);
		inicialAfd.set_isInicial(true);
		
		automatoAfd.add_estado(inicialAfd);
		
		/*
		 * Percorre todas as transicoes do estadoInicial AFND para um dado
		 * simbolo do alfabeto
		 */
		for (int k = 0; k < alfabeto.size(); k++) {
			char simboloAlfabeto;
			simboloAlfabeto = alfabeto.get(k);
			
			/*
			 * O novo estado AFD para o qual o estadoInicial AFD transitara
			 * tera todos os simbolos de todos os estados AFND para o qual o
			 *  estadoInicial AFND transitava
			 */
			ArrayList<Transicao> transicoes;
			transicoes = inicialAfnd.get_transicoes();
			
			String simboloDestinoAfd;
			simboloDestinoAfd = "";
			simboloDestinoAfd += this.get_simbolo_afd(transicoes, simboloAlfabeto);
			
			/* SE: o estadoInicial AFND nao possui transicoes, para dado
			 * simbolo do alfabeto, entao passa para o proximo simbolo do
			 * alfabeto
			 */
			if (simboloDestinoAfd.equals("")) {
				continue;
			}
			
			// Adiciona as transicoes ao estadoAFD
			this.add_transicao_afd(automatoAfd, inicialAfd, simboloDestinoAfd, simboloAlfabeto);
		}
	}
	
	private void add_transicao_afd(Automato automatoAfd, Estado estadoAfd, String simboloDestinoAfd, char simboloAlfabeto) {
		// Remove virgula em excesso
		simboloDestinoAfd = simboloDestinoAfd.substring(0, simboloDestinoAfd.length()-1);
		simboloDestinoAfd = "{"+simboloDestinoAfd+"}";
		
		Estado destinoAfd;
		destinoAfd = this.get_estado_destino_afd(automatoAfd, simboloDestinoAfd);
		
		Transicao transicao;
		transicao = new Transicao();
		transicao.set_destino(destinoAfd);
		transicao.set_inicio(estadoAfd);
		transicao.set_entrada(simboloAlfabeto);
		
		estadoAfd.add_transicao(transicao);
	}
	/*
	 * Ao se criar um estado AFD, ele cria tambem novos estados que sao
	 * obtidos pela transicao por dado simbolo do alfabeto. Por exemplo:
	 * {S}->(a)->{A,B}. Quando isso acontece, o {A,B} eh adicionado na
	 * lista de estados do AFD. Ao entrar nesse for, eh feito a verificacao
	 * se esse estado ja existe na lista de estados. Isso porque, pode
	 * existir, por exemplo, um {A} que tambem tenha transicao para {A,B},
	 * ou seja, ele tentaria adicionar o estado novamente.
	 */
	private Estado get_estado_destino_afd(Automato automatoAfd, String nomeEstadoDestinoAfd) {
		Estado destinoAfd;
		destinoAfd = new Estado(nomeEstadoDestinoAfd);
		
		// PARA: cada estado do AFD, verifica se esse novo estado ja existe
		for (int c = 0; c < automatoAfd.get_estados().size(); c++) {
			Estado estado;
			estado = automatoAfd.get_estados().get(c);
			
			// Verifica a existencia
			if (estado.get_igual(destinoAfd)) {
				destinoAfd = estado;
				break;
			}
		}
		
		// SE: o AFD ainda nao possui o estado que esta sendo "criado"
		if (!automatoAfd.get_estados().contains(destinoAfd)) {
			automatoAfd.add_estado(destinoAfd);
		}
		
		return destinoAfd;
	}
	
	private String get_simbolo_afd(ArrayList<Transicao> transicoes, char simboloAlfabeto) {
		String simboloDestinoAfd;
		simboloDestinoAfd = "";
		
		for (int c = 0; c < transicoes.size(); c++) {
			Transicao transicaoAfnd;
			transicaoAfnd = transicoes.get(c);
			
			if (transicaoAfnd.get_entrada() != simboloAlfabeto) {
				continue;
			}
			
			String simboloDestinoAfnd;
			simboloDestinoAfnd = transicaoAfnd.get_destino().get_simbolo();
			
			if (simboloDestinoAfd.indexOf(simboloDestinoAfnd) < 0) {
				simboloDestinoAfd += transicaoAfnd.get_destino().get_simbolo()+",";
			}
		}
		
		
		return simboloDestinoAfd;
	}
}

package linguagem.automato;

import java.util.ArrayList;

import linguagem.AlfabetoPortugues;
import linguagem.EOperacao;

public class ConcatenarAutomatos {

	public ConcatenarAutomatos() {}

	//Metodo para retornar a concatenacao de 2 automatos
	public ArrayList<Automato> get_concatenacao(Automato a1_antigo, Automato a2_antigo) {
		//CONFERIR SE ALFABETOS SAO IGUAIS
		
		Automato a1 = new Automato(a1_antigo);
		Automato a2 = new Automato(a2_antigo);
		
		AlfabetoPortugues alfabeto;
		alfabeto = new AlfabetoPortugues();
		
		Estado inicialA1;
		inicialA1 = a1.get_inicial();
		inicialA1.set_simbolo(String.valueOf(alfabeto.get_next_simbolo()));
		
		Automato a3;
		a3 = new Automato(Automato.get_novo_nome());
		a3.set_alfabeto(a1.get_alfabeto());
		a3.add_estado(inicialA1);
		a3.set_gerador(a1_antigo, a2_antigo, EOperacao.CONCATENACAO);
		
		Estado inicialA2;
		inicialA2 = a2.get_inicial();
		
		// Estados de a1 incluidos no automato
		for(int i = 0; i < a1.get_estados().size(); i++) {
			Estado estado = a1.get_estados().get(i);
			
			if (estado.get_isFinal()) {
				for(int j = 0; j < inicialA2.get_transicoes().size(); j++) {
					Transicao novaTransicao;
					novaTransicao = new Transicao();
					
					Transicao transicao;
					transicao = inicialA2.get_transicoes().get(j);
					
					novaTransicao.set_inicio(estado);
					novaTransicao.set_entrada(transicao.get_entrada());
					novaTransicao.set_destino(transicao.get_destino());
					
					estado.add_transicao(novaTransicao);
				}
				
				estado.set_isFinal(false);
			}
			
			if(estado != inicialA1) {
				a3.add_estado(estado);
			}
		}
		
		// Estados de a2 incluidos no automato
		for(int i = 0; i < a2.get_estados().size(); i++) {
			Estado estado;
			estado = a2.get_estados().get(i);
			
			if(estado.get_isInicial()) {
				estado.set_isInicial(false);
			}
			
			a3.add_estado(estado);
		}

		// Mudando os nomes
		for (int i = 1; i < a3.get_estados().size(); i++) {
			Estado estado;
			estado = a3.get_estados().get(i);
			estado.set_simbolo(String.valueOf(alfabeto.get_next_simbolo()));
		}
		
		ArrayList<Automato> retorno;
		retorno = new ArrayList<Automato>();
		retorno.add(a1_antigo);
		retorno.add(a2_antigo);
		retorno.add(a3);
		
		MinimizarAutomato minimizar;
		minimizar = new MinimizarAutomato();
		
		Automato a4;
		a4 = minimizar.get_minimizacao(a3);
		a4.set_gerador(a3, null, EOperacao.MINIMIZACAO);
		
		retorno.add(a4);
		return retorno;
	}
	
}

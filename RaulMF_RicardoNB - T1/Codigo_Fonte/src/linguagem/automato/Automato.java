package linguagem.automato;
import java.util.ArrayList;

import linguagem.EOperacao;
import linguagem.ILinguagem;

public class Automato implements ILinguagem {
	private String nome;
	private Estado inicial;
	private ILinguagem automatoPai1, automatoPai2;
	private EOperacao operacaoGerador;
	private int n_sentencas;
	
	private ArrayList<Estado> estados;
	private ArrayList<Estado> finais;
	private ArrayList<Character> alfabeto;
	private ArrayList<Transicao> transicoes;
	
	private static int novoNome = 0;
	public static String get_novo_nome() {
		Automato.novoNome++;
		return "AF"+Automato.novoNome;
	}
	
	/* Metodos Construtor */
	public Automato(String nome) {
		this.nome = nome;
		this.inicial = null;
		this.estados = new ArrayList<Estado>();
		this.finais = new ArrayList<Estado>();
		this.alfabeto = new ArrayList<Character>();
		this.transicoes = new ArrayList<Transicao>();
		this.n_sentencas = 0;
		
		this.operacaoGerador = null;
		this.automatoPai1 = null;
		this.automatoPai2 = null;
	}
	public Automato(Automato automato) {
		this(automato.get_nome());
		
		this.alfabeto = automato.get_alfabeto();
		
		for (int c = 0; c < automato.estados.size(); c++) {
			Estado estado, estadoNovo;
			estado = automato.estados.get(c);
			
			estadoNovo = new Estado(estado.get_simbolo());
			estadoNovo.set_isInicial(estado.get_isInicial());
			estadoNovo.set_isFinal(estado.get_isFinal());
			
			if (estado.get_isInicial()) {
				this.inicial = estadoNovo;
			}
			
			this.add_estado(estadoNovo);
		}
		
		for (int c = 0; c < automato.estados.size(); c++) {
			Estado estado, estadoNovo;
			estado = automato.estados.get(c);
			estadoNovo = this.estados.get(c);
			
			ArrayList<Transicao> transicoes;
			transicoes = estado.get_transicoes();
			
			for (int i = 0; i < transicoes.size(); i++) {
				Transicao transicao, novaTransicao;
				transicao = transicoes.get(i);
				
				novaTransicao = new Transicao();
				novaTransicao.set_entrada( transicao.get_entrada() );
				novaTransicao.set_inicio(estadoNovo);
				
				Estado destino;
				destino = transicao.get_destino();
				
				for (int j = 0; j < this.estados.size(); j++) {
					Estado destinoNovo;
					destinoNovo = this.estados.get(j);
					
					String nomeDestinoNovo;
					nomeDestinoNovo = destinoNovo.get_simbolo();
					
					if ( nomeDestinoNovo.equals(destino.get_simbolo()) ) {
						novaTransicao.set_destino(destinoNovo);
						break;
					}
				}
				
				estadoNovo.add_transicao(novaTransicao);
			}
		}
	}
	
	public void add_estado(Estado estado) {
		if (this.inicial == null) {
			this.inicial = estado;
		}
		if (estado.get_isFinal() && !this.finais.contains(estado)) {
			this.finais.add(estado);
		}
		
		this.estados.add(estado);
	}
	
	/* Metodos Setter */
	@Override
	public void set_nome(String nome) {
		this.nome = nome;
	}
	public void set_inicial(Estado inicial) {
		this.inicial = inicial;
	}
	public void set_finais(ArrayList<Estado> finais) {
		this.finais = finais;
	}
	public void set_alfabeto(ArrayList<Character> a) {
		this.alfabeto = a;
	}
	public void set_estados(ArrayList<Estado> estados) {
		this.estados = estados;
		this.obter_estados_finais();
		this.obter_estado_inicial();
	}
	public void copia_alfabeto(Automato automato) {
		this.alfabeto = automato.alfabeto;
	}
	public void set_gerador(ILinguagem automatoPai1, ILinguagem automatoPai2, EOperacao operacaoGerador) {
		this.operacaoGerador = operacaoGerador;
		this.automatoPai1 = automatoPai1;
		this.automatoPai2 = automatoPai2;
	}
	
	/* Metodos Getter */
	@Override
	public String get_nome() {
		return this.nome;
	}
	public Estado get_inicial() {
		return this.inicial;
	}
	public ArrayList<Estado> get_finais() {
		return this.finais;
	}
	public ArrayList<Estado> get_estados() {
		return this.estados;
	}
	public ArrayList<Estado> get_naoFinais() {
		ArrayList<Estado> naoFinais;
		naoFinais = new ArrayList<Estado>();
		
		for (int c = 0; c < this.estados.size(); c++) {
			Estado estado;
			estado = this.estados.get(c);
			
			if (!this.finais.contains(estado)) {
				naoFinais.add(estado);
			}
		}
		
		return naoFinais;
	}
	@Override
	public ArrayList<Character> get_alfabeto() {
		return this.alfabeto;
	}
	public ILinguagem get_automato_pai1() {
		return this.automatoPai1;
	}
	public ILinguagem get_automato_pai2() {
		return this.automatoPai2;
	}
	public String get_operacao_gerador() {
		String operacao;
		
		if (this.operacaoGerador == null) {
			return null;
		}
		
		switch (this.operacaoGerador) {
			case CONCATENACAO:
				operacao = "CONCATENACAO";
				break;
			case DETERMINIZACAO:
				operacao = "DETERMINIZACAO";
				break;
			case DIFERENCA:
				operacao = "DIFERENCA";
				break;
			case FECHAMENTO:
				operacao = "FECHAMENTO";
				break;
			case INTERSECCAO:
				operacao = "INTERSECCAO";
				break;
			case MINIMIZACAO:
				operacao = "MINIMIZACAO";
				break;
			case UNIAO:
				operacao = "UNIAO";
				break;
			case COMPLEMENTO:
				operacao = "COMPLEMENTO";
				break;
			case REVERSO:
				operacao = "REVERSO";
				break;
			case GR_PARA_AF:
				operacao = "GR PARA AF";
			case ER_PARA_AF:
				operacao = "ER PARA AF";
				break;
			default:
				operacao = null;
				break;
		}
		
		return operacao;
	}
	public static boolean get_existe(Automato automato, String estadoNovo) {
		for (int c = 0; c < automato.estados.size(); c++) {
			Estado estado;
			estado = automato.estados.get(c);
			
			if (estado.get_simbolo().equals(estadoNovo)) {
				return true;
			}
		}
		
		return false;
	}
	public static boolean get_existe_transicao(Transicao transicaoBuscada, Estado e) {
		for (int c = 0; c < e.get_transicoes().size(); c++) {
			Transicao transicao = e.get_transicoes().get(c);
			
			boolean entradaIgual, destinoIgual, inicioIgual;
			entradaIgual = transicao.get_entrada() != transicaoBuscada.get_entrada();
			destinoIgual = transicao.get_destino().get_simbolo() != transicaoBuscada.get_destino().get_simbolo();
			inicioIgual = transicao.get_inicio().get_simbolo() != transicaoBuscada.get_inicio().get_simbolo();
			
			if(entradaIgual & destinoIgual & inicioIgual) {
				return true;
			}
		}
		
		return false;
	}
	
	@Override
	public boolean get_compativeis(ILinguagem linguagem) {
		ArrayList<Character> alfabetoComparar;
		alfabetoComparar = linguagem.get_alfabeto();
		
		// Se um alfabeto possuir mais simbolos que o outro alfabeto
		if (this.alfabeto.size() != alfabetoComparar.size()) {
			return false;
		}
		
		for (int c = 0; c < this.alfabeto.size(); c++) {
			char simboloAlfabeto;
			simboloAlfabeto = this.alfabeto.get(c);
			
			// Se algum simbolo de um alfabeto nao existir no outro alfabeto
			if (!alfabetoComparar.contains(simboloAlfabeto)) {
				return false;
			}
		}
		
		return true;
	}
	
	/* Metodos (outros) */
	private void obter_estados_finais() {
		for (int c = 0; c < this.estados.size(); c++) {
			Estado estado = this.estados.get(c);
			
			if (estado.get_isFinal()) {
				this.finais.add(estado);
			}
		}
	}
	private void obter_estado_inicial() {
		for (int c = 0; c < this.estados.size(); c++) {
			Estado estado = this.estados.get(c);
			
			if (estado.get_isInicial()) {
				this.inicial = estado;
				return;
			}
		}
	}
	
	//Completar automato caso nao haja transicoes para todos os elementos do alfabeto
	public Automato completar_automato() {
		
		Automato a3 = new Automato(this);
		a3.set_nome(Automato.get_novo_nome());
		
		//Criando novo estado de erro
		Estado erro;
		erro = new Estado("erro");
		
		//Para cada elemento do alfabeto
		for (int k = 0; k < a3.alfabeto.size(); k++) {
			char elemAlfabeto = a3.alfabeto.get(k);
			
			int size = a3.estados.size();
			//Para cada estado do automato
			for (int i = 0; i < size; i++) {
				Estado estado = a3.estados.get(i);
				
				//Variavel que informa se ha ou nao transicoes para tal elemento
				boolean possui = false;
				
				//Para cada transicao do estado
				for (int j = 0 ; j < estado.get_transicoes().size(); j++) {
					Transicao transicao = estado.get_transicoes().get(j);
					if (transicao.get_entrada() == elemAlfabeto) {
						possui = true;
					}
				}
				//Se nao ha transicoes
				if(!possui) {
					//Adiciona estado de erro se ja nao existir
					if(!a3.estados.contains(erro)) {
						a3.add_estado(erro);
					}
					//Cria nova transicao para o estado de erro
					Transicao t = new Transicao();
					t.set_entrada(a3.alfabeto.get(k));
					t.set_inicio(estado);
					t.set_destino(erro);
					//Adiciona transicao ao estado
					estado.add_transicao(t);
				}
			}
		}
		
		for (int k = 0; k < a3.alfabeto.size(); k++) {
			Transicao t = new Transicao();
			t.set_entrada(a3.alfabeto.get(k));
			t.set_inicio(erro);
			t.set_destino(erro);
			erro.add_transicao(t);
		}
		
		return a3;
		
	}
	
	//Metodo para complementar um automato
	public Automato complementar_automato() {
		
		Automato a3 = new Automato(this);
		
		//Primeiramente completa o automato
		a3 = a3.completar_automato();
		
		//Inverte os estados (final vira nao final e vice-versa)
		for (int i = 0; i < a3.estados.size(); i++) {
			Estado estado = a3.estados.get(i);
			if (estado.get_isFinal() == true) {
				estado.set_isFinal(false);
			} else {
				estado.set_isFinal(true);
			}
		}
		
		return a3;
	}
	
	//Metodo para buscar todas as transicoes dos estados dos automato
	private void set_transicoes_automato() {
		
		//Para cada transicao existente, adiciona nas transicoes do automato
		for(int i = 0; i < this.estados.size(); i++) {
			Estado estado = this.estados.get(i);
			for(int j = 0; j < estado.get_transicoes().size(); j++) {
				Transicao transicao = estado.get_transicoes().get(j);
				this.transicoes.add(transicao);	
			}
		}
	}
	
	//Metodo para excluir as transicoes atuais do automato
	private void limpar_transicoes() {
		for(int i = 0; i < this.estados.size(); i++) {
			Estado estado = this.estados.get(i);
			ArrayList<Transicao> transicoes_estado = estado.get_transicoes();
			transicoes_estado.clear();;
		}
	}
	
	//Metodo para setar as novas transicoes dos estados depois de fazer o reverso
	private void set_transicoes_estados_reverso() {
		//Para cada estado
		for(int i = 0; i < this.estados.size(); i++) {
			Estado estado = this.estados.get(i);
			//Setar as novas transicoes
			for(int j = 0; j < this.transicoes.size(); j++) {
				Transicao transicao = this.transicoes.get(j);
				if(transicao.get_inicio() == estado) {
					estado.add_transicao(transicao);
				}
			}
		}
		
	}
	
	//Metodo para realizar o reverso de um automato
	public Automato get_reverso() {
		Automato a3 = new Automato(this);
		
		MinimizarAutomato minimizar;
		minimizar = new MinimizarAutomato();
		
		a3 = minimizar.get_minimizacao(a3);
		
		//Seta as transicoes do automato
		a3.set_transicoes_automato();
		
		//Exclui as transicoes atuais
		a3.limpar_transicoes();
		
		ArrayList<Transicao> novas_transicoes = new ArrayList<Transicao>();
		
		//Reverso das transicoes
		for(int i = 0; i < a3.transicoes.size(); i++) {
			Transicao transicao = a3.transicoes.get(i);
			Transicao transicao_nova = new Transicao();
			transicao_nova.set_destino(transicao.get_inicio());
			transicao_nova.set_entrada(transicao.get_entrada());
			transicao_nova.set_inicio(transicao.get_destino());
			
			novas_transicoes.add(transicao_nova);
		}
		
		//Seta as transicoes como as ja revertidas
		a3.transicoes = novas_transicoes;
		
		//Seta as transicoes dos estados
		a3.set_transicoes_estados_reverso();
		
		//Desmarcar antigo como inicial
		Estado antigo_inicial = a3.get_inicial();
		antigo_inicial.set_isInicial(false);
		
		//Criar novo estado inicial
		Estado novo_inicial = new Estado("qi");
		novo_inicial.set_isInicial(true);
		//Seta-lo como inicial do automato
		a3.set_inicial(novo_inicial);
		
		//Se antigo inicial era final, o novo tbm deve ser
		if(antigo_inicial.get_isFinal()) {
			novo_inicial.set_isFinal(true);
		}
		
		//Setar transicoes do novo estado inicial
		for(int i = 0; i < a3.estados.size(); i++) {
			Estado estado = a3.estados.get(i);
			if(estado.get_isFinal()) {
				for(int j = 0; j < estado.get_transicoes().size(); j++) {
					
					Transicao transicao = estado.get_transicoes().get(j);
					Transicao transicao_nova = new Transicao();
					transicao_nova.set_destino(transicao.get_destino());
					transicao_nova.set_entrada(transicao.get_entrada());
					transicao_nova.set_inicio(novo_inicial);
					novo_inicial.add_transicao(transicao_nova);
					
					//Transicao transicao = estado.get_transicoes().get(j);
					//novo_inicial.add_transicao(transicao);
				}
				estado.set_isFinal(false);
			}
		}
		
		//Adicionar novo estado inicial ao automato
		a3.add_estado(novo_inicial);
		
		//Marcar antigo inicial como final
		antigo_inicial.set_isFinal(true);
		
		a3 = minimizar.get_minimizacao(a3);
		a3.set_gerador(this, null, EOperacao.REVERSO);
		
		return a3;
	}
	
	//Metodo para realizar o fechamento de um automato
	public Automato get_fechamento() {
		
		Automato a3 = new Automato(this);
		
		Estado inicial;
		inicial = a3.get_inicial();
		
		//Replica as transicoes do inicial no final
		for (int i = 0; i < a3.estados.size(); i++) {
			Estado estado;
			estado = a3.estados.get(i);
			
			if(estado.get_isFinal()) {
				for (int j = 0; j < inicial.get_transicoes().size(); j++) {
					Transicao transicao;
					transicao = new Transicao();
					
					Transicao t;
					t = inicial.get_transicoes().get(j);
					
					transicao.set_entrada(t.get_entrada());
					transicao.set_inicio(estado);
					transicao.set_destino(t.get_destino());
					
					estado.add_transicao(transicao);
				}
			}
		}
		return a3;
	}
	
	//Metodo para verificar se uma sentenca eh gerada por um automato
	public boolean get_sentenca_pertence(String sentenca) {
		Estado inicial = this.get_inicial();
		
		//Estado auxiliar no percorrimento no automato
		Estado atual = inicial;
		
		//Verificar se sentenca eh igual a epsilon
		if (sentenca.equals("&")) {
			if(atual.get_isFinal()) {
				return true;
			} else {
				return false;
			}
		}
		
		//Faz o percorrimento no automato
		//Para cada caracter da sentenca
		for(int i = 0; i < sentenca.length(); i++) {
			
			//Booleano auxiliar
			boolean existe = false;
			
			if(atual.get_transicoes().size() == 0) {
				return false;
			}
			
			//Para cada transicao do automato atual
			for (int j = 0; j < atual.get_transicoes().size(); j++) {
				Transicao transicao = atual.get_transicoes().get(j);
				
				//Se estado possui a transicao, estado atual recebe o destino da transicao
				if(transicao.get_entrada() == sentenca.charAt(i)) {
					existe = true;
					atual = transicao.get_destino();
					break;
				}
				
				//Caso nao haja transicao com o caracter da sentenca, return false
				if(j == atual.get_transicoes().size() - 1 && existe == false) {
					return false;
				}
			}
		}
		
		//Se o estado atual (ultimo visitado) for final, return true
		if(atual.get_isFinal()) {
			return true;
		}
		
		return false;
	}
	
	//Metodo que retorna numero de sentencas de tamanho n
	public int enumera_tamanho_n(int n) {
		this.n_sentencas = 0;
		
		//Preparando para a iteracao inicial
		int tamanho_sentenca = 0;
		Estado inicial = this.get_inicial();
		
		//Chama metodo recursivo para percorrer automato
		this.caminha_automato(inicial, tamanho_sentenca, n);
		
		return this.n_sentencas;
	}
	
	//Metodo recursivo para percorrer automato
	public boolean caminha_automato(Estado estado, int tamanho_sentenca, int n) {
		//Para a recursao se atingiu o tamanho pedido
		if(tamanho_sentenca == n) {
			if(estado.get_isFinal()) {
				this.n_sentencas++;
				return true;
			}
		} else {
			//Array que armazena os estados a serem visitados
			ArrayList<Estado> a_visitar = new ArrayList<Estado>();
			
			//Para cada transicao, adicionar o destino aos estados a visitar
			for(int i = 0; i < estado.get_transicoes().size(); i++) {
				Transicao transicao = estado.get_transicoes().get(i);
				a_visitar.add(transicao.get_destino());
			}
			
			//Se o array de a visitar nao for vazio, chama recursao atualizada
			if(!a_visitar.isEmpty()) {
				for(int i = 0; i < a_visitar.size(); i++) {
					this.caminha_automato(a_visitar.get(i), tamanho_sentenca+1, n);
				}
			} else {
				return false;
			}
		}
				
		return false;
	}
	
	// REMOVER DEPOIS
	public void print() {
		for (int c = 0; c < this.estados.size(); c++) {
			System.out.println("estado");
			this.estados.get(c).print();
		}
	}
}

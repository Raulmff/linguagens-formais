package linguagem;

import java.util.ArrayList;

import linguagem.automato.Automato;
import linguagem.automato.ConcatenarAutomatos;
import linguagem.automato.DeterminizarAutomato;
import linguagem.automato.DiferencaAutomatos;
import linguagem.automato.IntersectarAutomatos;
import linguagem.automato.MinimizarAutomato;
import linguagem.automato.UnirAutomatos;
import linguagem.expressao.Expressao;
import linguagem.gramatica.Gramatica;

public class ManagerILinguagem {
	private ArrayList<ILinguagem> automatos;
	private ArrayList<ILinguagem> gramaticas;
	private ArrayList<ILinguagem> expressoes;
	
	private MinimizarAutomato minimizar;
	private DeterminizarAutomato determinizar;
	private UnirAutomatos unir;
	private ConcatenarAutomatos concatenar;
	private IntersectarAutomatos intersectar;
	private DiferencaAutomatos diferenca;
	
	public ManagerILinguagem() {
		this.minimizar = new MinimizarAutomato();
		this.determinizar = new DeterminizarAutomato();
		this.unir = new UnirAutomatos();
		this.concatenar = new ConcatenarAutomatos();
		this.intersectar = new IntersectarAutomatos();
		this.diferenca = new DiferencaAutomatos();
		
		this.automatos = new ArrayList<ILinguagem>();
		this.gramaticas = new ArrayList<ILinguagem>();
		this.expressoes = new ArrayList<ILinguagem>();
	}
	
	public void add_gramatica(Gramatica gramatica) {
		if (!this.gramaticas.contains(gramatica)) {
			this.gramaticas.add(gramatica);
		}
	}
	public void add_automato(Automato automato) {
		if (!this.automatos.contains(automato)) {
			this.automatos.add(automato);
		}
	}
	public void add_expressao(Expressao expressao) {
		if (!this.expressoes.contains(expressao)) {
			this.expressoes.add(expressao);
		}
	}
	
	public void remover_automato(Automato automato) {
		this.automatos.remove(automato);
	}
	public void remover_expressao(Expressao expressao) {
		this.expressoes.remove(expressao);
	}
	public void remover_gramatica(Gramatica gramatica) {
		this.gramaticas.remove(gramatica);
	}
	public void remover_ilinguagem(ILinguagem iLinguagem, ELinguagem eLinguagem) {
		switch (eLinguagem) {
			case AUTOMATO:
				this.remover_automato((Automato)iLinguagem);
				break;
			case EXPRESSAO:
				this.remover_expressao((Expressao)iLinguagem);
				break;
			case GRAMATICA:
				this.remover_gramatica((Gramatica)iLinguagem);
				break;
			default:
				break;
		}
	}
	
	public ArrayList<ILinguagem> get_automatos() {
		return this.automatos;
	}
	public ArrayList<ILinguagem> get_expressoes() {
		return this.expressoes;
	}
	public ArrayList<ILinguagem> get_gramaticas() {
		return this.gramaticas;
	}
	public ArrayList<ILinguagem> get_array_iLinguagem(ELinguagem eLinguagem) {
		switch (eLinguagem) {
			case AUTOMATO:
				return this.automatos;
			case EXPRESSAO:
				return this.expressoes;
			case GRAMATICA:
				return this.gramaticas;
			default:
				return null;
		}
	}
	
	public Automato get_automato(String nomeBuscado) {
		return (Automato) this.get_iLinguagem(this.automatos, nomeBuscado);
	}
	public Expressao get_expressao(String nomeBuscado) {
		return (Expressao) this.get_iLinguagem(this.expressoes, nomeBuscado);
	}
	public Gramatica get_gramatica(String nomeBuscado) {
		return (Gramatica) this.get_iLinguagem(this.gramaticas, nomeBuscado);
	}
	public ILinguagem get_iLinguagem(ELinguagem eLinguagem, String nomeBuscado) {
		switch (eLinguagem) {
			case AUTOMATO:
				return this.get_automato(nomeBuscado);
			case EXPRESSAO:
				return this.get_expressao(nomeBuscado);
			case GRAMATICA:
				return this.get_gramatica(nomeBuscado);
			default:
				return null;
		}
	}
	private ILinguagem get_iLinguagem(ArrayList<ILinguagem> iLinguagens, String nomeBuscado) {
		for (int c = 0; c < iLinguagens.size(); c++) {
			ILinguagem iLinguagem;
			iLinguagem = iLinguagens.get(c);
			
			String nomeILinguagem;
			nomeILinguagem = iLinguagem.get_nome();
			
			if (nomeILinguagem.equals(nomeBuscado)) {
				return iLinguagem;
			}
		}
		
		return null;
	}
	
	public Automato get_minimizacao(Automato automatoOriginal) {
		return this.minimizar.get_minimizacao(automatoOriginal);
	}
	public Automato get_determinizacao(Automato automatoOriginal) {
		return this.determinizar.get_determinizacao(automatoOriginal);
	}
	public ArrayList<Automato> get_diferenca(Automato automatoOriginal1, Automato automatoOriginal2) {
		return this.diferenca.get_diferenca(automatoOriginal1, automatoOriginal2);
	}
	public ArrayList<Automato> get_uniao(Automato automatoOriginal1, Automato automatoOriginal2) {
		return this.unir.get_uniao(automatoOriginal1, automatoOriginal2);
	}
	public ArrayList<Automato> get_concatenacao(Automato automatoOriginal1, Automato automatoOriginal2) {
		return this.concatenar.get_concatenacao(automatoOriginal1, automatoOriginal2);
	}
	public ArrayList<Automato> get_intersecao(Automato automatoOriginal1, Automato automatoOriginal2) {
		return this.intersectar.get_interseccao(automatoOriginal1, automatoOriginal2);
	}
}

package linguagem;
public class AlfabetoPortugues {
	private char[] alfabeto;
	private int cont;
	
	public AlfabetoPortugues() {
		this.cont = -1;
		this.gerar_alfabeto();
	}
	
	public char get_next_simbolo() {
		this.cont++;
		return this.alfabeto[this.cont];
	}
	
	private void gerar_alfabeto() {
		char[] alfabeto = {
				'S', 'A', 'B', 'C', 'D',
				'E', 'F', 'G', 'H', 'I',
				'J', 'K', 'L', 'M', 'N',
				'O', 'P', 'Q', 'R', 'T',
				'U', 'V', 'W', 'Y', 'X', 'Z'
		};
		
		this.alfabeto = alfabeto;
	}
}

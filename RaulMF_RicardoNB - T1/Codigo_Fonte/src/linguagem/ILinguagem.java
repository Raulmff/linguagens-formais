package linguagem;

import java.util.ArrayList;

public interface ILinguagem {
	public void set_nome(String nome);
	public String get_nome();
	public boolean get_compativeis(ILinguagem i);
	public ArrayList<Character> get_alfabeto();
}

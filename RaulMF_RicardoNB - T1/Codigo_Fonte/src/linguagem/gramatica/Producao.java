package linguagem.gramatica;

public class Producao {
	
	private char terminal;
	private NaoTerminal naoTerminal;

	public Producao(char s) {
		this.terminal = s;
		this.naoTerminal = null;
	}
	
	public void set_naoTerminal(NaoTerminal nt) {
		this.naoTerminal = nt;
	}
	
	public char get_terminal() {
		return this.terminal;
	}
	
	public NaoTerminal get_naoTerminal() {
		return this.naoTerminal;
	}
	
	public String toString() {
		String retorno = "Simbolo: "+this.terminal+" | NaoTerminal: ";
		
		if ( this.naoTerminal != null ) {
			retorno += this.naoTerminal.get_simbolo();
		}
		
		return retorno;
	}
}

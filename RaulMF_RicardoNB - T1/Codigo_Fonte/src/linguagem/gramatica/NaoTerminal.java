package linguagem.gramatica;
import java.util.ArrayList;

public class NaoTerminal {
	//private Map<Character, Producao> producoes;
	private ArrayList<Producao> producoes;
	private char simbolo;
	
	public NaoTerminal(char simbolo) {
		//this.producoes = new HashMap<Character, Producao>();
		this.producoes = new ArrayList<Producao>();
		this.simbolo = simbolo;
	}
	
	public ArrayList<Producao> get_producoes() {
		return this.producoes;
	}
	
	public void add_producao(Producao producao) {
		//this.producoes.put(producao.get_simbolo(), producao);
		this.producoes.add(producao);
	}
	
	public void print() {
		System.out.println(simbolo);
		for (int c = 0; c < this.producoes.size(); c++) {
			System.out.println(this.producoes.get(c));
		}
	}
	
	public String get_simbolo() {
		return this.simbolo+"";
	}
}

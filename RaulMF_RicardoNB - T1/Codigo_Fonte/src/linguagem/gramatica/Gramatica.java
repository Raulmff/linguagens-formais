package linguagem.gramatica;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import linguagem.AlfabetoPortugues;
import linguagem.EOperacao;
import linguagem.ILinguagem;
import linguagem.ILinguagemGerador;
import linguagem.automato.Automato;
import linguagem.automato.Estado;
import linguagem.automato.Transicao;

public class Gramatica implements ILinguagemGerador {
	private String nome;
	private NaoTerminal inicial;
	private ArrayList<Character> alfabeto;
	private ArrayList<NaoTerminal> naoTerminais;
	
	private String producoes;
	
	private static int novoNome = 0;
	
	public static String get_novo_nome() {
		String nome;
		nome = "GR"+Gramatica.novoNome;
		
		Gramatica.novoNome++;
		
		return nome;
	}
	
	/* Metodos Construtor */
	public Gramatica(String nome) {
		this.producoes = "";
		this.nome = nome;
		this.naoTerminais = new ArrayList<NaoTerminal>();
		this.inicial = null;
		this.alfabeto = new ArrayList<Character>();
	}
	public Gramatica(String nome, Automato automato) {
		this(nome);
		this.gerar_gramatica(automato);
	}
	
	@Override
	public boolean get_compativeis(ILinguagem linguagem) {
		ArrayList<Character> alfabetoComparar;
		alfabetoComparar = linguagem.get_alfabeto();
		
		// Se um alfabeto possuir mais simbolos que o outro alfabeto
		if (this.alfabeto.size() != alfabetoComparar.size()) {
			return false;
		}
		
		for (int c = 0; c < this.alfabeto.size(); c++) {
			char simboloAlfabeto;
			simboloAlfabeto = this.alfabeto.get(c);
			
			// Se algum simbolo de um alfabeto nao existir no outro alfabeto
			if (!alfabetoComparar.contains(simboloAlfabeto)) {
				return false;
			}
		}
		
		return true;
	}
	
	/* Metodos Add */
	public void add_alfabeto(char s) {
		if(!this.alfabeto.contains(s) & s!='&') {
			System.out.println("AQUI: "+s);
			this.alfabeto.add(s);
		}
	}
	public void add_naoTerminal(NaoTerminal nt) {
		this.naoTerminais.add(nt);
	}
	
	/* Metodos Setter */
	@Override
	public void set_nome(String nome) {
		this.nome = nome;
	}
	@Override
	public void set_producoes(String producoes) {
		producoes = producoes.replaceAll(" ", "");
		producoes = producoes.replaceAll("\n", "");
		
		this.producoes = producoes;
		this.obter_producoes(producoes);
	}
	private void obter_producoes(String producoes) {
		int i;
		i = producoes.indexOf(",");
		
		String str1;
		
		if (i == 0) {
			// INVALIDA
		} else if (i > 0) {
			str1 = producoes.substring(0, i);
			this.gerar_naoTerminais(str1);
			this.obter_producoes(producoes.substring(i+1, producoes.length()));
		} else {
			this.gerar_naoTerminais(producoes);
		}
	}
	public void gerar_naoTerminais(String producoes) {
		// SE: producoes vazia
		if (producoes == "") {
			// INVALIDA
			return;
		}
		
		String simbolo;
		simbolo = producoes.substring(0,1);
		
		NaoTerminal naoTerminal;
		naoTerminal = new NaoTerminal(simbolo.charAt(0));
		
		// SE: simbolo nao terminal for minusculo ou maior ou menor que 1
		if ( simbolo.toLowerCase().equals(simbolo) | producoes.indexOf("->") != 1 ) {
			// INVALIDA
			return;
		}
		
		// SE: for o primeiro naoTerminal da gramatica
		if (this.inicial == null) {
			this.inicial = naoTerminal;
		}
		
		this.naoTerminais.add(naoTerminal);
		
		producoes = producoes.substring(3, producoes.length());
		
		int i;
		do {
			// Seta producoes
			i = producoes.indexOf("|");
			
			String p;
			p = producoes;
			
			if (i > 0) {
				p = producoes.substring(0,i);
			}
			
			int size;
			size = p.length();
			
			if (size == 1) {
				this.add_alfabeto(p.charAt(0));
				naoTerminal.add_producao( new Producao(p.charAt(0)) );
			} else if (size == 2) {
				String p1, p2;
				p1 = p.substring(0,1);
				p2 = p.substring(1,2);
				
				Producao producao;
				producao = new Producao(p1.charAt(0));
				this.add_alfabeto(p.charAt(0));
				producao.set_naoTerminal( new NaoTerminal(p2.charAt(0)) );
				naoTerminal.add_producao( producao );
			} else {
				// INVALIDA
				return;
			}
			
			producoes = producoes.substring(i+1, producoes.length());
		} while( i != -1 );
	}
	
	public void gerar_gramatica(Automato automato) {
		automato = new Automato(automato);
		
		ArrayList<Estado> estados, estadosTransicao;
		estados = automato.get_estados();
		estadosTransicao = new ArrayList<Estado>();
		
		Estado estadoInicial;
		estadoInicial = automato.get_inicial();
		
		AlfabetoPortugues alfabeto;
		alfabeto = new AlfabetoPortugues();
		
		estadoInicial.set_simbolo(alfabeto.get_next_simbolo()+"");
		estadosTransicao.add(estadoInicial);
		
		for (int c = 0; c < estados.size(); c++) {
			Estado estado;
			estado = estados.get(c);
			
			if (estadoInicial == estado) {
				continue;
			}
			
			if (estado.get_transicoes().size() > 0) {
				estadosTransicao.add(estado);
				estado.set_simbolo(alfabeto.get_next_simbolo()+"");
			}
		}
		
		for (int c = 0; c < estadosTransicao.size(); c++) {
			Estado estado;
			estado = estadosTransicao.get(c);
			
			this.producoes += estado.get_simbolo()+"->";
			
			ArrayList<Transicao> transicoes;
			transicoes = estado.get_transicoes();
			
			for (int i = 0; i < transicoes.size(); i++) {
				Transicao transicao;
				transicao = transicoes.get(i);
				
				Estado destino;
				destino = transicao.get_destino();
				
				char entrada;
				entrada = transicao.get_entrada();
				
				if (destino.get_isFinal()) {
					this.producoes += entrada;
					
					if (estadosTransicao.contains(destino)) {
						this.producoes += "|" + entrada + destino.get_simbolo();
					}
				} else {
					this.producoes += entrada + destino.get_simbolo();
				}
				
				if (transicoes.size()-1 != i) {
					this.producoes += "|";
				}
			}
			
			if (estadosTransicao.size()-1 != c) {
				this.producoes += ",";
			}
		}
		
		System.out.println(this.producoes);
		
		this.set_producoes(this.producoes);
	}
	
	/* Metodos Getter */
	@Override
	public String get_nome() {
		return this.nome;
	}
	public Automato gera_automato() {
		ArrayList<Estado> estadosAutomato = new ArrayList<Estado>();
		
		// CRIA: estado inicial do automato
		Estado inicial;
		inicial = new Estado(this.naoTerminais.get(0).get_simbolo());
		inicial.set_isInicial(true);
		estadosAutomato.add(inicial);
		
		// VERIFICA: se o estado inicial eh estado final (possui eplison)
		NaoTerminal nt = this.naoTerminais.get(0);
		
		for(int i = 0; i < nt.get_producoes().size(); i++) {
			Producao p = nt.get_producoes().get(i);
			if(p.get_terminal() == '&') {
				inicial.set_isFinal(true);
			}
		}
		
		Estado estado;
		// CRIA: os estados (sem inicial e qF)
		for(int i = 1; i < this.naoTerminais.size(); i++) {
			estado = new Estado(this.naoTerminais.get(i).get_simbolo());
			estadosAutomato.add(estado);
		}
		
		// CRIA: estado qF
		estado = new Estado("qf");
		estado.set_isFinal(true);
		estadosAutomato.add(estado);
		
		// PERCORRE: todas os nao terminais para obter producoes
		for(int i = 0; i < this.naoTerminais.size(); i++) {
			ArrayList<Producao> producoes;
			producoes = this.naoTerminais.get(i).get_producoes();
			
			// PERCORRE: todas as producoes para gerar transicoes
			for (int j = 0; j < producoes.size(); j++) {
				NaoTerminal nT = this.naoTerminais.get(i);
				Producao producao = producoes.get(j);
				Transicao transicao = new Transicao();
				Estado inicio = busca_estado(estadosAutomato, nT.get_simbolo());
				transicao.set_inicio(inicio);
				Estado fim;
				
				if (producao.get_naoTerminal() == null) {
					fim = busca_estado(estadosAutomato, "qf");
				} else {
					fim = busca_estado(estadosAutomato, producao.get_naoTerminal().get_simbolo());
				}
				
				if (fim == null) {
					continue;
				}
				
				transicao.set_destino(fim);
				transicao.set_entrada(producao.get_terminal());
				inicio.add_transicao(transicao);
				
				this.add_alfabeto(producao.get_terminal());
			}
		}
		
		String nomeAutomato;
		nomeAutomato = Automato.get_novo_nome();
		
		Automato automato = new Automato(nomeAutomato);
		automato.set_estados(estadosAutomato);
		automato.set_inicial(inicial);
		automato.set_alfabeto(this.alfabeto);
		automato.set_gerador(this, null, EOperacao.GR_PARA_AF);
		
		return automato;
	}
	
	@Override
	public ArrayList<Character> get_alfabeto() {
		return this.alfabeto;
	}
	
	public Estado busca_estado(ArrayList<Estado> estados, String nome) {
		for (int c = 0; c < estados.size(); c++) {
			Estado estado = estados.get(c);
			
			// SE: possui o simbolo do estado buscado
			if (estado.get_simbolo().equals(nome)) {
				return estado;
			}
		}
		
		// SE: nao possui o simbolo do estado buscado
		return null;
	}
	
	public static boolean valida_gramatica(String producoes, boolean primeiro) {
		if (producoes == null) {
			return false;
		}
		if (producoes.equals("")) {
			return false;
		}
		
		producoes = producoes.replaceAll(" ", "");
		producoes = producoes.replaceAll("\n", "");
		
		if (primeiro) {
			int indiceEpsilon;
			indiceEpsilon = producoes.indexOf("&");
			
			if (indiceEpsilon > 0) {
				String producaoP;
				producaoP = producoes.substring(0, 1);
				
				String prod;
				prod = producoes.substring(1);
				
				int indicePrimeiro;
				indicePrimeiro = prod.indexOf(producaoP);
				
				if (indicePrimeiro > 0) {
					return false;
				}
			}
			
			// SE DEPOIS DO PRIMEIRO CARACTER EXISTIR OUTRO IGUAL AO PRIMEIRO, ENTAO VERIFICA EXISTENCIA DO EPLISON
			// SE EXISTIR O ELISON ENTAO DA ERRO
		}
		
		System.out.println(producoes);
		
		int indice;
		indice = producoes.indexOf(",");
		
		if (indice < 0) {
			return Gramatica.valida_nTerminal(producoes, primeiro);
		}
		
		String prod1, prod2;
		prod1 = producoes.substring(0, indice);
		prod2 = producoes.substring(indice+1);
		
		boolean valido;
		valido = Gramatica.valida_nTerminal(prod1, primeiro);
		
		if (!valido) {
			return false;
		}
		
		return Gramatica.valida_gramatica(prod2, false);
	}
	
	private static boolean valida_nTerminal(String nTerminal, boolean primeiro) {
		boolean b;
		Pattern regex;
		Matcher m;
		
		if (primeiro) {
			regex = Pattern.compile("\\p{Upper}->((\\p{Lower}|\\p{Lower}\\p{Upper}|&)[|])*(\\p{Lower}|\\p{Lower}\\p{Upper}|&),?");
			m = regex.matcher(nTerminal);
			b = m.matches();
		} else {
			regex = Pattern.compile("\\p{Upper}->((\\p{Lower}|\\p{Lower}\\p{Upper})[|])*(\\p{Lower}|\\p{Lower}\\p{Upper}),?");
			m = regex.matcher(nTerminal);
			b = m.matches();
		}
		
		if(!b) {
			
			if (primeiro) {
				regex = Pattern.compile("\\p{Upper}->((\\d|\\d\\p{Upper}|&)[|])*(\\d|\\d\\p{Upper}|&),?");
				m = regex.matcher(nTerminal);
				b = m.matches();
			} else {
				regex = Pattern.compile("\\p{Upper}->((\\d|\\d\\p{Upper})[|])*(\\d|\\d\\p{Upper}),?");
				m = regex.matcher(nTerminal);
				b = m.matches();
			}
		}
		
		return b;
	}
	
	/* Metodos para deletar depois */
	public void print() {
		for (int c = 0; c < this.alfabeto.size(); c++) {
			System.out.println(this.alfabeto.get(c));
		}
	}
	
	@Override
	public String get_producoes() {
		return this.producoes;
	}
}

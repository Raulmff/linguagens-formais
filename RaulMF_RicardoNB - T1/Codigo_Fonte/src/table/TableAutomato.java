package table;

import java.util.ArrayList;

import javax.swing.JScrollPane;

import linguagem.automato.Automato;
import linguagem.automato.Estado;
import linguagem.automato.Transicao;
import view.component.ViewTable;

public class TableAutomato {
	private ViewTable viewTable;
	private String simboloEstadoErro;
	
	public TableAutomato(int x, int y, int width, int height) {
		this.viewTable = new ViewTable(x, y, width, height);
		this.simboloEstadoErro = "-";
	}
	
	public void set_visible(boolean visible) {
		this.viewTable.set_visible(visible);
	}
	
	public JScrollPane get_jScrollPane() {
		return this.viewTable.get_JScrollPane();
	}
	
	public void montar_table(Automato automato) {
		ArrayList<Estado> estados;
		estados = automato.get_estados();
		
		ArrayList<Character> alfabeto;
		alfabeto = automato.get_alfabeto();
		
		TableRow row_head;
		row_head = this.montar_row_head(alfabeto);
		
		TableRow row_inicial;
		row_inicial = this.montar_row(automato.get_inicial(), alfabeto);
		
		Table table;
		table = new Table(row_head);
		table.add_row(row_inicial);
		
		for (int i = 0; i < estados.size(); i++) {
			Estado estado;
			estado = estados.get(i);
			
			if (estado.get_isInicial()) {
				continue;
			}
			
			TableRow row;
			row = this.montar_row(estado, alfabeto);
			
			table.add_row(row);
		}
		
		this.viewTable.set_table(table);
	}
	
	private TableRow montar_row_head(ArrayList<Character> alfabeto) {
		TableRow row_head;
		row_head = new TableRow();
		row_head.add_column("");
		row_head.add_column("𝛅");
		
		for (int j = 0; j < alfabeto.size(); j++) {
			row_head.add_column(alfabeto.get(j)+"");
		}
		
		return row_head;
	}
	private TableRow montar_row(Estado estado, ArrayList<Character> alfabeto) {
		String inicialFinal;
		inicialFinal = "";
		
		if (estado.get_isFinal()) {
			inicialFinal += "*";
		}
		if (estado.get_isInicial()) {
			inicialFinal += "->";
		}
		
		TableRow row;
		row = new TableRow();
		row.add_column(inicialFinal);
		row.add_column(estado.get_simbolo());
		
		for (int i = 0; i < alfabeto.size(); i++) {
			char simboloAlfabeto;
			simboloAlfabeto = alfabeto.get(i);
			
			String column;
			column = "";
			
			for (int j = 0; j < estado.get_transicoes().size(); j++) {
				Transicao transicao;
				transicao = estado.get_transicoes().get(j);
				
				if (transicao.get_entrada() == simboloAlfabeto) {
					Estado destino;
					destino = transicao.get_destino();
					
					if (!column.equals("")) {
						column += ",";
					}
					
					column += destino.get_simbolo();
				}
			}
			
			if (column.equals("")) {
				column += this.simboloEstadoErro;
			}
			
			row.add_column(column);
		}
		
		return row;
	}
}

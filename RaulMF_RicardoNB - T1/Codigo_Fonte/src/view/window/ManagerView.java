package view.window;

import javax.swing.JPanel;

import linguagem.ELinguagem;
import linguagem.ManagerILinguagem;
import linguagem.automato.Automato;
import linguagem.expressao.Expressao;
import linguagem.gramatica.Gramatica;
import view.EView;
import view.View;
import view.ViewAutomato;
import view.ViewCriar;
import view.ViewGerarAutomato;
import view.ViewGerarGramatica;
import view.ViewInicio;
import view.ViewListarGerador;

public class ManagerView {
	private Window window;
	private JPanel viewMain;
	
	private ManagerILinguagem managerILinguagem;
	
	private View criarGr, gerarGr, listarGr;
	private View criarEr, listarEr;
	private View gerarAf, listarAf;
	private View inicio;
	
	public ManagerView(Window window) {
		this.window = window;
		
		this.managerILinguagem = new ManagerILinguagem();
		
		this.criarGr = new ViewCriar(this, ELinguagem.GRAMATICA);
		this.criarEr = new ViewCriar(this, ELinguagem.EXPRESSAO);
		
		this.listarGr = new ViewListarGerador(this, ELinguagem.GRAMATICA);
		this.listarEr = new ViewListarGerador(this, ELinguagem.EXPRESSAO);
		
		this.inicio = new ViewInicio(this);
		this.gerarGr = new ViewGerarGramatica(this);
		
		this.listarAf = new ViewAutomato(this);
		this.gerarAf = new ViewGerarAutomato(this);
		
		this.load_viewMain();
		this.window.getContentPane().add(this.viewMain);
	}
	
	public void add_gramatica(Gramatica gramatica) {
		this.managerILinguagem.add_gramatica(gramatica);
	}
	public void add_expressao(Expressao expressao) {
		this.managerILinguagem.add_expressao(expressao);
	}
	public void add_automato(Automato automato) {
		this.managerILinguagem.add_automato(automato);
	}
	
	public void set_visible(EView eView) {
		String titulo;
		View view;
		view = this.inicio;
		
		switch (eView) {
			// GRAMATICA
			case GR_CRIAR:
				view = this.criarGr;
				titulo = "Gramatica - Criar";
				break;
			case GR_LISTAR:
				view = this.listarGr;
				titulo = "Gramatica - Listar";
				break;
			case GR_GERAR:
				view = this.gerarGr;
				titulo = "Gramatica - Gerar";
				break;
				
			// EXPRESSAO
			case ER_CRIAR:
				view = this.criarEr;
				titulo = "Expressao - Criar";
				break;
			case ER_LISTAR:
				view = this.listarEr;
				titulo = "Expressao - Listar";
				break;
				
			// AUTOMATO
			case AF_LISTAR:
				view = this.listarAf;
				titulo = "Automato - Listar";
				break;
			case AF_GERAR:
				view = this.gerarAf;
				titulo = "Automato - Gerar";
				break;
			case AF_SENTENCA_ENUMERAR:
				//view = this.;
				titulo = "Automato - Enumerar Sentencas";
				break;
			case AF_SENTENCA_RECONHECER:
				//view = this.reconhecerSentenca;
				titulo = "Automato - Reconhecer Sentenca";
				break;
			
			// INICIO
			default:
				titulo = "Inicio";
				break;
		}
		
		view.visible(this.viewMain);
		this.window.setTitle(titulo);
		this.window.repaint();
	}
	
	private void load_viewMain() {
		this.viewMain = new JPanel();
		this.viewMain.setBounds(10, 40, 790, 560);
		this.viewMain.setLayout(null);
	}

	public ManagerILinguagem get_managerILinguagem() {
		return this.managerILinguagem;
	}
}

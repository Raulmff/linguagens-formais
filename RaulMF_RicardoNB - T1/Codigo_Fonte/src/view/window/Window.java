package view.window;
import java.awt.FlowLayout;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import linguagem.automato.Automato;
import linguagem.automato.MinimizarAutomato;
import linguagem.gramatica.Gramatica;
import view.EView;
import view.component.MenuBar;
import view.event.EventMenuBar;

public class Window extends JFrame {
	private JPanel contentPane;
	private ManagerView manager;
	private MenuBar menuBar;
	private EventMenuBar event;
	
	/* Metodos Construtores */
	public Window() {
		this.load_contentPane();
		
		this.manager = new ManagerView(this);
		this.manager.set_visible(EView.INICIO);
		
		this.event = new EventMenuBar(this.manager);
		this.load_menuBar();
		
		this.configurar();
	}
	
	// REMOVER ESSE METODO DEPOIS, EH SO PRA FACILITAR OS TESTES DE AUTOMATO
	public void teste_de_automato_HRHAHHSRAUH_BRBR() {
		String nomeGramatica;
		nomeGramatica = Gramatica.get_novo_nome();
		
		Gramatica gramatica;
		gramatica = new Gramatica(nomeGramatica);
		gramatica.set_producoes("S->aS|a|bB,B->bB|b");
		
		Automato automato;
		automato = gramatica.gera_automato();
		
		this.manager.add_automato(automato);
		
		MinimizarAutomato minimizar;
		minimizar = new MinimizarAutomato();
		automato = minimizar.get_minimizacao(automato);
		
		this.manager.add_automato(automato);
	}
	
	/* Metodos Load */
	private void load_contentPane() {
		this.contentPane = new JPanel();
		this.contentPane.setLayout(new FlowLayout());
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		this.setContentPane(this.contentPane);
	}
	private void load_menuBar() {
		this.menuBar = new MenuBar(this.event);
		
		this.contentPane.add(this.menuBar.get_jmenu_bar());
	}
	private void configurar() {
		this.setBounds(10, 10, 820, 620);
		this.setLayout(null);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.setVisible(true);
	}
	
	public ManagerView get_managerView() {
		return this.manager;
	}
	
	public static void insert_message(String message, String title) {
		JOptionPane.showMessageDialog(null, message, title, JOptionPane.INFORMATION_MESSAGE);
	}
}

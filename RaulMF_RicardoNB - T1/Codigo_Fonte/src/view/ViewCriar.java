package view;

import linguagem.ELinguagem;
import linguagem.expressao.Expressao;
import linguagem.gramatica.Gramatica;
import view.component.EMenuDialog;
import view.component.TextArea;
import view.window.ManagerView;
import view.window.Window;

public class ViewCriar extends ViewNotListener {
	/* Construtor */
	public ViewCriar(ManagerView manager, ELinguagem eLinguagem) {
		super(manager, eLinguagem);
		super.menuDialog.set_menu_dialog(EMenuDialog.ADICIONAR_CANCELAR);
		
		this.inputProducao = new TextArea(90, 50, 680, 400);
		super.view_content_add_component(super.inputProducao.get_JScrollPane());
		
		this.update_input_nome();
	}
	
	@Override
	public void adicionar() {
		super.adicionar();
		
		String nome, producoes;
		nome = this.get_nome();
		producoes = this.get_producao();
		
		String message, title;
		if (super.eLinguagem == ELinguagem.GRAMATICA) {
			
			message = "Gramatica";
			title = "Nova Gramatica Regular";
			
			if(Gramatica.valida_gramatica(producoes,true)) {
				Gramatica gramatica;
				gramatica = new Gramatica(nome);
				gramatica.set_producoes(producoes);
				this.manager.add_gramatica(gramatica);
			} else {
				Window.insert_message(message+" invalida!", title);
				return;
			}
		} else {
			message = "Expressao";
			title = "Nova Expressao Regular";
			
			if(Expressao.valida_expressao(producoes)) {
				Expressao expressao;
				expressao = new Expressao(nome);
				expressao.set_producoes(producoes);
				this.manager.add_expressao(expressao);
			} else {
				Window.insert_message(message+" invalida!", title);
				return;
			}
		}
		
		Window.insert_message(message+" adicionada com sucesso!", title);
		
		this.update_input_nome();
	}
	
	private void update_input_nome() {
		String nome;
		
		if (super.eLinguagem == ELinguagem.GRAMATICA) {
			nome = Gramatica.get_novo_nome();
		} else {
			nome = Expressao.get_novo_nome();
		}
		
		this.get_input_nome().setText(nome);
	}
	
	/* Metodos Implements - Nao utilizados */
	@Override
	public void salvar() {}
}

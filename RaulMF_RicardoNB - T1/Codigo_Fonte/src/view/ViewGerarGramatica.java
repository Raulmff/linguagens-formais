package view;

import java.util.ArrayList;

import javax.swing.JPanel;

import linguagem.ELinguagem;
import linguagem.EOperacao;
import linguagem.ILinguagem;
import linguagem.automato.Automato;
import linguagem.gramatica.Gramatica;
import view.component.ComboBox;
import view.component.Component;
import view.component.EMenuDialog;
import view.component.GerarILinguagem;
import view.event.EventGerarGramatica;
import view.window.ManagerView;
import view.window.Window;

public class ViewGerarGramatica extends View {
	private EventGerarGramatica event;
	private ComboBox boxP, boxS;
	
	private GerarILinguagem iLinguagemP, iLinguagemS;
	
	public ViewGerarGramatica(ManagerView manager) {
		super(manager);
		this.menuDialog.set_menu_dialog(EMenuDialog.UNIR_CONCATENAR_CANCELAR);
		
		this.event = new EventGerarGramatica(this);
		
		this.load_iLinguagem();
		this.load_components();
	}
	
	private void load_iLinguagem() {
		this.iLinguagemP = new GerarILinguagem(0, 100);
		this.iLinguagemS = new GerarILinguagem(430, 100);
		
		this.view_content_add_component(this.iLinguagemP.get_gerador_JScrollPane());
		this.view_content_add_component(this.iLinguagemS.get_gerador_JScrollPane());
	}
	private void load_components() {
		this.boxP = new ComboBox(100, 0, "OPCAO_P", this.event);
		this.boxS = new ComboBox(530, 0, "OPCAO_S", this.event);
		
		this.boxP.load_default();
		this.boxS.load_default();
		
		this.view_content_add_component(this.boxP.get_comboBox());
		this.view_content_add_component(this.boxS.get_comboBox());
		
		this.view_content_add_component( Component.new_label(0, 0, 100, 40, "Opcao 1:") );
		this.view_content_add_component( Component.new_label(430, 0, 100, 40, "Opcao 2:") );
	}
	
	public void update_opcao_show_item_p() {
		this.update_opcao_show_item(this.boxP, this.iLinguagemP);
	}
	public void update_opcao_show_item_s() {
		this.update_opcao_show_item(this.boxS, this.iLinguagemS);
	}
	
	private void load_opcoes() {
		ArrayList<ILinguagem> itens;
		itens = this.managerILinguagem.get_array_iLinguagem(ELinguagem.GRAMATICA);
		
		this.boxP.set_itens_iLinguagem(itens);
		this.boxS.set_itens_iLinguagem(itens);
	}
	private Gramatica get_gramatica(ComboBox boxOpcao) {
		String nomeBuscada;
		nomeBuscada = boxOpcao.get_selected();
		
		ArrayList<ILinguagem> array;
		array = this.managerILinguagem.get_array_iLinguagem(ELinguagem.GRAMATICA);
		
		for (int c = 0; c < array.size(); c++) {
			Gramatica gramatica;
			gramatica = (Gramatica)array.get(c);
			
			String nomeGramatica;
			nomeGramatica = gramatica.get_nome();
			
			if (nomeGramatica.equals(nomeBuscada)) {
				return gramatica;
			}
		}
		
		return null;
	}
	private void update_opcao_show_item(ComboBox boxOpcao, GerarILinguagem iLinguagem) {
		if (boxOpcao.get_indice_selected() == 0) {
			iLinguagem.hide_gerador();
			return;
		}
		
		Gramatica gramatica;
		gramatica = this.get_gramatica(boxOpcao);
		
		if (gramatica != null) {
			iLinguagem.show_gerador(gramatica);
		}
	}
	
	private boolean get_valido() {
		if (this.boxP.get_indice_selected() == 0 || this.boxS.get_indice_selected() == 0) {
			Window.insert_message("Todos os campos devem ser preenchidos.", "Erro!");
			return false;
		}
		return true;
	}
	
	@Override
	public void visible(JPanel p) {
		super.visible(p);
		this.load_opcoes();
	}
	
	@Override
	public void unir() {
		if( !this.get_valido() ) {
			return;
		}
		
		Gramatica gramaticaP, gramaticaS;
		gramaticaP = this.get_gramatica(this.boxP);
		gramaticaS = this.get_gramatica(this.boxS);
		
		if (!gramaticaS.get_compativeis(gramaticaP)) {
			System.out.println(":)");
			Window.insert_message("Alfabetos incompativeis", "Erro!");
			return;
		}
		System.out.println(":(");
		
		Automato automatoP, automatoS;
		automatoP = gramaticaP.gera_automato();
		automatoP.set_gerador(gramaticaP, null, EOperacao.GR_PARA_AF);
		
		automatoS = gramaticaS.gera_automato();
		automatoS.set_gerador(gramaticaS, null, EOperacao.GR_PARA_AF);
		
		ArrayList<Automato> retorno;
		retorno = new ArrayList<Automato>();
		retorno.add(automatoP);
		retorno.add(automatoS);
		
		automatoP = this.managerILinguagem.get_minimizacao(automatoP);
		automatoS = this.managerILinguagem.get_minimizacao(automatoS);
		
		ArrayList<Automato> automatosGerados;
		automatosGerados = this.managerILinguagem.get_uniao(automatoP, automatoS);
		automatosGerados.add(automatoP);
		automatosGerados.add(automatoS);
		
		retorno.addAll(automatosGerados);
		
		Automato automato;
		automato = automatosGerados.get( automatosGerados.size()-1 );
		
		for (int c = 0; c < retorno.size(); c++) {
			Automato gerado;
			gerado = retorno.get(c);
			
			this.managerILinguagem.add_automato(gerado);
		}
		
		Gramatica gramaticaGerada;
		gramaticaGerada = new Gramatica(Gramatica.get_novo_nome(), automato);
		
		this.managerILinguagem.add_gramatica(gramaticaGerada);
		
		String mensagem;
		mensagem = "Gramatica obtida pela uniao foi gerada com sucesso!\n";
		
		Window.insert_message(mensagem, "Sucesso!");
	}
	@Override
	public void concatenar() {
		if( !this.get_valido() ) {
			return;
		}
		
		ArrayList<Automato> retorno;
		retorno = new ArrayList<Automato>();
		
		Gramatica gramaticaP, gramaticaS;
		gramaticaP = this.get_gramatica(this.boxP);
		gramaticaS = this.get_gramatica(this.boxS);
		
		if (!gramaticaS.get_compativeis(gramaticaP)) {
			Window.insert_message("Alfabetos incompativeis", "Erro!");
			return;
		}
		
		Automato automatoP, automatoS;
		automatoP = gramaticaP.gera_automato();
		automatoS = gramaticaS.gera_automato();
		
		retorno.add(automatoP);
		retorno.add(automatoS);
		
		automatoP = this.managerILinguagem.get_minimizacao(automatoP);
		automatoS = this.managerILinguagem.get_minimizacao(automatoS);
		
		ArrayList<Automato> retornoConcatenacao;
		retornoConcatenacao = this.managerILinguagem.get_concatenacao(automatoP, automatoS);
		
		retorno.add(automatoP);
		retorno.add(automatoS);
		retorno.addAll(retornoConcatenacao);
		
		for (int c = 0; c < retorno.size(); c++) {
			Automato gerado;
			gerado = retorno.get(c);
			
			this.managerILinguagem.add_automato(gerado);
		}
		
		Automato automato;
		automato = retornoConcatenacao.get(retornoConcatenacao.size()-1 );
		
		Gramatica gramaticaGerada;
		gramaticaGerada = new Gramatica(Gramatica.get_novo_nome(), automato);
		
		this.managerILinguagem.add_gramatica(gramaticaGerada);
		
		String mensagem;
		mensagem = "Gramatica obtida pela concatenacao foi gerada com sucesso!\n";
		
		Window.insert_message(mensagem, "Sucesso!");
	}
}

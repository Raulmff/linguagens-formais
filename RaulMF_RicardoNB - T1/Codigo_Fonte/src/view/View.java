package view;

import java.awt.Component;

import javax.swing.JPanel;

import linguagem.ManagerILinguagem;
import view.component.IMenuDialog;
import view.component.MenuDialog;
import view.event.EventMenuDialog;
import view.window.ManagerView;

public abstract class View implements IMenuDialog {
	private JPanel viewMain;
	private EventMenuDialog event;
	
	protected JPanel viewContent;
	protected MenuDialog menuDialog;
	protected ManagerView manager;
	
	protected ManagerILinguagem managerILinguagem;
	
	private final int width = 790;
	private final int height = 560;
	
	/* Metodos Construtor */
	public View(ManagerView manager) {
		this.manager = manager;
		this.managerILinguagem = manager.get_managerILinguagem();
		
		this.viewMain = new JPanel();
		this.viewMain.setLayout(null);
		this.viewMain.setBounds(0, 0, this.width, this.height);
		
		this.viewContent = new JPanel();
		this.viewContent.setLayout(null);
		this.viewContent.setBounds(0, 0, this.width, this.height);
		
		this.event = new EventMenuDialog(this);
		this.menuDialog = new MenuDialog(this.event);
		
		this.view_main_add_component( this.menuDialog.get_JPanel() );
		this.view_main_add_component( this.viewContent );
	}
	
	protected void repaint() {
		this.viewMain.repaint();
		this.viewContent.repaint();
		this.menuDialog.get_JPanel().repaint();
	}
	
	/* Metodos Setter */
	public void visible(JPanel p) {
		p.removeAll();
		p.add(this.viewMain);
		
		Component[] components;
		components = this.viewMain.getComponents();
		
		for (int c = 0; c < components.length; c++) {
			Component component;
			component = components[c];
			component.setVisible(false);
			component.setVisible(true);
		}
	}
	protected void set_size_view_conteudo(int width, int height) {
		int x, y;
		x = this.width - width;
		y = this.height - height;
		
		this.viewContent.setBounds(x, y, width, height);
	}
	
	/* Metodos Getter */
	public JPanel get_JPanel() {
		return this.viewMain;
	}
	
	/* Metodos Add */
	protected void view_main_add_component(Component c) {
		this.viewMain.add(c);
	}
	protected void view_content_add_component(Component c) {
		this.viewContent.add(c);
	}
	
	/* Metodos Remover */
	protected void view_main_remove_component(Component c) {
		this.viewMain.remove(c);
	}
	
	/* Metodos Implements */
	@Override
	public void salvar() {
		System.out.println("View.salvar()");
	}
	@Override
	public void adicionar() {
		System.out.println("View.adicionar()");
	}
	@Override
	public void cancelar() {
		System.out.println("View.cancelar()");
		this.manager.set_visible(EView.INICIO);
	}
	@Override
	public void editar() {
		System.out.println("View.editar()");
	}
	@Override
	public void remover() {
		System.out.println("View.remover()");
	}
	
	@Override
	public void gerarAf() {
		System.out.println("View.gerarAf()");
	}
	@Override
	public void minimizarAf() {
		System.out.println("View.minimizarAf()");
	}
	@Override
	public void determinizarAf() {
		System.out.println("View.determinizarAf()");
	}
	
	@Override
	public void intersectar() {
		System.out.println("View.intersectar()");
	}
	@Override
	public void subtrair() {
		System.out.println("View.subtrair()");
	}
	
	@Override
	public void unir() {
		System.out.println("View.unir()");
	}
	@Override
	public void concatenar() {
		System.out.println("View.concatenar()");
	}
	
	@Override
	public void obter_fechamento() {
		System.out.println("View.obter_fechamento()");
	}
	@Override
	public void obter_reverso() {
		System.out.println("View.obter_reverso()");
	}
	@Override
	public void gerar_gr() {
		System.out.println("View.gerar_gr()");
	}
}

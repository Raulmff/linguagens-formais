package view;

import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.JTextField;

import linguagem.ELinguagem;
import linguagem.ILinguagem;
import view.component.Component;
import view.component.MenuAside;
import view.window.ManagerView;

public abstract class ViewListener extends View {
	private MenuAside menuAside;
	private JTextField inputNome;
	
	protected ELinguagem eLinguagem;
	
	public ViewListener(ManagerView manager, ELinguagem eLinguagem) {
		super(manager);
		
		this.eLinguagem = eLinguagem;
		
		this.menuAside = new MenuAside(this);
		
		this.inputNome = new JTextField();
		this.inputNome.setBounds(90, 0, 470, 20);
		this.inputNome.setEditable(false);
		
		this.view_content_add_component( Component.new_label(0, 2, 50, 20, "Nome:") );
		this.view_content_add_component(this.inputNome);
		
		if (eLinguagem == ELinguagem.AUTOMATO) {
			this.view_content_add_component( Component.new_label(0, 32, 100, 20, "Transicoes:") );
		} else {
			this.view_content_add_component( Component.new_label(0, 32, 100, 20, "Producoes:") );
		}
		
		this.set_size_view_conteudo(580, 560);
		this.view_main_add_component( this.menuAside.get_JPanel() );
	}
	
	public void mostrar(String nomeLinguagem) {
		super.viewContent.setVisible(true);
		super.menuDialog.set_visible(true);
		
		this.cancelar();
	}
	
	protected void set_input_nome(String nome) {
		this.inputNome.setText(nome);
	}
	
	protected JTextField get_input_nome() {
		return this.inputNome;
	}
	protected String get_nome() {
		return this.inputNome.getText();
	}
	
	protected void modo_visualizar() {
		this.enable_editar(false);
	}
	protected void modo_editar() {
		this.enable_editar(true);
	}
	
	@Override
	public void repaint() {
		super.repaint();
		this.menuAside.repaint();
	}
	
	@Override
	public void editar() {
		this.modo_editar();
		this.repaint();
	}
	
	@Override
	public void cancelar() {
		this.modo_visualizar();
		this.repaint();
	}
	
	@Override
	public void remover() {
		this.reload();
	}
	
	@Override
	public void visible(JPanel p) {
		super.visible(p);
		this.reload();
	}
	
	protected void reload() {
		this.reload_aside();
		
		this.viewContent.setVisible(false);
		this.menuDialog.set_visible(false);
		this.repaint();
	}
	
	protected void reload_aside() {
		super.view_main_remove_component( this.menuAside.get_JPanel() );
		
		ArrayList<ILinguagem> array;
		array = this.managerILinguagem.get_array_iLinguagem(this.eLinguagem);
		
		this.menuAside.set_menu(array);
		
		super.view_main_add_component(this.menuAside.get_JPanel());
	}
	
	protected void enable_editar(boolean enable) {}
}

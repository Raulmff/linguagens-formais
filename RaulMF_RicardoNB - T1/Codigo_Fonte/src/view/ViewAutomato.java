package view;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

import linguagem.ELinguagem;
import linguagem.ILinguagem;
import linguagem.automato.Automato;
import linguagem.automato.DeterminizarAutomato;
import linguagem.automato.MinimizarAutomato;
import linguagem.gramatica.Gramatica;
import table.TableAutomato;
import view.component.Component;
import view.component.EMenuDialog;
import view.event.EventListarAutomato;
import view.window.ManagerView;
import view.window.Window;

public class ViewAutomato extends ViewListener {
	private EventListarAutomato eventListar;
	private TableAutomato inputProd;
	private Automato visualizando;
	
	private JTextField inputSentenca, inputEnumeracao;
	private JTextField inputOperacao, inputGerador1, inputGerador2;
	
	private JLabel labelOperacao, labelGerador1, labelGerador2;
	
	/* Construtor */
	public ViewAutomato(ManagerView manager) {
		super(manager, ELinguagem.AUTOMATO);
		this.menuDialog.set_menu_dialog(EMenuDialog.DETERMINIZAR_MINIMIZAR_REVERSO_REMOVER);
		
		this.eventListar = new EventListarAutomato(this);
		this.inputSentenca = new JTextField();
		this.inputEnumeracao = new JTextField();
		
		this.inputOperacao = new JTextField();
		this.inputGerador1 = new JTextField();
		this.inputGerador2 = new JTextField();
		
		this.inputOperacao.setEditable(false);
		this.inputGerador1.setEditable(false);
		this.inputGerador2.setEditable(false);
		
		this.inputProd = new TableAutomato(0, 62, 560, 340);
		
		this.view_content_add_component( Component.new_label(0, 450, 90, 20, "Sentenca:") );
		this.view_content_add_component( Component.new_label(370, 450, 50, 20, "N:") );
		
		this.inputSentenca.setBounds(0, 470, 190, 25);
		this.inputEnumeracao.setBounds(370, 470, 40, 25);
		
		this.add_botao("Reconhecer", "RECONHECER_SENTENCA", 200, 470);
		this.add_botao("Enumerar", "ENUMERAR_SENTENCAS", 420, 470);
		
		this.labelOperacao = Component.new_label(0, 412, 90, 25, "Obtido por");
		this.labelGerador1 = Component.new_label(235, 412, 30, 25, "de");
		this.labelGerador2 = Component.new_label(410, 412, 20, 25, "e");
		
		this.inputOperacao.setBounds(100, 410, 120, 25);
		this.inputGerador1.setBounds(270, 410, 120, 25);
		this.inputGerador2.setBounds(440, 412, 120, 25);
		
		this.view_content_add_component( this.labelOperacao );
		this.view_content_add_component( this.labelGerador1 );
		this.view_content_add_component( this.labelGerador2 );
		this.view_content_add_component( this.inputOperacao );
		this.view_content_add_component( this.inputGerador1 );
		this.view_content_add_component( this.inputGerador2 );
		this.view_content_add_component(this.inputProd.get_jScrollPane());
		this.view_content_add_component(this.inputSentenca);
		this.view_content_add_component(this.inputEnumeracao);
	}
	
	private void add_botao(String label, String actionCommand, int x, int y) {
		JButton button;
		button = new JButton(label);
		button.setBounds(x, y, 140, 25);
		button.addActionListener(this.eventListar);
		button.setActionCommand(actionCommand);
		
		this.view_content_add_component(button);
	}
	
	@Override
	protected void reload() {
		super.reload();
	}
	
	public void reconhecer_sentenca() {
		String sentenca;
		sentenca = this.inputSentenca.getText();
		
		Automato automatoMinimo;
		automatoMinimo = this.managerILinguagem.get_minimizacao(this.visualizando);
		
		boolean reconheceu;
		reconheceu = automatoMinimo.get_sentenca_pertence(sentenca);
		
		String status;
		status = "nao reconhecida.";
		
		if (reconheceu) {
			status = "reconhecida";
		}
		
		Window.insert_message("Sentenca: "+status, "Reconhecer Sentenca: "+sentenca);
	}
	public void enumerar_sentenca() {
		String en;
		en = this.inputEnumeracao.getText();
		
		int nEnumerar;
		nEnumerar = Integer.parseInt(en);
		
		Automato automatoMinimo;
		automatoMinimo = this.managerILinguagem.get_minimizacao(this.visualizando);
		
		int retorno;
		retorno = automatoMinimo.enumera_tamanho_n(nEnumerar);
		
		Window.insert_message("Total de sentencas do automato: "+retorno, "Enumerar Sentencas tamanho "+nEnumerar);
	}
	
	private void set_producoes(Automato automato) {
		this.inputProd.montar_table(automato);
	}
	
	private void add_automato(Automato automato) {
		this.managerILinguagem.add_automato(automato);
		this.reload_aside();
		this.repaint();
		
		Window.insert_message("Automato gerado com sucesso!", "Gerar Automato");
	}
	
	@Override
	public void mostrar(String nomeAutomato) {
		this.visualizando = this.managerILinguagem.get_automato(nomeAutomato);
		
		this.inputOperacao.setVisible(false);
		this.inputGerador1.setVisible(false);
		this.inputGerador2.setVisible(false);
		
		this.labelOperacao.setVisible(false);
		this.labelGerador1.setVisible(false);
		this.labelGerador2.setVisible(false);
		
		this.set_input_nome(nomeAutomato);
		this.set_producoes(this.visualizando);
		
		String eOperacao;
		eOperacao = this.visualizando.get_operacao_gerador();
		
		System.out.println(eOperacao);
		
		if (eOperacao != null) {
			this.inputOperacao.setText(eOperacao);
			this.inputOperacao.setVisible(true);
			this.labelOperacao.setVisible(true);
			
			ILinguagem gerador1, gerador2;
			gerador1 = this.visualizando.get_automato_pai1();
			gerador2 = this.visualizando.get_automato_pai2();
			
			this.inputGerador1.setText(gerador1.get_nome());
			this.inputGerador1.setVisible(true);
			this.labelGerador1.setVisible(true);
			
			if (gerador2 != null) {
				this.inputGerador2.setText(gerador2.get_nome());
				this.inputGerador2.setVisible(true);
				this.labelGerador2.setVisible(true);
			}
		}
		
		super.mostrar(nomeAutomato);
	}
	
	@Override
	public void determinizarAf() {
		super.determinizarAf();
		
		DeterminizarAutomato determinizar;
		determinizar = new DeterminizarAutomato();
		
		Automato automato;
		automato = determinizar.get_determinizacao(this.visualizando);
		
		this.add_automato(automato);
	}
	@Override
	public void minimizarAf() {
		super.minimizarAf();
		
		DeterminizarAutomato determinizar;
		determinizar = new DeterminizarAutomato();
		
		Automato automato;
		automato = determinizar.get_determinizacao(this.visualizando);
		
		this.managerILinguagem.add_automato(automato);
		
		MinimizarAutomato minimizar;
		minimizar = new MinimizarAutomato();
		
		automato = minimizar.get_minimizacao(automato);
		this.add_automato(automato);
	}
	
	@Override
	public void obter_reverso() {
		super.obter_reverso();
		
		Automato automatoReverso;
		automatoReverso = this.visualizando.get_reverso();
		
		this.add_automato(automatoReverso);
	}
	
	@Override
	public void remover() {
		this.managerILinguagem.remover_automato(this.visualizando);
		super.remover();
	}
	
	@Override
	public void gerar_gr() {
		super.gerar_gr();
		
		Gramatica gramatica;
		gramatica = new Gramatica(Gramatica.get_novo_nome(), this.visualizando);
		
		this.managerILinguagem.add_gramatica(gramatica);
		this.reload_aside();
		this.repaint();
		
		Window.insert_message("Gramatica gerada com sucesso!", "Gerar Gramatica");
	}
}

package view;

import javax.swing.JTextArea;
import javax.swing.JTextField;

import linguagem.ELinguagem;
import view.component.Component;
import view.component.TextArea;
import view.window.ManagerView;

public abstract class ViewNotListener extends View {
	protected TextArea inputProducao;
	private JTextField inputNome;
	
	protected ELinguagem eLinguagem;
	
	/* Construtor */
	public ViewNotListener(ManagerView manager, ELinguagem eLinguagem) {
		super(manager);
		
		this.eLinguagem = eLinguagem;
		
		this.inputNome = new JTextField();
		this.inputNome.setBounds(90, 0, 200, 20);
		this.inputNome.setEditable(false);
		
		super.view_content_add_component(this.inputNome);
		
		super.view_content_add_component( Component.new_label(0, 2, 50, 15, "Nome:") );
		super.view_content_add_component( Component.new_label(0, 52, 80, 25, "Producoes:") );
		super.view_content_add_component( Component.new_label(90, 460, 700, 15, "Espaços e quebras de linhas serão desconsiderados.") );
	}
	
	protected JTextArea get_input_producao() {
		return this.inputProducao.get_JTextArea();
	}
	protected JTextField get_input_nome() {
		return this.inputNome;
	}
	
	protected String get_nome() {
		return this.inputNome.getText();
	}
	protected String get_producao() {
		return this.inputProducao.get_JTextArea().getText();
	}
}

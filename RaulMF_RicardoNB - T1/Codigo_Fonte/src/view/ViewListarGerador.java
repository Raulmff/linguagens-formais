package view;

import linguagem.ELinguagem;
import linguagem.ILinguagemGerador;
import linguagem.automato.Automato;
import linguagem.expressao.Expressao;
import linguagem.expressao.Simone;
import linguagem.gramatica.Gramatica;
import view.component.Component;
import view.component.EMenuDialog;
import view.component.TextArea;
import view.window.ManagerView;
import view.window.Window;

public class ViewListarGerador extends ViewListener {
	private TextArea inputProducao;
	private ILinguagemGerador editando;
	
	private EMenuDialog eMenu;
	
	/* Construtor */
	public ViewListarGerador(ManagerView manager, ELinguagem eLinguagem) {
		super(manager, eLinguagem);
		
		if (eLinguagem == ELinguagem.GRAMATICA) {
			this.eMenu = EMenuDialog.EDITAR_GERARAF_FECHAMENTO_REMOVER;
		} else {
			this.eMenu = EMenuDialog.EDITAR_GERARAF_REMOVER;
		}
		
		this.menuDialog.set_menu_dialog(this.eMenu);
		this.inputProducao = new TextArea(90, 50, 470, 400);
		
		this.view_content_add_component(this.inputProducao.get_JScrollPane());
		this.view_content_add_component( Component.new_label(90, 460, 700, 15, "Espaços e quebras de linhas serão desconsiderados.") );
	}
	
	@Override
	public void mostrar(String nomeBuscado) {
		this.editando = (ILinguagemGerador) this.managerILinguagem.get_iLinguagem(this.eLinguagem, nomeBuscado);
		
		String producoes;
		producoes = this.editando.get_producoes();
		
		this.set_input_nome(nomeBuscado);
		this.set_producoes(producoes);
		
		super.mostrar(nomeBuscado);
	}
	@Override
	protected void modo_visualizar() {
		super.modo_visualizar();
		super.menuDialog.set_menu_dialog(this.eMenu);
	}
	@Override
	protected void modo_editar() {
		super.modo_editar();
		super.menuDialog.set_menu_dialog(EMenuDialog.SALVAR_CANCELAR);
	}
	@Override
	protected void enable_editar(boolean enable) {
		this.inputProducao.set_enable(enable);
	}
	
	/* Metodos Implements */
	@Override
	public void salvar() {
		super.salvar();
		
		String producoes;
		producoes = this.get_producao();
		
		this.editando.set_producoes(producoes);
		
		String message, title;
		
		if (this.eLinguagem == ELinguagem.GRAMATICA) {
			message = "Gramatica";
			title = "Editar Gramatica Regular";
		} else {
			message = "Expressao";
			title = "Editar Expressao Regular";
		}
		
		this.cancelar();
		
		Window.insert_message(message+" salva com sucesso!", title);
	}
	
	private void set_producoes(String producoes) {
		this.inputProducao.set_text(producoes);
	}
	private String get_producao() {
		return this.inputProducao.get_JTextArea().getText();
	}
	
	@Override
	public void obter_fechamento() {
		super.obter_fechamento();
		
		Gramatica gramatica;
		gramatica = (Gramatica) this.editando;
		
		System.out.println(gramatica.get_producoes().replaceAll("\n", ""));
		
		Automato automato;
		automato = gramatica.gera_automato();
		automato = automato.get_fechamento();
		
		automato.print();
		
		String nomeGramatica;
		nomeGramatica = Gramatica.get_novo_nome();
		
		Gramatica gramaticaFechada;
		gramaticaFechada = new Gramatica(nomeGramatica);
		gramaticaFechada.gerar_gramatica(automato);
		
		this.managerILinguagem.add_gramatica(gramaticaFechada);
		this.managerILinguagem.add_automato(automato);
		
		this.reload_aside();
		this.repaint();
		
		Window.insert_message("Gramatica gerada com sucesso!", "Fechamento Gramatica");
	}
	
	@Override
	public void gerarAf() {
		if (this.eLinguagem == ELinguagem.GRAMATICA) {
			Gramatica gramatica;
			gramatica = (Gramatica) this.editando;
			
			Automato automato;
			automato = gramatica.gera_automato();
			
			this.managerILinguagem.add_automato(automato);
			
			Window.insert_message("Automato gerado com sucesso!", "Gerar Automato Finito");
		} else {
			Expressao expressao;
			expressao = (Expressao) this.editando;
			
			Simone simone;
			simone = new Simone();
			
			Automato automato;
			automato = simone.gerar_automato(expressao);
			
			this.managerILinguagem.add_automato(automato);
			
			Window.insert_message("Automato gerado com sucesso!", "Gerar Automato Finito");
		}
	}
	
	@Override
	public void remover() {
		this.managerILinguagem.remover_ilinguagem(this.editando, this.eLinguagem);
		super.remover();
	}
}

package view.event;

import java.awt.event.ActionEvent;

import view.component.IMenuDialog;

public class EventMenuDialog implements IEvent {
	private IMenuDialog view;
	
	public EventMenuDialog(IMenuDialog view) {
		this.view = view;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
			case "SUBTRAIR":
				this.view.subtrair();
				break;
			case "INTERSECTAR":
				this.view.intersectar();
				break;
			case "ADICIONAR":
				this.view.adicionar();
				break;
			case "SALVAR":
				this.view.salvar();
				break;
			case "GERARAF":
				this.view.gerarAf();
				break;
			case "EDITAR":
				this.view.editar();
				break;
			case "REMOVER":
				this.view.remover();
				break;
			case "MINIMIZARAF":
				this.view.minimizarAf();
				break;
			case "DETERMINIZARAF":
				this.view.determinizarAf();
				break;
			case "UNIR":
				this.view.unir();
				break;
			case "CONCATENAR":
				this.view.concatenar();
				break;
			case "OBTERREVERSO":
				this.view.obter_reverso();
				break;
			case "OBTERFECHAMENTO":
				this.view.obter_fechamento();
				break;
			case "GERARGR":
				this.view.gerar_gr();
				break;
				//"CANCELAR"
			default: 
				this.view.cancelar();
				break;
		}
	}

	@Override
	public void process(EEvent e, Object conteudo) {}
}

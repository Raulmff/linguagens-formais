package view.event;

import java.awt.event.ActionEvent;

import view.ViewAutomato;

public class EventListarAutomato implements IEvent {
	private ViewAutomato view;
	
	public EventListarAutomato(ViewAutomato view) {
		this.view = view;
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		String actionCommand;
		actionCommand = event.getActionCommand();
		
		switch (actionCommand) {
			case "RECONHECER_SENTENCA":
				this.view.reconhecer_sentenca();
				break;
			case "ENUMERAR_SENTENCAS":
				this.view.enumerar_sentenca();
				break;
			default:
				break;
		}
	}

	@Override
	public void process(EEvent e, Object conteudo) {
		
	}
}

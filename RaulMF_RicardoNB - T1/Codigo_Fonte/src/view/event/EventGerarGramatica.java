package view.event;

import java.awt.event.ActionEvent;

import view.ViewGerarGramatica;

public class EventGerarGramatica implements IEvent {
	private ViewGerarGramatica view;
	
	public EventGerarGramatica(ViewGerarGramatica view) {
		this.view = view;
	}
	
	@Override
	public void actionPerformed(ActionEvent event) {
		switch (event.getActionCommand()) {
			case "OPCAO_P":
				this.view.update_opcao_show_item_p();
				break;
			case "OPCAO_S":
				this.view.update_opcao_show_item_s();
				break;
			default:
				break;
		}
	}
	
	@Override
	public void process(EEvent e, Object conteudo) {}
}

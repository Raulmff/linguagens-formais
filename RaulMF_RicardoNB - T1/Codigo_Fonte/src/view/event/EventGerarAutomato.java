package view.event;

import java.awt.event.ActionEvent;

import view.ViewGerarAutomato;

public class EventGerarAutomato implements IEvent {
	private ViewGerarAutomato view;
	
	public EventGerarAutomato(ViewGerarAutomato view) {
		this.view = view;
	}
	
	@Override
	public void actionPerformed(ActionEvent event) {
		switch (event.getActionCommand()) {
			case "TIPO_P":
				this.view.update_tipo_item_p();
				break;
			case "TIPO_S":
				this.view.update_tipo_item_s();
				break;
			case "OPCAO_P":
				this.view.update_opcao_show_item_p();
				break;
			case "OPCAO_S":
				this.view.update_opcao_show_item_s();
				break;
			default:
				break;
		}
	}
	
	@Override
	public void process(EEvent e, Object conteudo) {}
}

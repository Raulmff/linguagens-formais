package view.event;

import java.awt.event.ActionEvent;

import view.ViewCriar;

public class EventNovo implements IEvent {
	private ViewCriar novo;
	
	public EventNovo(ViewCriar novo) {
		this.novo = novo;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		this.novo.adicionar();
	}

	@Override
	public void process(EEvent e, Object conteudo) {}
}

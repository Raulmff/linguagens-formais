package view.event;

import java.awt.event.ActionEvent;

import view.ViewListener;

public class EventMenuAside implements IEvent {
	private ViewListener viewListener;
	
	public EventMenuAside(ViewListener listener) {
		this.viewListener = listener;
	}
	
	@Override
	public void process(EEvent e, Object conteudo) {
		if (this.viewListener == null) {
			return;
		}
		
		String nome;
		nome = (String)conteudo;
		
		this.viewListener.mostrar(nome);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {}
}

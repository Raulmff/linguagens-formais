package view.event;

import java.awt.event.ActionEvent;

import view.EView;
import view.window.ManagerView;

public class EventMenuBar implements IEvent {
	private ManagerView manager;
	
	public EventMenuBar(ManagerView manager) {
		this.manager = manager;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		String actionCommand;
		actionCommand = e.getActionCommand();
		
		System.out.println(actionCommand);
		
		switch (actionCommand) {
			/* AUTOMATO */
			case "AF_GERAR":
				this.manager.set_visible(EView.AF_GERAR);
				break;
			case "AF_LISTAR":
				this.manager.set_visible(EView.AF_LISTAR);
				break;
			case "AF_SENTENCA_ENUMERAR":
				this.manager.set_visible(EView.AF_SENTENCA_ENUMERAR);
				break;
			case "AF_SENTENCA_RECONHECER":
				this.manager.set_visible(EView.AF_SENTENCA_RECONHECER);
				break;
				
			// GRAMATICA
			case "GR_CRIAR":
				this.manager.set_visible(EView.GR_CRIAR);
				break;
			case "GR_LISTAR":
				this.manager.set_visible(EView.GR_LISTAR);
				break;
			case "GR_GERAR":
				this.manager.set_visible(EView.GR_GERAR);
				break;
				
			// EXPRESSAO
			case "ER_CRIAR":
				this.manager.set_visible(EView.ER_CRIAR);
				break;
			case "ER_LISTAR":
				this.manager.set_visible(EView.ER_LISTAR);
				break;
			default:
				break;
		}
	}

	@Override
	public void process(EEvent e, Object conteudo) {}
}

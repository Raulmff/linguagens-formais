package view;

import java.util.ArrayList;

import javax.swing.JPanel;

import linguagem.ILinguagem;
import linguagem.ILinguagemGerador;
import linguagem.automato.Automato;
import linguagem.gramatica.Gramatica;
import view.component.ComboBox;
import view.component.Component;
import view.component.EMenuDialog;
import view.component.GerarILinguagem;
import view.event.EventGerarAutomato;
import view.window.ManagerView;
import view.window.Window;

public class ViewGerarAutomato extends View {
	private EventGerarAutomato event;
	private ComboBox boxTipoP, boxTipoS;
	private ComboBox boxOpcaoP, boxOpcaoS;
	
	private GerarILinguagem iLinguagemP, iLinguagemS;
	private String[] tipo;
	
	public ViewGerarAutomato(ManagerView manager) {
		super(manager);
		this.menuDialog.set_menu_dialog(EMenuDialog.SUBTRAIR_INTERSECTAR_CANCELAR);
		
		this.event = new EventGerarAutomato(this);
		
		this.tipo = new String[] {
			"Automato Finito",
			"Expressao Regular",
			"Gramatica Regular"
		};
		
		this.load_iLinguagem();
		this.load_opcao();
		this.load_tipo();
		this.load_label();
	}
	
	private void load_iLinguagem() {
		this.iLinguagemP = new GerarILinguagem(0, 100);
		this.iLinguagemS = new GerarILinguagem(430, 100);
		
		this.view_content_add_component(this.iLinguagemP.get_gerador_JScrollPane());
		this.view_content_add_component(this.iLinguagemS.get_gerador_JScrollPane());
		
		this.view_content_add_component(this.iLinguagemP.get_automato_JScrollPane());
		this.view_content_add_component(this.iLinguagemS.get_automato_JScrollPane());
	}
	private void load_label() {
		this.view_content_add_component( Component.new_label(0, 0, 100, 40, "Tipo 1:") );
		this.view_content_add_component( Component.new_label(430, 0, 100, 40, "Tipo 2:") );
		
		this.view_content_add_component( Component.new_label(0, 50, 100, 40, "Opcao 1:") );
		this.view_content_add_component( Component.new_label(430, 50, 100, 40, "Opcao 2:") );
	}
	private void load_tipo() {
		this.boxTipoP = new ComboBox(100, 0, "TIPO_P", this.event);
		this.boxTipoS = new ComboBox(530, 0, "TIPO_S", this.event);
		
		this.boxTipoP.set_itens(this.tipo);
		this.boxTipoS.set_itens(this.tipo);
		
		this.view_content_add_component(this.boxTipoP.get_comboBox());
		this.view_content_add_component(this.boxTipoS.get_comboBox());
	}
	private void load_opcao() {
		this.boxOpcaoP = new ComboBox(100, 50, "OPCAO_P", this.event);
		this.boxOpcaoS = new ComboBox(530, 50, "OPCAO_S", this.event);
		
		this.boxOpcaoP.set_enable(false);
		this.boxOpcaoS.set_enable(false);
		
		this.view_content_add_component(this.boxOpcaoP.get_comboBox());
		this.view_content_add_component(this.boxOpcaoS.get_comboBox());
	}
	
	public void update_tipo_item_p() {
		this.update_opcao_item(this.boxTipoP, this.boxOpcaoP);
		this.iLinguagemP.hide_gerador();
		this.iLinguagemP.hide_automato();
	}
	public void update_tipo_item_s() {
		this.update_opcao_item(this.boxTipoS, this.boxOpcaoS);
		this.iLinguagemS.hide_gerador();
		this.iLinguagemS.hide_automato();
	}
	
	public void update_opcao_show_item_p() {
		this.update_opcao_show_item(this.boxTipoP, this.boxOpcaoP, this.iLinguagemP);
	}
	public void update_opcao_show_item_s() {
		this.update_opcao_show_item(this.boxTipoS, this.boxOpcaoS, this.iLinguagemS);
	}
	
	private void update_opcao_item(ComboBox boxTipo, ComboBox boxOpcao) {
		int selected;
		selected = boxTipo.get_indice_selected();
		
		ArrayList<ILinguagem> array;
		
		switch (selected) {
			// AUTOMATO
			case 1:
				array = this.managerILinguagem.get_automatos();
				break;
			// EXPRESSAO
			case 2:
				array = this.managerILinguagem.get_expressoes();
				break;
			// GRAMATICA
			case 3:
				array = this.managerILinguagem.get_gramaticas();
				break;
			// SELECIONAR
			default:
				boxOpcao.load_default_disabled();
				return;
		}
		
		boxOpcao.set_itens_iLinguagem(array);
		boxOpcao.set_enable(true);
	}
	private void update_opcao_show_item(ComboBox boxTipo, ComboBox boxOpcao, GerarILinguagem geradorILinguagem) {
		if (boxOpcao.get_indice_selected() == 0) {
			geradorILinguagem.hide_gerador();
			geradorILinguagem.hide_automato();
			return;
		}
		
		int selected;
		selected = boxTipo.get_indice_selected();
		
		String nomeBuscado;
		nomeBuscado = boxOpcao.get_selected();
		
		ILinguagem iLinguagem;
		
		switch (selected) {
			// AUTOMATO
			case 1:
				iLinguagem = this.managerILinguagem.get_automato(nomeBuscado);
				
				if (iLinguagem == null) {
					return;
				}
				
				geradorILinguagem.show_automato((Automato)iLinguagem);
				return;
			// EXPRESSAO
			case 2:
				iLinguagem = this.managerILinguagem.get_expressao(nomeBuscado);
				break;
			// GRAMATICA
			case 3:
				iLinguagem = this.managerILinguagem.get_gramatica(nomeBuscado);
				break;
			// SELECIONAR
			default:
				boxOpcao.load_default_disabled();
				return;
		}
		
		if (iLinguagem == null) {
			return;
		}
		
		geradorILinguagem.show_gerador((ILinguagemGerador)iLinguagem);
	}
	
	private boolean get_valido() {
		boolean valido;
		valido = this.boxTipoP.get_valido() && this.boxOpcaoP.get_valido();
		valido = this.boxTipoS.get_valido() && this.boxOpcaoS.get_valido() && valido;
		
		if (!valido) {
			Window.insert_message("Todos os campos devem ser preenchidos", "Falha ao gerar automato!");
		}
		
		return valido;
	}
	
	@Override
	public void visible(JPanel p) {
		super.visible(p);
		
		this.boxTipoP.set_itens(this.tipo);
		this.boxTipoS.set_itens(this.tipo);
		
		this.update_tipo_item_p();
		this.update_tipo_item_s();
	}
	@Override
	public void intersectar() {
		if( !this.get_valido() ) {
			return;
		}
		
		ArrayList<Automato> retorno;
		retorno = new ArrayList<Automato>();
		
		String tipoSelecionadoP, tipoSelecionadoS;
		tipoSelecionadoP = this.boxTipoP.get_selected();
		tipoSelecionadoS = this.boxTipoS.get_selected();
		
		String nomeBuscadoP, nomeBuscadoS;
		nomeBuscadoP = this.boxOpcaoP.get_selected();
		nomeBuscadoS = this.boxOpcaoS.get_selected();
		
		ILinguagem iLinguagemS, iLinguagemP;
		iLinguagemP = this.get_iLinguagem(tipoSelecionadoP, nomeBuscadoP);
		iLinguagemS = this.get_iLinguagem(tipoSelecionadoS, nomeBuscadoS);
		
		if (iLinguagemP == null || iLinguagemS == null) {
			return;
		}
		
		if (!iLinguagemP.get_compativeis(iLinguagemS)) {
			Window.insert_message("Alfabetos incompativeis", "Erro!");
			return;
		}
		
		Automato automatoP, automatoS;
		automatoP = this.get_automato(tipoSelecionadoP, nomeBuscadoP);
		automatoS = this.get_automato(tipoSelecionadoS, nomeBuscadoS);
		
		retorno.add(automatoP);
		retorno.add(automatoS);
		
		automatoP = this.managerILinguagem.get_minimizacao(automatoP);
		automatoS = this.managerILinguagem.get_minimizacao(automatoS);
		
		retorno.add(automatoP);
		retorno.add(automatoS);
		
		ArrayList<Automato> automatosGerados;
		automatosGerados = this.managerILinguagem.get_intersecao(automatoP, automatoS);
		retorno.addAll(automatosGerados);
		
		for (int c = 0; c < retorno.size(); c++) {
			Automato gerado;
			gerado = retorno.get(c);
			
			this.managerILinguagem.add_automato(gerado);
		}
		
		String mensagem;
		mensagem = "Automato gerado pela interseccao foi gerado com sucesso!\n";
		
		Window.insert_message(mensagem, "Sucesso!");
	}
	@Override
	public void subtrair() {
		if( !this.get_valido() ) {
			return;
		}
		
		ArrayList<Automato> retorno;
		retorno = new ArrayList<Automato>();
		
		String tipoSelecionadoP, tipoSelecionadoS;
		tipoSelecionadoP = this.boxTipoP.get_selected();
		tipoSelecionadoS = this.boxTipoS.get_selected();
		
		String nomeBuscadoP, nomeBuscadoS;
		nomeBuscadoP = this.boxOpcaoP.get_selected();
		nomeBuscadoS = this.boxOpcaoS.get_selected();
		
		ILinguagem iLinguagemS, iLinguagemP;
		iLinguagemP = this.get_iLinguagem(tipoSelecionadoP, nomeBuscadoP);
		iLinguagemS = this.get_iLinguagem(tipoSelecionadoS, nomeBuscadoS);
		
		if (iLinguagemP == null || iLinguagemS == null) {
			return;
		}
		
		if (!iLinguagemP.get_compativeis(iLinguagemS)) {
			Window.insert_message("Alfabetos incompativeis", "Erro!");
			return;
		}
		
		Automato automatoP, automatoS;
		automatoP = this.get_automato(tipoSelecionadoP, nomeBuscadoP);
		automatoS = this.get_automato(tipoSelecionadoS, nomeBuscadoS);
		
		retorno.add(automatoP);
		retorno.add(automatoS);
		
		automatoP = this.managerILinguagem.get_minimizacao(automatoP);
		automatoS = this.managerILinguagem.get_minimizacao(automatoS);
		
		retorno.add(automatoP);
		retorno.add(automatoS);
		
		ArrayList<Automato> automatosGerados;
		automatosGerados = this.managerILinguagem.get_diferenca(automatoP, automatoS);
		retorno.addAll(automatosGerados);
		
		for (int c = 0; c < retorno.size(); c++) {
			Automato gerado;
			gerado = retorno.get(c);
			
			this.managerILinguagem.add_automato(gerado);
		}
		
		String mensagem;
		mensagem = "Automato gerado pela diferenca foi gerado com sucesso!\n";
		
		Window.insert_message(mensagem, "Sucesso!");
	}
	
	private ILinguagem get_iLinguagem(String tipoSelecionado, String nomeBuscado) {
		switch (tipoSelecionado) {
			case "Expressao Regular":
				//Expressao expressao;
				//expressao = this.managerILinguagem.get_expressao( nomeBuscado );
				
				Window.insert_message("Nao implementado", "FALHA");
				return null;
			case "Gramatica Regular":
				Gramatica gramatica;
				gramatica = this.managerILinguagem.get_gramatica( nomeBuscado );
				return gramatica;
			default:
				return this.managerILinguagem.get_automato( nomeBuscado );
		}
	}
	private Automato get_automato(String tipoSelecionado, String nomeBuscado) {
		Automato automato;
		
		switch (tipoSelecionado) {
			case "Expressao Regular":
				//Expressao expressaoS;
				//expressaoS = this.managerILinguagem.get_expressao( nomeBuscado );
				
				Window.insert_message("Nao implementado", "FALHA");
				return null;
			case "Gramatica Regular":
				Gramatica gramatica;
				gramatica = this.managerILinguagem.get_gramatica( nomeBuscado );
				automato = gramatica.gera_automato();
				break;
			default:
				automato = this.managerILinguagem.get_automato( nomeBuscado );
				break;
		}
		
		return automato;
	}
}

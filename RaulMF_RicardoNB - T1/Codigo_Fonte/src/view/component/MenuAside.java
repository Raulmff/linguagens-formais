package view.component;

import java.util.ArrayList;

import javax.swing.JPanel;

import linguagem.ILinguagem;
import table.Table;
import table.TableRow;
import view.ViewListener;
import view.event.EventMenuAside;

public class MenuAside {
	private JPanel panel;
	private ViewTable vTable;
	private EventMenuAside event;
	
	private Table table;
	
	public MenuAside(ViewListener listener) {
		this.event = new EventMenuAside(listener);
		this.vTable = new ViewTable(0, 0, 200, 540, this.event);
		
		TableRow rowHead;
		rowHead = new TableRow();
		rowHead.add_column("Menu");
		
		this.table = new Table(rowHead);
		
		this.panel = new JPanel();
		this.panel.setLayout(null);
		this.panel.setBounds(0, 0, 200, 560);
		
		this.load();
	}
	
	private void load() {
		if (this.vTable != null) {
			this.panel.remove(this.vTable.get_JScrollPane());
		}
		
		this.table.re_load();
		this.vTable.set_table(this.table);
		this.panel.add(this.vTable.get_JScrollPane());
	}
	
	/* Metodos Setter */
	public void set_menu(ArrayList<ILinguagem> array) {
		this.load();
		
		for (int c = 0; c < array.size(); c++) {
			ILinguagem linguagem;
			linguagem = array.get(c);
			
			TableRow row;
			row = new TableRow();
			row.add_column(linguagem.get_nome());
			
			this.table.add_row(row);
		}
		
		this.vTable.reload_model();
	}
	
	/* Metodos Getter */
	public JPanel get_JPanel() {
		return this.panel;
	}
	
	public void repaint() {
		this.panel.repaint();
		this.vTable.repaint();
	}
}

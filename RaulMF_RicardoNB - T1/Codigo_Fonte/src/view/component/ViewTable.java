package view.component;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import table.Table;
import view.event.EEvent;
import view.event.IEvent;

public class ViewTable {
	private DefaultTableModel model;
	private JTable jTable;
	private JScrollPane scroll;
	private Table table;
	
	private int x, y;
	private int width, height;
	
	/* Metodos Construtor */
	public ViewTable(int x, int y, int width, int height) {
		this(x, y, width, height, null);
	}
	public ViewTable(int x, int y, int width, int height, IEvent e) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		
		this.load_table(e);
	}
	
	/* Metodos Load */
	private void load_table(IEvent i) {
		this.jTable = new JTable() {
			@Override
			public boolean isCellEditable(int r, int c) {  
				return false;  
			}
		};
		this.jTable.addMouseListener ( new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 1) {
					if (i != null) {
						JTable table;
						table = (JTable) e.getComponent();
						
						i.process(EEvent.SELECT_MENU_ASIDE, table.getValueAt( table.getSelectedRow() , 0) );
					}
				}
			}
		});
		
		this.jTable.setBounds(this.x, this.y, this.width, this.height);
		this.jTable.getTableHeader().setReorderingAllowed(false);
		this.jTable.setCellSelectionEnabled(false);
		
		this.load_scroll();
	}
	private void load_scroll() {
		this.scroll = new JScrollPane();
		this.scroll.setBounds(this.x, this.y, this.width, this.height);
		
		this.scroll.setViewportView(this.jTable);
	}
	public void reload_model() {
		this.model = new DefaultTableModel(null, this.table.get_head().to_object());
		
		int c;
		for (c = 0; c < this.table.get_size(); c++) {
			this.model.addRow( table.get_body(c).to_object() );
		}
		
		this.jTable.setModel(this.model);
	}
	
	public void repaint() {
		this.jTable.repaint();
		this.scroll.repaint();
	}
	
	/* Metodos Setter */
	public void set_table(Table table) {
		this.table = table;
		this.reload_model();
	}
	
	public void set_visible(boolean visible) {
		this.scroll.setVisible(visible);
		this.jTable.setVisible(visible);
	}
	
	/* Metodos Getter */
	public JScrollPane get_JScrollPane() {
		return this.scroll;
	}
	public DefaultTableModel get_DefaultTableModel() {
		return this.model;
	}
}

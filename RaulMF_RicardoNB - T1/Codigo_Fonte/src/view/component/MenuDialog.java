package view.component;

import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JPanel;

import view.event.IEvent;

public class MenuDialog {
	private JPanel panel;
	private IEvent event;
	
	private int widthLivre;
	
	public MenuDialog(IEvent i) {
		this.event = i;
		
		this.panel = new JPanel();
		this.panel.setLayout(null);
		this.panel.setBounds(210, 510, 560, 35);
	}
	
	/* Metodos Setter */
	public void set_menu_dialog(EMenuDialog e) {
		this.panel.removeAll();
		
		this.widthLivre = 560;
		
		switch (e) {
			case SALVAR_CANCELAR:
				this.add_dialog_botao("Cancelar", "CANCELAR");
				this.add_dialog_botao("Salvar", "SALVAR");
				break;
			case ADICIONAR_CANCELAR:
				this.add_dialog_botao("Cancelar", "CANCELAR");
				this.add_dialog_botao("Adicionar", "ADICIONAR");
				break;
			case EDITAR_GERARAF_FECHAMENTO_REMOVER:
				this.add_dialog_botao("Remover", "REMOVER");
				this.add_dialog_botao("Obter Fechamento", "OBTERFECHAMENTO", 170);
				this.add_dialog_botao("Gerar AF", "GERARAF");
				this.add_dialog_botao("Editar", "EDITAR");
				break;
			case EDITAR_GERARAF_REMOVER:
				this.add_dialog_botao("Remover", "REMOVER");
				this.add_dialog_botao("Gerar AF", "GERARAF");
				this.add_dialog_botao("Editar", "EDITAR");
				break;
			case DETERMINIZAR_MINIMIZAR_REVERSO_REMOVER:
				this.add_dialog_botao("Remover", "REMOVER");
				this.add_dialog_botao("Reverso", "OBTERREVERSO");
				this.add_dialog_botao("Minimizar", "MINIMIZARAF", 115);
				this.add_dialog_botao("Determinizar", "DETERMINIZARAF", 125);
				this.add_dialog_botao("Gerar GR", "GERARGR");
				break;
			case CANCELAR:
				this.add_dialog_botao("Cancelar", "CANCELAR");
				break;
			case SUBTRAIR_INTERSECTAR_CANCELAR:
				this.add_dialog_botao("Cancelar", "CANCELAR");
				this.add_dialog_botao("Intersectar", "INTERSECTAR", 130);
				this.add_dialog_botao("Subtrair", "SUBTRAIR");
				break;
			case UNIR_CONCATENAR_CANCELAR:
				this.add_dialog_botao("Cancelar", "CANCELAR");
				this.add_dialog_botao("Concatenar", "CONCATENAR", 120);
				this.add_dialog_botao("Unir", "UNIR");
			default:
				break;
		}
	}
	public void set_visible(boolean visible) {
		this.panel.setVisible(visible);
	}
	
	/* Metodos Getter */
	public JPanel get_JPanel() {
		return this.panel;
	}
	
	private void add_dialog_botao(String label, String actionCommand, int width) {
		this.widthLivre -= width;
		
		int posX;
		posX = this.widthLivre;
		
		// Adiciona espacamento para proximo botao nao ficar grudado com o atualmente criado
		this.widthLivre -= 5;
		
		
		String familia = "Arial";
		int estilo = Font.BOLD;
		int tamanho = 11;
		Font f = new Font(familia, estilo, tamanho);
		
		
		JButton button;
		button = new JButton(label);
		button.setFont(f);
		button.setBounds(posX, 0, width, 32);
		button.addActionListener(this.event);
		button.setActionCommand(actionCommand);
		
		this.panel.add(button);
	}
	
	/* Metodos Add */
	private void add_dialog_botao(String label, String actionCommand) {
		this.add_dialog_botao(label, actionCommand, 100);
	}
}

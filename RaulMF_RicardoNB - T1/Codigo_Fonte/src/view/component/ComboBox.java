package view.component;

import java.util.ArrayList;

import javax.swing.JComboBox;

import linguagem.ILinguagem;
import view.event.IEvent;

public class ComboBox {
	private JComboBox<String> comboBox;
	
	public ComboBox(int x, int y, String actionCommand, IEvent event) {
		this.comboBox = new JComboBox<String>();
		this.comboBox.setBounds(x, y, 240, 40);
		this.comboBox.addActionListener(event);
		this.comboBox.setActionCommand(actionCommand);
	}
	
	public void load_default_disabled() {
		this.load_default();
		this.comboBox.setEnabled(false);
	}
	
	public void load_default() {
		this.comboBox.removeAllItems();
		this.comboBox.addItem("Selecione");
	}
	
	public void set_enable(boolean enable) {
		this.comboBox.setEnabled(enable);
	}
	public void set_itens(String[] itens) {
		this.load_default();
		
		if (itens == null) {
			return;
		}
		
		for(int c = 0; c < itens.length; c++) {
			this.comboBox.addItem(itens[c]);
		}
	}
	public void set_itens_iLinguagem(ArrayList<ILinguagem> itens) {
		this.load_default();
		
		if (itens == null) {
			return;
		}
		
		for(int c = 0; c < itens.size(); c++) {
			ILinguagem linguagem;
			linguagem = itens.get(c);
			
			this.comboBox.addItem(linguagem.get_nome());
		}
	}
	
	public int get_indice_selected() {
		return this.comboBox.getSelectedIndex();
	}
	public String get_selected() {
		return (String)this.comboBox.getSelectedItem();
	}
	public JComboBox<String> get_comboBox() {
		return this.comboBox;
	}
	
	public boolean get_valido() {
		return (this.get_indice_selected() > 0);
	}
}

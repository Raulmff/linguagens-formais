package view.component;

import javax.swing.JLabel;

public class Component {
	public static JLabel new_label(int x, int y, int width, int height, String title) {
		JLabel label;
		label = new JLabel(title);
		label.setBounds(x, y, width, height);
		
		return label;
	}
}

package view.component;

import java.awt.Color;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.UIManager;

import view.event.EventMenuBar;

public class MenuBar {
	private JMenuBar menuBar;
	private EventMenuBar event;
	
	/* 
	 * Construtor
	 */
	public MenuBar(EventMenuBar event) {
		this.event = event;
		
		this.menuBar = new JMenuBar();
		this.menuBar.setFont(UIManager.getFont("MenuBar.font"));
		this.menuBar.setForeground(Color.BLACK);
		this.menuBar.setBackground(Color.LIGHT_GRAY);
		this.menuBar.setBounds(0, 0, 820, 30);
		
		this.gerar_menu_af();
		this.gerar_menu_er();
		this.gerar_menu_gr();
	}
	
	/* Metodos Gerador Menu */
	private void gerar_menu_af() {
		JMenuItem listar, gerarAf, reconhecer, enumerar;
		listar = new JMenuItem("Listar");
		gerarAf = new JMenuItem("Operacoes");
		enumerar = new JMenuItem("Enumerar Sentencas");
		reconhecer = new JMenuItem("Reconhecer Sentenca");
		
		listar.addActionListener(this.event);
		gerarAf.addActionListener(this.event);
		enumerar.addActionListener(this.event);
		reconhecer.addActionListener(this.event);
		
		listar.setActionCommand("AF_LISTAR");
		gerarAf.setActionCommand("AF_GERAR");
		enumerar.setActionCommand("AF_ENUMERAR");
		reconhecer.setActionCommand("AF_RECONHECER");
		
		JMenu menuAF;
		menuAF = new JMenu("Automato");
		menuAF.add(listar);
		menuAF.add(gerarAf);
		//menuAF.addSeparator();
		//menuAF.add(enumerar);
		//menuAF.add(reconhecer);
		
		this.menuBar.add(menuAF);
	}
	
	private void gerar_menu_gr() {
		JMenuItem listar, criar, gerarGr;
		criar = new JMenuItem("Criar");
		listar = new JMenuItem("Listar");
		gerarGr = new JMenuItem("Operacoes");
		
		criar.addActionListener(this.event);
		listar.addActionListener(this.event);
		gerarGr.addActionListener(this.event);
		
		criar.setActionCommand("GR_CRIAR");
		listar.setActionCommand("GR_LISTAR");
		gerarGr.setActionCommand("GR_GERAR");
		
		JMenu menuGR;
		menuGR = new JMenu("Gramatica");
		menuGR.add(criar);
		menuGR.add(listar);
		menuGR.add(gerarGr);
		
		this.menuBar.add(menuGR);
	}

	private void gerar_menu_er() {
		JMenuItem criar, listar;
		criar = new JMenuItem("Criar");
		listar = new JMenuItem("Listar");
		
		criar.addActionListener(this.event);
		listar.addActionListener(this.event);
		
		criar.setActionCommand("ER_CRIAR");
		listar.setActionCommand("ER_LISTAR");
		
		JMenu menuER;
		menuER = new JMenu("Expressao");
		menuER.add(criar);
		menuER.add(listar);
		
		this.menuBar.add(menuER);
	}
	
	/* Metodos Getters */
	public JMenuBar get_jmenu_bar() {
		return this.menuBar;
	}
}

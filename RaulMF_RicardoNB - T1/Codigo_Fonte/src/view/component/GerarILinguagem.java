package view.component;

import javax.swing.JScrollPane;

import linguagem.ILinguagemGerador;
import linguagem.automato.Automato;
import table.TableAutomato;

public class GerarILinguagem {
	private TextArea textArea;
	private TableAutomato table;
	
	public GerarILinguagem(int x, int y) {
		this.table = new TableAutomato(x, y, 340, 380);
		
		this.textArea = new TextArea(x, y, 340, 380);
		this.textArea.set_enable(false);
		
		this.hide_automato();
		this.hide_gerador();
	}
	
	public JScrollPane get_gerador_JScrollPane() {
		return this.textArea.get_JScrollPane();
	}
	public JScrollPane get_automato_JScrollPane() {
		return this.table.get_jScrollPane();
	}
	
	public void show_gerador(ILinguagemGerador gerador) {
		String producoes;
		producoes = gerador.get_producoes();
		
		this.textArea.set_text(producoes);
		this.textArea.set_visible(true);
	}
	
	public void hide_gerador() {
		this.textArea.set_visible(false);
	}
	public void hide_automato() {
		this.table.set_visible(false);
	}
	
	public void show_automato(Automato automato) {
		this.table.montar_table(automato);
		this.table.set_visible(true);
	}
}

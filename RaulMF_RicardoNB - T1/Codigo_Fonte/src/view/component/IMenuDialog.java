package view.component;

public interface IMenuDialog {
	public void salvar();
	public void cancelar();
	public void adicionar();
	public void editar();
	public void remover();
	
	public void gerarAf();
	public void minimizarAf();
	public void determinizarAf();
	
	public void intersectar();
	public void subtrair();
	
	public void unir();
	public void concatenar();
	public void obter_fechamento();
	public void obter_reverso();
	public void gerar_gr();
}
